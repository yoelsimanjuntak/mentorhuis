# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: web_mentorhuis_20200619
# Generation Time: 2021-04-30 02:39:40 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Inspirasi','#f56954'),
	(5,'Lainnya','#3c8dbc');

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(4,5,'2019-06-26','Hubungi Kami','hubungi-kami','<p><strong>Dinas Komunikasi Dan Informatika Kabupaten Humbang Hasundutan</strong></p>\r\n\r\n<p>Jl. SM. Raja Kompleks Perkantoran Tano Tubu, Doloksanggul 22457</p>\r\n\r\n<table border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Telp</td>\r\n			<td>:</td>\r\n			<td>&nbsp;(0633) 31555</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Email&nbsp;</td>\r\n			<td>:</td>\r\n			<td colspan=\"4\">&nbsp;diskominfo@humbanghasundutankab.go.id</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<hr />\r\n<p>&nbsp;</p>\r\n\r\n<p style=\"text-align:center\"><iframe frameborder=\"0\" height=\"450\" src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3986.6996793830294!2d98.76777421426368!3d2.265541658581495!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x302e3cb7ff0bcc73%3A0x2c1a5b16f34531dc!2sDinas+Komunikasi+dan+Informatika+Kab.+Humbang+Hasundutan!5e0!3m2!1sen!2sid!4v1561433370666!5m2!1sen!2sid\" style=\"border:0\" width=\"600\"></iframe></p>\r\n','2020-12-31',11,NULL,0,NULL,'admin','2019-06-26 08:27:46','admin','2019-06-26 08:27:46'),
	(5,5,'2020-07-06','About','about','<p>DEV</p>\r\n','2021-12-31',0,NULL,0,NULL,'admin','2020-07-06 21:37:31','admin','2020-07-06 21:37:31'),
	(6,5,'2020-07-06','Terms and Condition','terms-and-condition','<p>TOC</p>\r\n','2021-12-31',0,NULL,0,NULL,'admin','2020-07-06 21:39:58','admin','2020-07-06 21:40:25');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Pribadi'),
	(3,'Komunitas'),
	(4,'Perusahaan'),
	(5,'Psikolog');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settingbill
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settingbill`;

CREATE TABLE `_settingbill` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_Bank` varchar(50) NOT NULL DEFAULT '',
  `NM_AccountNo` varchar(50) NOT NULL DEFAULT '',
  `NM_AccountName` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settingbill` WRITE;
/*!40000 ALTER TABLE `_settingbill` DISABLE KEYS */;

INSERT INTO `_settingbill` (`Uniq`, `KD_Bank`, `NM_AccountNo`, `NM_AccountName`)
VALUES
	(3,'BNI','11112087','Mentorhuis ID'),
	(4,'BCA','1111208','Mentorhuis ID');

/*!40000 ALTER TABLE `_settingbill` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settingrate
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settingrate`;

CREATE TABLE `_settingrate` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NM_Grade` varchar(50) NOT NULL DEFAULT '',
  `NUM_ExpFrom` double NOT NULL,
  `NUM_ExpTo` double NOT NULL,
  `NUM_RateRegular` double NOT NULL,
  `NUM_RateOnline` double NOT NULL,
  `NUM_RateGroup` double NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settingrate` WRITE;
/*!40000 ALTER TABLE `_settingrate` DISABLE KEYS */;

INSERT INTO `_settingrate` (`Uniq`, `NM_Grade`, `NUM_ExpFrom`, `NUM_ExpTo`, `NUM_RateRegular`, `NUM_RateOnline`, `NUM_RateGroup`)
VALUES
	(29,'MUDA',0,0,100000,75000,75000),
	(30,'MADYA',0,0,100000,75000,75000),
	(31,'UTAMA',0,0,100000,75000,75000),
	(32,'AHLI',0,0,100000,75000,75000);

/*!40000 ALTER TABLE `_settingrate` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','MENTORHUIS'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Pusat Layanan Konseling dan Mentoring'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_LINKFACEBOOK','SETTING_ORG_LINKFACEBOOK','https://www.facebook.com/Mentor-Huis-105230401189706'),
	(17,'SETTING_ORG_LINKTWITTER','SETTING_ORG_LINKTWITTER','https://twitter.com/MentorhuisID'),
	(18,'SETTING_ORG_LINKINSTAGRAM','SETTING_ORG_LINKINSTAGRAM','');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FirstName` varchar(50) DEFAULT NULL,
  `NM_LastName` varchar(50) DEFAULT NULL,
  `NM_Nickname` varchar(50) NOT NULL,
  `NM_Gender` varchar(50) DEFAULT NULL,
  `DATE_Birth` date DEFAULT NULL,
  `NO_Phone` varchar(50) DEFAULT NULL,
  `NM_Address` text,
  `NM_Province` varchar(50) DEFAULT NULL,
  `NM_City` varchar(50) DEFAULT NULL,
  `NM_Region` varchar(50) DEFAULT NULL,
  `NM_Occupation` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `NUM_RateRegular` double DEFAULT NULL,
  `NUM_RateOnline` double DEFAULT NULL,
  `NUM_Experience` double DEFAULT NULL,
  `NM_Bio` text,
  `NM_MaritalStatus` varchar(50) DEFAULT NULL,
  `NM_EducationalBackground` text,
  `NM_Grade` varchar(50) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `Email`, `NM_FirstName`, `NM_LastName`, `NM_Nickname`, `NM_Gender`, `DATE_Birth`, `NO_Phone`, `NM_Address`, `NM_Province`, `NM_City`, `NM_Region`, `NM_Occupation`, `NM_ProfileImage`, `NUM_RateRegular`, `NUM_RateOnline`, `NUM_Experience`, `NM_Bio`, `NM_MaritalStatus`, `NM_EducationalBackground`, `NM_Grade`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`)
VALUES
	('admin','admin@mentorhuis.com','Administrator',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-08-17 00:00:00',NULL,NULL,NULL,NULL),
	('rolassimanjuntak@gmail.com','rolassimanjuntak@gmail.com','Rolas','Simanjuntak','rolassim',NULL,'2020-08-29','085359867032','DEV','ACEH','KABUPATEN SIMEULUE',NULL,'Budak Birokrat','rolassimanjuntak-2020-09-13143945.jpeg',NULL,NULL,2,NULL,'BELUM MENIKAH','[{\"Grade\":\"DOKTOR\",\"TahunLulus\":\"2010\",\"Institusi\":\"UI\"}]','MUDA','2020-08-29 00:00:00',1,NULL,NULL,NULL),
	('xenomega666@gmail.com','xenomega666@gmail.com','Pengguna',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-13 00:00:00',NULL,NULL,NULL,NULL),
	('yoelrolas@gmail.com','yoelrolas@gmail.com','Yoel R','Simanjuntak','yoelrolas',NULL,'2020-06-25','085359867032','Medan','SUMATERA UTARA','KOTA MEDAN',NULL,'Budak Birokrat','pp-yoelrolas.png',NULL,NULL,0,NULL,'BELUM MENIKAH',NULL,NULL,'2020-06-20 00:00:00',1,1,NULL,'105046270141408552981');

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2020-09-13 15:47:31','::1'),
	('rolassimanjuntak@gmail.com','e10adc3949ba59abbe56e057f20f883e',5,0,'2020-10-17 11:08:34','::1'),
	('xenomega666@gmail.com','e10adc3949ba59abbe56e057f20f883e',5,1,NULL,NULL),
	('yoelrolas@gmail.com','105046270141408552981',2,0,'2020-09-13 15:42:07','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mbank
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mbank`;

CREATE TABLE `mbank` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_Bank` varchar(50) NOT NULL DEFAULT '',
  `KD_Prefix` varchar(50) NOT NULL DEFAULT '',
  `NM_Bank` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mbank` WRITE;
/*!40000 ALTER TABLE `mbank` DISABLE KEYS */;

INSERT INTO `mbank` (`Uniq`, `KD_Bank`, `KD_Prefix`, `NM_Bank`)
VALUES
	(1,'BNI','','Bank Negara Indonesia'),
	(2,'BCA','','Bank Central Asia'),
	(3,'BRI','','Bank Rakyat Indonesia'),
	(4,'Mandiiri','','Bank Mandiri');

/*!40000 ALTER TABLE `mbank` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mdays
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mdays`;

CREATE TABLE `mdays` (
  `KD_Day` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NM_Day` varchar(50) DEFAULT NULL,
  `NM_DayIntl` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`KD_Day`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mdays` WRITE;
/*!40000 ALTER TABLE `mdays` DISABLE KEYS */;

INSERT INTO `mdays` (`KD_Day`, `NM_Day`, `NM_DayIntl`)
VALUES
	(0,'Minggu','Sunday'),
	(1,'Senin','Monday'),
	(2,'Selasa','Tuesday'),
	(3,'Rabu','Wednesday'),
	(4,'Kamis','Thursday'),
	(5,'Jumat','Friday'),
	(6,'Sabtu','Saturday');

/*!40000 ALTER TABLE `mdays` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpartner
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpartner`;

CREATE TABLE `mpartner` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NM_Partner` varchar(200) NOT NULL DEFAULT '',
  `NM_LogoImage` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpartner` WRITE;
/*!40000 ALTER TABLE `mpartner` DISABLE KEYS */;

INSERT INTO `mpartner` (`Uniq`, `NM_Partner`, `NM_LogoImage`)
VALUES
	(2,'Pemerintah Kabupaten Karo','pemerintah-kabupaten-karo-2020-05-30181116.png'),
	(3,'PT Indah Kiat Pulp & Water','pt-indah-kiat-pulp-water-2020-05-30181134.png'),
	(5,'PT Kwarsa Hexagon','pt-kwarsa-hexagon-2020-05-30181256.png'),
	(6,'SMP Dian Harapan','smp-dian-harapan-2020-06-05111432.png'),
	(25,'PT Teodore Pan Garmindo','pt-teodore-pan-garmindo-2020-06-05111341.png'),
	(28,'SMP BPK Penabur Holis Bandung','smp-bpk-penabur-holis-bandung-2020-06-05110009.png'),
	(30,'PT. PLN Persero','pt-pln-persero-2020-06-05114433.png'),
	(31,'Kementerian Hukum dan Hak Asasi Manusia','kementerian-hukum-dan-hak-asasi-manusia-2020-06-05114619.png'),
	(32,'Komisi Pemilihan Umum','komisi-pemilihan-umum-2020-06-05114648.png'),
	(33,'Mahkamah Konstitusi','mahkamah-konstitusi-2020-06-05114710.png'),
	(34,'Bank Indonesia','bank-indonesia-2020-06-05114748.png'),
	(35,'Bank OCBC NISP','bank-ocbc-nisp-2020-06-05114817.png'),
	(36,'Bina Nusantara University','bina-nusantara-university-2020-06-05114910.png'),
	(37,'Universitas Sumatera Utara','universitas-sumatera-utara-2020-06-05114934.png'),
	(38,'Universitas Surabaya','universitas-surabaya-2020-06-05114948.png');

/*!40000 ALTER TABLE `mpartner` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mquestion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mquestion`;

CREATE TABLE `mquestion` (
  `KD_Question` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NM_Question` text,
  `NM_Aspect` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KD_Question`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mquestion` WRITE;
/*!40000 ALTER TABLE `mquestion` DISABLE KEYS */;

INSERT INTO `mquestion` (`KD_Question`, `NM_Question`, `NM_Aspect`)
VALUES
	(1,'Saya melakukan olah raga secara teratur.','FISIK'),
	(2,'Saya mampu mengelola pola makan yang sehat.','FISIK'),
	(3,'Saya memiliki pola tidur yang cukup dan teratur.','FISIK'),
	(4,'Saya memiliki waktu untuk rekreasi atau rileks.','FISIK'),
	(5,'Saya melakukan pemeriksaan kesehatan secara berkala.','FISIK'),
	(6,'Saya dapat mengelola tekanan-tekanan kehidupan dan dapat berkonsentrasi.','EMOSI'),
	(7,'Saya mampu menghayati perasaan orang lain.','EMOSI'),
	(8,'Saya dapat mengelola emosi negatif secara efektif.','EMOSI'),
	(9,'Saya mengenali dan mengelola kekuatan dan kelemahan pribadi saya.','EMOSI'),
	(10,'Saya dapat mengungkapkan pendapat dan perasaan saya dengan nyaman tanpa takut orang lain tersinggung.','EMOSI'),
	(11,'Saya membina hubungan dengan keluarga besar, tetangga maupun teman kerja.','SOSIAL'),
	(12,'Saya menikmati berbagi suka-duka dengan teman-teman yang dapat saya percaya.','SOSIAL'),
	(13,'Saya mampu mengelola konflik antar pribadi dengan orang lain.','SOSIAL'),
	(14,'Saya aktif berkontribusi dalam lingkungan sosial saya.','SOSIAL'),
	(15,'Lingkungan sosial saya menerima saya dan memberikan perhatian pada saya.','SOSIAL'),
	(16,'Saya berkomunikasi dengan sehat dan teratur, saling menerima dan memberi masukan dengan pasangan saya.','KELUARGA'),
	(17,'Bersama pasangan, saya menikmati pemenuhan kebutuhan dasar suami-istri.','KELUARGA'),
	(18,'Saya mendampingi anak-anak bertumbuh dan berkembang.','KELUARGA'),
	(19,'Keluarga (pasangan & anak) mendukung apa yang saya kerjakan.','KELUARGA'),
	(20,'Kondisi keuangan saya memadai untuk mendukung kehidupan keluarga (termasuk pendidikan anak, asuransi kesehatan dan keluarga besar).\r\n','KELUARGA'),
	(21,'Saya menikmati dan memelihara dengan teratur hubungan pribadi dengan Tuhan, Sang Pencipta.','SPIRITUAL'),
	(22,'Saya dengan sadar sedang mentaati perintah Tuhan dalam sikap, kata dan perbuatan saya.','SPIRITUAL'),
	(23,'Saya berpegang pada ajaran Kitab Suci yang spesifik dalam menjalani baik kehidupan saya.','SPIRITUAL'),
	(24,'Saya memiliki komunitas rohani yang saling menopang dan saling berbagi dalam perjalanan hidup.','SPIRITUAL'),
	(25,'Saya membagikan pengalaman dan keyakinan iman saya dengan sederhana, tulus, dan apa adanya. ','SPIRITUAL'),
	(26,'Saya yakin bahwa pekerjaan saya saat ini sesuai dengan panggilan (calling) saya.','PEKERJAAN'),
	(27,'Saya memiliki kejelasan akan arah karir yang lebih baik di masa depan.','PEKERJAAN'),
	(28,'Saya menentukan prioritas pekerjaan saya dan menjalankannya secara mantap.','PEKERJAAN'),
	(29,'Saya merasa puas dengan perkembangan pekerjaan saya saat ini.','PEKERJAAN'),
	(30,'Saya memilki teman kerja yang bisa dijadikan teman berdiskusi, berbagi dan saling mendorong untuk maju.','PEKERJAAN');

/*!40000 ALTER TABLE `mquestion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mrates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mrates`;

CREATE TABLE `mrates` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `NUM_ExpFrom` int(10) NOT NULL,
  `NUM_ExpTo` int(10) NOT NULL,
  `NUM_RateRegular` double NOT NULL,
  `NUM_RateOnline` double NOT NULL,
  `NUM_RateGroup` double NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table mtype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mtype`;

CREATE TABLE `mtype` (
  `KD_Type` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_Category` varchar(10) DEFAULT NULL,
  `NM_Type` varchar(50) DEFAULT NULL,
  `NM_Desc` varchar(200) DEFAULT NULL,
  `NM_ColorMain` varchar(50) DEFAULT NULL,
  `NM_ColorSecondary` varchar(50) DEFAULT NULL,
  `NM_Thumbnail` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KD_Type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mtype` WRITE;
/*!40000 ALTER TABLE `mtype` DISABLE KEYS */;

INSERT INTO `mtype` (`KD_Type`, `KD_Category`, `NM_Type`, `NM_Desc`, `NM_ColorMain`, `NM_ColorSecondary`, `NM_Thumbnail`)
VALUES
	(1,'PERSONAL','Kesehatan Mental','Asesmen dan intervensi masalah-masalah kesehatan mental.','#007BFF',NULL,'_kesehatan-mental.png'),
	(2,'PERSONAL','Pernikahan','Asesmen dan intervensi masalah-masalah keluarga.','#E83E8C',NULL,'_pernikahan.png'),
	(3,'PERSONAL','Pengembangan Karir','Asesmen dan intervensi masalah-masalah pemilihan & pengembangan karir.','#28A745',NULL,'_pengembangan-karir.png'),
	(4,'PERSONAL','Pendidikan','Asesmen dan intervensi masalah-masalah pendidikan peserta didik.','#17A2B8','','_pendidikan.png'),
	(5,'PERSONAL','Komunikasi','Asesmen dan mentoring untuk masalah komunikasi dalam kehidupan sosial.','#FFC107',NULL,'_komunikasi.png'),
	(6,'PERSONAL','Spiritual','Asesmen untuk mengetahui hubungan pribadi dengan Tuhan YME baik secara umum maupun agama/kepercayaan yang dianut.','#6610F2',NULL,'_spiritual.png'),
	(7,'PERSONAL','Parenting','Asesmen dan mentoring tentang hubungan antara orang tua dan anak.','#3D9970',NULL,'_parenting.png'),
	(8,'PERSONAL','Pasangan Hidup','Mentoring tentang hal pasangan hidup dan hubungan asmara.','#F012BE',NULL,'_pasangan-hidup.png'),
	(9,'COMMUNITY','Pengembangan Organisasi','Asesmen dan intervensi masalah-masalah organisasi.','#007BFF',NULL,'_pengembangan-organisasi.png'),
	(10,'CORPORATE','Rekrutmen Karyawan','Asesmen untuk menyeleksi kandidat karyawan dari luar perusahaan.','#E83E8C',NULL,'_rekrutmen-karyawan.png'),
	(11,'CORPORATE','Promosi Jabatan','Asesmen untuk memilih kandidat karyawan internal untuk dipromosikan ke jabatan yang lebih tinggi.','#28A745',NULL,'_promosi-jabatan.png'),
	(12,'CORPORATE','Mutasi Karyawan','Asesmen untuk memilih kandidat karyawan internal untuk dimutasikan ke posisi lainnya yang tepati.','#17A2B8',NULL,'_mutasi-karyawan.png'),
	(13,'CORPORATE','Pengembangan Kapasitas','Asesmen untuk memahami performa kinerja karyawan.','#FFC107',NULL,'_capacity-building.png');

/*!40000 ALTER TABLE `mtype` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tsession
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tsession`;

CREATE TABLE `tsession` (
  `KD_Session` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_SessionVia` char(10) NOT NULL,
  `KD_Type` bigint(10) unsigned NOT NULL,
  `KD_Status` char(50) DEFAULT NULL,
  `IS_CareGroup` tinyint(1) NOT NULL DEFAULT '0',
  `NM_SessionHost` varchar(50) NOT NULL,
  `DATE_SessionDate` date NOT NULL,
  `DATE_SessionTime` char(10) DEFAULT NULL,
  `NUM_Quota` double NOT NULL DEFAULT '1',
  `NUM_Rate` double NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`KD_Session`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tsession` WRITE;
/*!40000 ALTER TABLE `tsession` DISABLE KEYS */;

INSERT INTO `tsession` (`KD_Session`, `KD_SessionVia`, `KD_Type`, `KD_Status`, `IS_CareGroup`, `NM_SessionHost`, `DATE_SessionDate`, `DATE_SessionTime`, `NUM_Quota`, `NUM_Rate`, `CreatedBy`, `CreatedOn`)
VALUES
	(6,'REGULER',5,'SELESAI',0,'rolassimanjuntak@gmail.com','2020-09-21','07:00',1,100000,'yoelrolas@gmail.com','2020-09-13 15:42:23'),
	(7,'REGULER',5,'SELESAI',0,'rolassimanjuntak@gmail.com','2020-09-21','08:00',1,100000,'yoelrolas@gmail.com','2020-09-13 15:46:32');

/*!40000 ALTER TABLE `tsession` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tsession_entry
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tsession_entry`;

CREATE TABLE `tsession_entry` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_Session` bigint(11) unsigned NOT NULL,
  `KD_StatusPayment` char(50) NOT NULL DEFAULT '',
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `NM_RemarksClientFeeling` text,
  `NM_RemarksClientCondition` text,
  `NM_RemarksClientExpectation` text,
  `NM_RemarksMentor` text,
  `NM_PaymentImage` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tsession_entry` WRITE;
/*!40000 ALTER TABLE `tsession_entry` DISABLE KEYS */;

INSERT INTO `tsession_entry` (`Uniq`, `KD_Session`, `KD_StatusPayment`, `UserName`, `NM_RemarksClientFeeling`, `NM_RemarksClientCondition`, `NM_RemarksClientExpectation`, `NM_RemarksMentor`, `NM_PaymentImage`, `CreatedBy`, `CreatedOn`)
VALUES
	(6,6,'LUNAS','yoelrolas@gmail.com','','','','GOODJOB..','MH-000061.jpeg','yoelrolas@gmail.com','2020-09-13 15:42:23'),
	(7,7,'LUNAS','yoelrolas@gmail.com','','','',NULL,'MH-00007.jpeg','yoelrolas@gmail.com','2020-09-13 15:46:32');

/*!40000 ALTER TABLE `tsession_entry` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tsession_feedback
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tsession_feedback`;

CREATE TABLE `tsession_feedback` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_Session` bigint(10) unsigned DEFAULT NULL,
  `KD_FeedbackType` varchar(50) NOT NULL DEFAULT '',
  `NM_FeedbackText` text,
  `NM_FeedbackName` varchar(50) DEFAULT NULL,
  `NM_FeedbackOcupation` varchar(50) DEFAULT NULL,
  `NM_FeedbackAge` varchar(50) DEFAULT NULL,
  `NUM_Rate` double DEFAULT NULL,
  `IS_DisplayHomepage` tinyint(4) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tsession_feedback` WRITE;
/*!40000 ALTER TABLE `tsession_feedback` DISABLE KEYS */;

INSERT INTO `tsession_feedback` (`Uniq`, `KD_Session`, `KD_FeedbackType`, `NM_FeedbackText`, `NM_FeedbackName`, `NM_FeedbackOcupation`, `NM_FeedbackAge`, `NUM_Rate`, `IS_DisplayHomepage`, `CreatedBy`, `CreatedOn`)
VALUES
	(4,NULL,'','Mantull gan..','Partopi Tao','Freelancer','32',NULL,1,'admin','2020-09-13 15:30:17'),
	(9,6,'RATE','Goodjob','yoelrolas','Budak Birokrat',NULL,4,NULL,'yoelrolas@gmail.com','2020-09-13 15:44:35'),
	(10,7,'RATE','Goodjob','yoelrolas','Budak Birokrat',NULL,5,NULL,'yoelrolas@gmail.com','2020-09-13 15:44:35');

/*!40000 ALTER TABLE `tsession_feedback` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tsession_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tsession_log`;

CREATE TABLE `tsession_log` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KD_Session` bigint(10) unsigned NOT NULL,
  `KD_LogType` varchar(50) NOT NULL DEFAULT '',
  `NM_LogDesc` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table userbill
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userbill`;

CREATE TABLE `userbill` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `KD_Bank` varchar(50) NOT NULL DEFAULT '',
  `NM_AccountName` varchar(50) NOT NULL DEFAULT '',
  `NM_AccountNo` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Uniq`),
  KEY `FK_Userbill_User` (`UserName`),
  CONSTRAINT `FK_Userbill_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table userschedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userschedule`;

CREATE TABLE `userschedule` (
  `Uniq` bigint(10) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL,
  `KD_ScheduleType` varchar(10) NOT NULL DEFAULT '',
  `KD_ScheduleDay` bigint(10) unsigned DEFAULT NULL,
  `DATE_ScheduleDate` date DEFAULT NULL,
  `DATE_ScheduleTime_From` varchar(10) DEFAULT NULL,
  `DATE_ScheduleTime_To` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_UserSchedule_User` (`UserName`),
  CONSTRAINT `FK_UserSchedule_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `userschedule` WRITE;
/*!40000 ALTER TABLE `userschedule` DISABLE KEYS */;

INSERT INTO `userschedule` (`Uniq`, `UserName`, `KD_ScheduleType`, `KD_ScheduleDay`, `DATE_ScheduleDate`, `DATE_ScheduleTime_From`, `DATE_ScheduleTime_To`)
VALUES
	(107,'rolassimanjuntak@gmail.com','FIXED',1,NULL,'07:00','08:00'),
	(108,'rolassimanjuntak@gmail.com','FIXED',1,NULL,'08:00','09:00');

/*!40000 ALTER TABLE `userschedule` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userstatus`;

CREATE TABLE `userstatus` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) DEFAULT NULL,
  `Status` text,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_UserStatus_User` (`UserName`),
  CONSTRAINT `FK_UserStatus_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table usertype
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usertype`;

CREATE TABLE `usertype` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UserName` varchar(50) NOT NULL DEFAULT '',
  `KD_Type` bigint(10) unsigned NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_UserType` (`UserName`),
  CONSTRAINT `FK_UserType` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `usertype` WRITE;
/*!40000 ALTER TABLE `usertype` DISABLE KEYS */;

INSERT INTO `usertype` (`Uniq`, `UserName`, `KD_Type`)
VALUES
	(98,'rolassimanjuntak@gmail.com',5);

/*!40000 ALTER TABLE `usertype` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
