<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

/*require APPPATH.'/third_party/google-api-client/vendor/autoload.php';
set_include_path(APPPATH.'third_party/google-api-client/src/'.PATH_SEPARATOR.get_include_path());
require_once APPPATH.'third_party/google-api-client/src/Google/Client.php';*/
include_once FCPATH."vendor/autoload.php";

class Google extends Google_Client {
	function __construct() {
		parent::__construct();
		$this->setAuthConfigFile(FCPATH.'google_client_secret.json');
		//$this->setRedirectUri(MY_BASEURL.'user/google-auth/');
		$this->setScopes(array(
		"https://www.googleapis.com/auth/plus.login",
		"https://www.googleapis.com/auth/userinfo.email",
		"https://www.googleapis.com/auth/userinfo.profile",
		"https://www.googleapis.com/auth/plus.me",
		));
	}
}
