<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />',
      anchor('admin/partner/edit/'.$d[COL_UNIQ],$d[COL_NM_PARTNER],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_NM_PARTNER], 'data-logo'=>$d[COL_NM_LOGOIMAGE])),
      anchor(MY_UPLOADURL.$d[COL_NM_LOGOIMAGE], '<img src="'.MY_UPLOADURL.$d[COL_NM_LOGOIMAGE].'" style="max-height: 6vh" />',array('target' => '_blank'))
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
          <?=anchor('admin/partner/delete','<i class="fas fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/partner/add'.(!empty($role)?'/'.$role:''),'<i class="fas fa-plus"></i> Tambah',array('class'=>'btn btn-primary btn-sm modal-popup'))?>
        </p>
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor" method="post" action="#">
          <div class="form-group text-center">
            <img id="logo" src="" style="max-width: 25%" />
          </div>
          <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="<?=COL_NM_PARTNER?>" required />
          </div>
          <div class="form-group">
            <label>Logo</label>
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fad fa-image"></i></span>
              </div>
              <div class="custom-file">
                <input id="logo" type="file" class="custom-file-input" name="logo" accept="image/*">
                <label class="custom-file-label" for="logo">Upload Logo</label>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-ok">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "asc" ]],
    "aoColumns": [
      {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
      {"sTitle": "Nama",},
      {"sTitle": "Logo","bSortable":false}
    ]
  });
  $('#cekbox').click(function() {
    if($(this).is(':checked')) {
      $('.cekbox').prop('checked',true);
    } else {
      $('.cekbox').prop('checked',false);
    }
  });

  $('.modal-popup, .modal-popup-edit').click(function(){
    var a = $(this);
    var name = $(this).data('name');
    var logo = $(this).data('logo');
    var editor = $("#modal-editor");

    $('[name=<?=COL_NM_PARTNER?>]', editor).val(name);
    $('#logo', editor).attr('src', '<?=MY_UPLOADURL?>'+logo);
    editor.modal("show");
    $(".btn-ok", editor).unbind('click').click(function() {
      var dis = $(this);
      var dishtml = dis.html();
      dis.html("Loading...").attr("disabled", true);
      $('#form-editor').ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            window.location.reload();
          }else{
            toastr.error(data.error);
          }
        },
        complete: function() {
          dis.html(dishtml).attr("disabled", false);
        }
      });
    });
    return false;
  });
});
</script>
