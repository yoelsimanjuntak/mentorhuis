<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light"><i class="fas fa-cog"></i>&nbsp;Pengaturan Dasar</h5>
          </div>
          <?=form_open(site_url('admin/dashboard/main-edit'),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="col-sm-6">
              <div class="form-group row">
                <label  class="control-label col-sm-3 text-left">Link Facebook</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="<?=SETTING_ORG_LINKFACEBOOK?>" value="<?=!empty($data[SETTING_ORG_LINKFACEBOOK]) ? $data[SETTING_ORG_LINKFACEBOOK] : $this->setting_org_linkfacebook?>" />
                </div>
              </div>
              <div class="form-group row">
                <label  class="control-label col-sm-3 text-left">Link Twitter</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="<?=SETTING_ORG_LINKTWITTER?>" value="<?=!empty($data[SETTING_ORG_LINKTWITTER]) ? $data[SETTING_ORG_LINKTWITTER] : $this->setting_org_linktwitter?>" />
                </div>
              </div>
              <div class="form-group row">
                <label  class="control-label col-sm-3 text-left">Link Instagram</label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="<?=SETTING_ORG_LINKINSTAGRAM?>" value="<?=!empty($data[SETTING_ORG_LINKINSTAGRAM]) ? $data[SETTING_ORG_LINKINSTAGRAM] : $this->setting_org_linkinstagram?>" />
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <button type="submit" class="btn btn-outline-pallete-1"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light"><i class="fas fa-bank"></i>&nbsp;Pengaturan Rekening</h5>
          </div>
          <?=form_open(site_url('admin/dashboard/bill-edit'),array('role'=>'form','id'=>'form-bill','class'=>'form-horizontal'))?>
          <div class="card-body p-0">
            <div class="form-group mb-0">
              <input type="hidden" name="Bills" />
              <div class="row">
                <div class="col-sm-12">
                  <table id="tbl-bill" class="table table-bordered mb-0">
                    <tbody>
                      <?php
                      if(count($rBills)  > 0) {
                        foreach($rBills as $s) {
                          ?>
                          <tr>
                            <td><?=$s[COL_KD_BANK].'<br />'.$s[COL_NM_ACCOUNTNO].' an. '.$s[COL_NM_ACCOUNTNAME]?></td>
                            <td class="text-center">
                              <button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button>
                              <input type="hidden" name="idx" value="<?=$s[COL_KD_BANK]?>" />
                            </td>
                          </tr>
                          <?php
                        }
                      } else {
                        ?>
                        <tr>
                          <td colspan="2">
                            <p class="font-italic mb-0">Data masih kosong.</p>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th style="white-space: nowrap">
                          <div class="row">
                            <div class="col-sm-6 p-2">
                              <select name="<?=COL_KD_BANK?>" class="form-control" style="min-width: 10vw">
                                <?=GetCombobox("SELECT * from mbank order by KD_Bank", COL_KD_BANK, COL_KD_BANK, null, true, false, '-- Bank --')?>
                              </select>
                            </div>
                            <div class="col-sm-6 p-2">
                              <input type="text" class="form-control" name="<?=COL_NM_ACCOUNTNO?>" placeholder="No. Rekening" />
                            </div>
                            <div class="col-sm-12 p-2">
                              <input type="text" class="form-control" name="<?=COL_NM_ACCOUNTNAME?>" placeholder="Nama Rekening" />
                            </div>
                          </div>
                        </th>
                        <th class="text-center">
                          <div class="row">
                            <div class="col-sm-12 p-2">
                              <button type="button" id="btn-add-bill" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                            </div>
                          </div>

                        </th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <button type="submit" class="btn btn-outline-pallete-1"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light"><i class="fas fa-usd"></i>&nbsp;Pengaturan Tarif</h5>
          </div>
          <?=form_open(site_url('admin/dashboard/rate-edit'),array('role'=>'form','id'=>'form-bill','class'=>'form-horizontal'))?>
          <div class="card-body p-0">
            <div class="form-group mb-0">
              <input type="hidden" name="Bills" />
              <div class="row">
                <div class="col-sm-12">
                  <table id="tbl-rates" class="table table-bordered mb-0">
                    <tbody>
                      <tr>
                        <td>Grade</td>
                        <td>Reguler</td>
                        <td>Online</td>
                        <td>Care Group</td>
                      </tr>
                      <?php
                      if(count($rRates)  > 0) {
                        $i=0;
                        foreach($rRates as $s) {
                          ?>
                          <tr>
                            <td class="text-left font-weight-bold"><input type="hidden" name="<?=COL_NM_GRADE?>[<?=$i?>]" value="<?=$s[COL_NM_GRADE]?>" /><?=$s[COL_NM_GRADE]?></td>
                            <td>
                              <input type="text" class="form-control uang text-right" name="<?=COL_NUM_RATEREGULAR?>[<?=$i?>]" value="<?=$s[COL_NUM_RATEREGULAR]?>" />
                            </td>
                            <td>
                              <input type="text" class="form-control uang text-right" name="<?=COL_NUM_RATEONLINE?>[<?=$i?>]" value="<?=$s[COL_NUM_RATEONLINE]?>" />
                            </td>
                            <td>
                              <input type="text" class="form-control uang text-right" name="<?=COL_NUM_RATEGROUP?>[<?=$i?>]" value="<?=$s[COL_NUM_RATEGROUP]?>" />
                            </td>
                          </tr>
                          <?php
                          $i++;
                        }
                      } else {
                        ?>
                        <tr>
                          <td colspan="2">
                            <p class="font-italic mb-0">Data masih kosong.</p>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer text-center">
            <button type="submit" class="btn btn-outline-pallete-1"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function() {
  $('form').each(function() {
    $(this).validate({
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
              if(res.data && res.data.redirect) {
                setTimeout(function(){
                  location.href = res.data.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
          }
        });
        return false;
      }
    });
  });

  $('[name=Bills]').change(function() {
    writeBill('tbl-bill', 'Bills');
  }).val(encodeURIComponent('<?=$data['Bills']?>')).trigger('change');

  $('#btn-add-bill', $('#tbl-bill')).click(function() {
    var dis = $(this);
    var arr = $('[name=Bills]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var kdBank = $('[name=KD_Bank]', row).val();
    var nmAccountNo = $('[name=NM_AccountNo]', row).val();
    var nmAccountName = $('[name=NM_AccountName]', row).val();
    if(kdBank && nmAccountNo && nmAccountName) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Bank == kdBank;
      });
      if(exist.length == 0) {
        arr.push({'Bank': kdBank, 'AccountNo': nmAccountNo, 'AccountName':nmAccountName});
        console.log(arr);
        $('[name=Bills]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi informasi bank dengan benar.');
    }
  });
});

function writeBill(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return a['Bank'].localeCompare(b['Bank']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].Bank+'<br />'+arr[i].AccountNo+' an. '+arr[i].AccountName+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Bank+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Bank != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
