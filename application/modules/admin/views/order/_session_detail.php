<table class="table table-striped">
  <tbody>
    <tr>
      <td style="width: 10vw">Mentor</td><td style="width: 2vw">:</td><td><?=$sesi['Mentor_FirstName'].' '.$sesi['Mentor_LastName']?></td>
    </tr>
    <tr>
      <td style="width: 10vw">Layanan</td><td style="width: 2vw">:</td><td><?=$sesi[COL_NM_TYPE]?></td>
    </tr>
    <tr>
      <td style="width: 10vw">Tanggal / Jam</td><td style="width: 2vw">:</td><td><?=date('Y-m-d', strtotime($sesi[COL_DATE_SESSIONDATE])).' '.$sesi[COL_DATE_SESSIONTIME]?></td>
    </tr>
    <tr>
      <td style="width: 10vw">Format</td><td style="width: 2vw">:</td><td><?=$sesi[COL_KD_SESSIONVIA].($sesi[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2">CARE GROUP</small>':'')?></td>
    </tr>
    <tr>
      <td style="width: 10vw">Status</td><td style="width: 2vw">:</td><td><?=$sesi[COL_KD_STATUS]?></td>
    </tr>
    <tr>
      <td style="width: 10vw">Biaya</td><td style="width: 2vw">:</td>
      <td>
        Rp. <strong><?=number_format($sesi[COL_NUM_RATE])?></strong>
      </td>
    </tr>
  </tbody>
</table>
