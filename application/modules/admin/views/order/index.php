<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      $stat == SESSIONPAY_WAITING ? anchor(site_url('site/ajax/session-change-status/'.SESSIONPAY_DECLINED.'/'.$d[COL_UNIQ].'/1'), '<i class="fas fa-times"></i>&nbsp;BATALKAN', array('class'=>'btn btn-xs btn-danger btn-change-status')).'&nbsp'.
      anchor(site_url('site/ajax/session-change-status/'.SESSIONPAY_VERIFIED.'/'.$d[COL_UNIQ]), '<i class="fas fa-check"></i>&nbsp;VERIFIKASI', array('class'=>'btn btn-xs btn-success btn-change-status')) : '-',
      date('Y-m-d H:i:s', strtotime($d[COL_CREATEDON])),
      anchor(site_url('admin/order/get-session-detail/'.$d[COL_KD_SESSION]), 'MH-'.str_pad($d[COL_KD_SESSION], 5, '0', STR_PAD_LEFT), array('class'=>'link-detail')),
      anchor(site_url('site/ajax/get-member-detail/'.GetEncryption($d[COL_USERNAME])), $d[COL_NM_FIRSTNAME].' '.$d[COL_NM_LASTNAME], array('class'=>'link-profile')),
      number_format($d[COL_NUM_RATE]),
      !empty($d[COL_NM_PAYMENTIMAGE])?anchor(MY_UPLOADURL.$d[COL_NM_PAYMENTIMAGE], $d[COL_NM_PAYMENTIMAGE], array('class'=>'link-transfer-image')):'<span class="text-danger"></span>',
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-payment" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Pembayaran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Detail</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body p-0">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-profile" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Profil</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalDetail = $('#modal-detail');
  var modalProfile = $('#modal-profile');
  var modalPayment = $('#modal-payment');
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "desc" ]],
    "columnDefs": [{targets: [0], class:"text-center"}, {targets: [4], class:"text-right"}],
    "aoColumns": [
      {"sTitle": "Opsi", "sWidth":"120px", "bSortable": false},
      {"sTitle": "Timestamp", "sWidth":"60px"},
      {"sTitle": "No. Sesi"},
      {"sTitle": "Nama"},
      {"sTitle": "Rp.", "bSortable": false},
      {"sTitle": "Lampiran"}
    ]
  });
  $('.btn-change-status').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.post(url, function(data) {
        if(data.error == 0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      }, "json").fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });
  $('.link-detail').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalDetail).load(url, function() {
      modalDetail.modal('show');
    });
    return false;
  });
  $('.link-profile').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalProfile).load(url, function() {
      modalProfile.modal('show');
    });
    return false;
  });
  $('.link-transfer-image').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalPayment).html('<p class="text-center"><img src="'+url+'" style="max-width: 100%" /></p>');
    modalPayment.modal('show');
    return false;
  });
});
</script>
