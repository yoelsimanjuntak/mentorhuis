<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />',
      !empty($d[COL_KD_SESSION]) ? str_pad($d[COL_KD_SESSION], 5, '0', STR_PAD_LEFT) : '-',
      ($d[COL_IS_DISPLAYHOMEPAGE]==1?'<i class="fas fa-check-square text-success"></i>':'<i class="fas fa-times-square text-danger"></i>').'&nbsp;'.anchor('admin/feedback/edit/'.$d[COL_UNIQ],$d[COL_NM_FEEDBACKNAME],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_NM_FEEDBACKNAME], 'data-age'=>$d[COL_NM_FEEDBACKAGE], 'data-ocupation'=>$d[COL_NM_FEEDBACKOCUPATION], 'data-text'=>$d[COL_NM_FEEDBACKTEXT])),
      strlen($d[COL_NM_FEEDBACKTEXT]) > 100 ? substr($d[COL_NM_FEEDBACKTEXT], 0, 100).'...' : $d[COL_NM_FEEDBACKTEXT],
      date('Y-m-d H:i:s', strtotime($d[COL_CREATEDON]))
    );
    $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
          <?=anchor('admin/feedback/delete','<i class="fas fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/feedback/add'.(!empty($role)?'/'.$role:''),'<i class="fas fa-plus"></i> Tambah',array('class'=>'btn btn-primary btn-sm modal-popup'))?>
          <?=anchor('admin/feedback/publish/1','<i class="fas fa-check"></i> Publish',array('class'=>'cekboxaction btn btn-success btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/feedback/publish/0','<i class="fas fa-times"></i> Unpublish',array('class'=>'cekboxaction btn btn-secondary btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
        </p>
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Editor</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor" method="post" action="#">
          <div class="form-group">
            <label>Nama / Jenis Kelamin</label>
            <input type="text" class="form-control" name="<?=COL_NM_FEEDBACKNAME?>" required />
          </div>
          <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" name="<?=COL_NM_FEEDBACKAGE?>" required />
          </div>
          <div class="form-group">
            <label>Pekerjaan</label>
            <input type="text" class="form-control" name="<?=COL_NM_FEEDBACKOCUPATION?>" required />
          </div>
          <div class="form-group">
            <label>Feedback / Testimoni</label>
            <textarea rows="5" name="<?=COL_NM_FEEDBACKTEXT?>" class="form-control"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-ok">Simpan</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 4, "desc" ]],
    "aoColumns": [
      {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
      {"sTitle": "No. Sesi",},
      {"sTitle": "Nama"},
      {"sTitle": "Feedback"},
      {"sTitle": "Tanggal & Waktu"}
    ]
  });
  $('#cekbox').click(function() {
    if($(this).is(':checked')) {
      $('.cekbox').prop('checked',true);
    } else {
      $('.cekbox').prop('checked',false);
    }
  });

  $('.modal-popup, .modal-popup-edit').click(function(){
    var a = $(this);
    var name = $(this).data('name');
    var age = $(this).data('age');
    var ocupation = $(this).data('ocupation');
    var text = $(this).data('text');
    var editor = $("#modal-editor");

    $('[name=<?=COL_NM_FEEDBACKNAME?>]', editor).val(name);
    $('[name=<?=COL_NM_FEEDBACKAGE?>]', editor).val(age);
    $('[name=<?=COL_NM_FEEDBACKOCUPATION?>]', editor).val(ocupation);
    $('[name=<?=COL_NM_FEEDBACKTEXT?>]', editor).val(text);
    editor.modal("show");
    $(".btn-ok", editor).unbind('click').click(function() {
      var dis = $(this);
      var dishtml = dis.html();
      dis.html("Loading...").attr("disabled", true);
      $('#form-editor').ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            window.location.reload();
          }else{
            toastr.error(data.error);
          }
        },
        complete: function() {
          dis.html(dishtml).attr("disabled", false);
        }
      });
    });
    return false;
  });
});
</script>
