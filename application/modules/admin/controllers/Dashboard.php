<?php
class Dashboard extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(IsLogin()) {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home');
      }
    }
  }

  public function login() {
    if(IsLogin()) {
      redirect('admin/dashboard');
    }

    $data['title'] = "Login";
    $rules = array(
      array(
          'field' => 'UserName',
          'label' => 'UserName',
          'rules' => 'required'
      ),
      array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
      )
    );
    $this->form_validation->set_rules($rules);
    if($this->form_validation->run()){
      $this->load->model('muser');
      $this->load->library('si/securimage');
      $username = $this->input->post(COL_USERNAME);
      $password = $this->input->post(COL_PASSWORD);

      /*if($this->securimage->check($this->input->post('Captcha')) != true){
          redirect(site_url('site/user/login')."?msg=captcha");
      }*/

      if($this->muser->authenticate($username, $password)) {
        if($this->muser->IsSuspend($username)) {
          redirect(site_url('admin/dashboard/login')."?msg=suspend");
        }

        // Update Last Login IP
        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

        $userdetails = $this->muser->getdetails($username);
        SetLoginSession($userdetails);
        redirect('admin/dashboard');
      }
      else {
        redirect(site_url('admin/dashboard/login')."?msg=notmatch");
      }
    } else {
      $this->load->view('admin/dashboard/_login', $data);
    }
  }
  public function logout(){
      UnsetLoginSession();
      redirect(site_url());
  }

  public function index() {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }

    $data['title'] = "Dashboard";

    $arrBills = array();
    $rBills = $this->db->get(TBL__SETTINGBILL)->result_array();
    $rRates = $this->db->get(TBL__SETTINGRATE)->result_array();
    foreach($rBills as $s) {
      $arrBills[] = array(
        'Bank' => $s[COL_KD_BANK],
        'AccountNo' => $s[COL_NM_ACCOUNTNO],
        'AccountName' => $s[COL_NM_ACCOUNTNAME]
      );
    }

    $data['rBills'] = $rBills;
    $data['rRates'] = $rRates;
    $data['data']['Bills'] = json_encode($arrBills);
    $this->template->load('main' , 'admin/dashboard/index', $data);
  }

  public function bill_edit() {
    $bills = $this->input->post("Bills");
    $arrBills = array();

    if(!empty($bills)) {
      $bills = json_decode(urldecode($bills));
      foreach($bills as $s) {
        $arrBills[] = array(
          COL_KD_BANK=>$s->Bank,
          COL_NM_ACCOUNTNO=>$s->AccountNo,
          COL_NM_ACCOUNTNAME=>$s->AccountName
        );
      }
    }

    $this->db->trans_begin();
    try {
      $this->db->empty_table(TBL__SETTINGBILL);

      if(!empty($arrBills)) {
        $res = $this->db->insert_batch(TBL__SETTINGBILL, $arrBills);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/dashboard')));
      return;
    } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError('Server Error: '.$e->getMessage());
        return;
    }
  }

  public function rate_edit() {
    $arrRate = array();
    //$rateExpFrom = $this->input->post(COL_NUM_EXPFROM);
    //$rateExpTo = $this->input->post(COL_NUM_EXPTO);
    $rateGrade = $this->input->post(COL_NM_GRADE);
    $rateRegular = $this->input->post(COL_NUM_RATEREGULAR);
    $rateOnline = $this->input->post(COL_NUM_RATEONLINE);
    $rateGroup = $this->input->post(COL_NUM_RATEGROUP);

    for($i = 0; $i<count($rateGrade); $i++) {
      $arrRate[] = array(
        //COL_NUM_EXPFROM => $rateExpFrom[$i],
        //COL_NUM_EXPTO => $rateExpTo[$i],
        COL_NM_GRADE => $rateGrade[$i],
        COL_NUM_RATEREGULAR => $rateRegular[$i],
        COL_NUM_RATEONLINE => $rateOnline[$i],
        COL_NUM_RATEGROUP => $rateGroup[$i]
      );
    }

    $this->db->trans_begin();
    try {
      $this->db->empty_table(TBL__SETTINGRATE);

      if(!empty($arrRate)) {
        $res = $this->db->insert_batch(TBL__SETTINGRATE, $arrRate);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/dashboard')));
      return;
    } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError('Server Error: '.$e->getMessage());
        return;
    }
  }

  public function main_edit()
  {
    SetSetting(SETTING_ORG_LINKFACEBOOK, $this->input->post(SETTING_ORG_LINKFACEBOOK));
    SetSetting(SETTING_ORG_LINKTWITTER, $this->input->post(SETTING_ORG_LINKTWITTER));
    SetSetting(SETTING_ORG_LINKINSTAGRAM, $this->input->post(SETTING_ORG_LINKINSTAGRAM));
    ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/dashboard')));
    return;
  }
}
