<?php
class Order extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('admin/dashboard');
    } else {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home');
      }
    }
  }

  public function index($stat) {
    $stat = urldecode($stat);
    $data['title'] = "Pembayaran - ".$stat;
    $data['stat'] = $stat;
    $data['res'] = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"left")
    ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_KD_SESSION." = ".TBL_TSESSION_ENTRY.".".COL_KD_SESSION,"left")
    ->where(COL_KD_STATUSPAYMENT, $stat)
    ->get(TBL_TSESSION_ENTRY)
    ->result_array();
    $this->template->load('main', 'admin/order/index', $data);
  }

  public function get_session_detail($uniq) {
    $data['sesi'] = $rsesi = $this->db
    ->select(TBL_TSESSION.'.*, mtype.*, mentor.'.COL_NM_FIRSTNAME.' as Mentor_FirstName, mentor.'.COL_NM_LASTNAME.' as Mentor_LastName')
    ->join(TBL__USERINFORMATION.' mentor','mentor.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_NM_SESSIONHOST,"inner")
    ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
    ->where(COL_KD_SESSION, $uniq)
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsesi)) {
      show_error('Sesi tidak valid');
      return;
    }
    $this->load->view('admin/order/_session_detail', $data);
  }
}
?>
