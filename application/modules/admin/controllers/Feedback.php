<?php
class Feedback extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('admin/dashboard');
    } else {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home');
      }
    }
  }

  public function index() {
    $data['title'] = "Feedback & Testimoni";
    $data['res'] = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_FEEDBACK.".".COL_CREATEDBY,"left")
    ->where(COL_KD_FEEDBACKTYPE.' != ','COMMENT')
    ->get(TBL_TSESSION_FEEDBACK)
    ->result_array();
    $this->template->load('main', 'admin/feedback/index', $data);
  }

  public function add() {
    $user = GetLoggedUser();
    if (!empty($_POST)) {
      $data = array(
        COL_NM_FEEDBACKNAME => $this->input->post(COL_NM_FEEDBACKNAME),
        COL_NM_FEEDBACKAGE => $this->input->post(COL_NM_FEEDBACKAGE),
        COL_NM_FEEDBACKOCUPATION => $this->input->post(COL_NM_FEEDBACKOCUPATION),
        COL_NM_FEEDBACKTEXT => $this->input->post(COL_NM_FEEDBACKTEXT),

        COL_CREATEDBY => $user[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_TSESSION_FEEDBACK, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function edit($id) {
    if (!empty($_POST)) {
      $data = array(
        COL_NM_FEEDBACKNAME => $this->input->post(COL_NM_FEEDBACKNAME),
        COL_NM_FEEDBACKAGE => $this->input->post(COL_NM_FEEDBACKAGE),
        COL_NM_FEEDBACKOCUPATION => $this->input->post(COL_NM_FEEDBACKOCUPATION),
        COL_NM_FEEDBACKTEXT => $this->input->post(COL_NM_FEEDBACKTEXT)
      );
      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TSESSION_FEEDBACK, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function delete()
  {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        $this->db->delete(TBL_TSESSION_FEEDBACK, array(COL_UNIQ => $datum));
        $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted." data dihapus");
    } else {
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function publish($opt = 0) {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      if($this->db->where(COL_UNIQ, $datum)->update(TBL_TSESSION_FEEDBACK, array(COL_IS_DISPLAYHOMEPAGE=>$opt))) {
        $deleted++;
      }
    }
    if($deleted){
      ShowJsonSuccess($deleted." data diubah");
    }else{
      ShowJsonError("Tidak ada data yang diubah");
    }
  }
}
?>
