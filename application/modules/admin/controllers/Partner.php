<?php
class Partner extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('admin/dashboard');
    } else {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        redirect('site/home');
      }
    }
  }

  public function index() {
    $data['title'] = "Partner";
    $data['res'] = $this->db
    ->get(TBL_MPARTNER)
    ->result_array();
    $this->template->load('main', 'admin/partner/index', $data);
  }

  public function add() {
    $user = GetLoggedUser();
    if (!empty($_POST)) {
      $data = array(
        COL_NM_PARTNER => $this->input->post(COL_NM_PARTNER)
      );

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size']	= 2048;
      $config['max_width']  = 2048;
      $config['max_height']  = 2048;
      $config['overwrite'] = TRUE;
      if(!empty($_FILES["logo"]["name"])) {
        $pname = slugify($data[COL_NM_PARTNER]);
        if(!empty($pname)) {
          $config['file_name'] = $pname."-".date('Y-m-dHis').".".end((explode(".", $_FILES["logo"]["name"])));
        }

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("logo")){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
          $data[COL_NM_LOGOIMAGE] = $dataupload['file_name'];
          /*if(!empty($ruser[COL_NM_PROFILEIMAGE]) && file_exists(MY_UPLOADPATH.$ruser[COL_NM_PROFILEIMAGE])) {
            unlink(MY_UPLOADPATH.$ruser[COL_NM_PROFILEIMAGE]);
          }*/
        }
      }

      $res = $this->db->insert(TBL_MPARTNER, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        $err = $this->db->error();
        ShowJsonError($err['message']);
      }
    }
  }

  public function edit($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MPARTNER)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Data tidak ditemukan.');
      return false;
    }

    if (!empty($_POST)) {
      $data = array(
        COL_NM_PARTNER => $this->input->post(COL_NM_PARTNER)
      );
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size']	= 500;
      $config['max_width']  = 1024;
      $config['max_height']  = 1024;
      $config['overwrite'] = TRUE;
      if(!empty($_FILES["logo"]["name"])) {
        $pname = slugify($data[COL_NM_PARTNER]);
        if(!empty($pname)) {
          $config['file_name'] = $pname."-".date('Y-m-dHis').".".end((explode(".", $_FILES["logo"]["name"])));
        }

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload("logo")){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
          $data[COL_NM_LOGOIMAGE] = $dataupload['file_name'];
          if(!empty($rdata[COL_NM_LOGOIMAGE]) && file_exists(MY_UPLOADPATH.$rdata[COL_NM_LOGOIMAGE])) {
            unlink(MY_UPLOADPATH.$rdata[COL_NM_LOGOIMAGE]);
          }
        }
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MPARTNER, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function delete()
  {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $rdata = $this->db->where(COL_UNIQ, $datum)->get(TBL_MPARTNER)->row_array();
      if(empty($rdata)) {
        continue;
      }

      if(!empty($rdata[COL_NM_LOGOIMAGE]) && file_exists(MY_UPLOADPATH.$rdata[COL_NM_LOGOIMAGE])) {
        unlink(MY_UPLOADPATH.$rdata[COL_NM_LOGOIMAGE]);
      }
      $this->db->delete(TBL_MPARTNER, array(COL_UNIQ => $datum));
      $deleted++;
    }
    if ($deleted) {
        ShowJsonSuccess($deleted." data dihapus");
    } else {
        ShowJsonError("Tidak ada dihapus");
    }
  }
}
?>
