<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small> Form</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('site/data/forecast-index')?>"> <?=$title?></a></li>
          <li class="breadcrumb-item active"><?=$edit?'Edit':'Add'?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-md-12 mb-2">
        <a href="<?=site_url('site/data/forecast-index')?>" class="btn btn-secondary"><i class="fas fa-chevron-left"></i> KEMBALI</a>
        <button type="submit" class="btn btn-info"><i class="fad fa-save"></i> SIMPAN</button>
      </div>
      <div class="col-sm-12">
        <?php
        if ($this->input->get('error') == 1) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;Data gagal disimpan, silahkan coba kembali.</span>
          </div>
          <?php
        }
        if (validation_errors()) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=validation_errors()?></span>
          </div>
          <?php
        }
        if (isset($err)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp;<?=$err['code'].' : '.$err['message']?></span>
          </div>
          <?php
        }
        if (!empty($upload_errors)) {
            ?>
          <div class="callout callout-danger">
            <span class="text-danger"><?=$upload_errors?></span>
          </div>
          <?php
        }
        ?>
      </div>
      <div class="clearfix"></div>
      <!--<div class="col-sm-12">
        <div class="card card-outline card-secondary">
          <div class="card-header">
            <h4 class="card-title">
              <a href="<?=site_url('site/data/forecast-index')?>" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i> KEMBALI</a>
              <button type="submit" class="btn btn-primary  btn-sm"><i class="fad fa-save"></i> SIMPAN</button>
            </h4>
          </div>
          <div class="card-body">
            <div class="form-group row">
                <label class="control-label col-sm-2">Tanggal</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control datepicker" name="<?=COL_TANGGAL?>" value="<?= $edit ? $data[COL_TANGGAL] : ""?>" required />
                </div>
            </div>
          </div>
        </div>
      </div>-->
      <div class="col-sm-6">
        <div class="card card-outline card-default">
          <div class="card-header">
            <h4 class="card-title">
              <i class="fad fa-calendar-day"></i> HARI INI
            </h4>
          </div>
          <div class="card-body">
            <div class="form-group row">
                <label class="control-label col-sm-3">Tanggal</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control datepicker" name="<?=COL_TANGGAL?>" value="<?= $edit ? $data[COL_TANGGAL] : ""?>" required />
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Peringatan</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" name="FC_Alert" placeholder="00:00 - 12:59" data-inputmask='"mask": "99:99 - 99:99"' value="<?=$edit?(!empty($data[COL_ALERT_FROM])&&!empty($data[COL_ALERT_TO])?$data[COL_ALERT_FROM].' - '.$data[COL_ALERT_TO]:''):''?>" data-mask />
                </div>
                <div class="col-sm-9 offset-sm-3">
                  <textarea class="form-control" rows="3" name="<?=COL_ALERT_WARNING?>" placeholder="Peringatan"><?=$edit?$data[COL_ALERT_WARNING]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Himbauan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" rows="3"  name="<?=COL_ALERT_TEXT?>" placeholder="Himbauan"><?=$edit?$data[COL_ALERT_TEXT]:''?></textarea>
                </div>
            </div>
            <h6 class="text-info b-1"><i class="fad fa-sunrise"></i> PAGI</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FC1_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FC1_KD_WEATHER]) ? $data[COL_FC1_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FC1_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FC1_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fc1[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FC1_IMAGES])) {
                  $arrImg = json_decode($data[COL_FC1_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>

            <h6 class="text-warning b-1"><i class="fad fa-sun"></i> SIANG</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FC2_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FC2_KD_WEATHER]) ? $data[COL_FC2_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FC2_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FC2_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fc2[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FC2_IMAGES])) {
                  $arrImg = json_decode($data[COL_FC2_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>

            <h6 class="text-danger b-1"><i class="fad fa-sunset"></i> SORE</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FC3_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FC3_KD_WEATHER]) ? $data[COL_FC3_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FC3_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FC3_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fc3[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FC3_IMAGES])) {
                  $arrImg = json_decode($data[COL_FC3_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>

            <h6 class="text-gray b-1"><i class="fad fa-moon"></i> MALAM</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FC4_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FC4_KD_WEATHER]) ? $data[COL_FC4_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FC4_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FC4_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fc4[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FC4_IMAGES])) {
                  $arrImg = json_decode($data[COL_FC4_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-default">
          <div class="card-header">
            <h4 class="card-title">
              <i class="fad fa-calendar"></i> BESOK
            </h4>
          </div>
          <div class="card-body">
            <div class="form-group row">
                <label class="control-label col-sm-3">Tanggal</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="Tomorrow" readonly />
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Peringatan</label>
                <div class="col-sm-4">
                  <input type="text" class="form-control" name="FCT_Alert" placeholder="00:00 - 12:59" data-inputmask='"mask": "99:99 - 99:99"' value="<?=$edit?(!empty($data[COL_ALERT_TFROM])&&!empty($data[COL_ALERT_TTO])?$data[COL_ALERT_TFROM].' - '.$data[COL_ALERT_TTO]:''):''?>" data-mask />
                </div>
                <div class="col-sm-9 offset-sm-3">
                  <textarea class="form-control" rows="3" name="<?=COL_ALERT_TWARNING?>" placeholder="Peringatan"><?=$edit?$data[COL_ALERT_TWARNING]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Himbauan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" rows="3"  name="<?=COL_ALERT_TTEXT?>" placeholder="Himbauan"><?=$edit?$data[COL_ALERT_TTEXT]:''?></textarea>
                </div>
            </div>

            <h6 class="text-info b-1"><i class="fad fa-sunrise"></i> PAGI</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FCT1_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FCT1_KD_WEATHER]) ? $data[COL_FCT1_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FCT1_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FCT1_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fct1[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FCT1_IMAGES])) {
                  $arrImg = json_decode($data[COL_FCT1_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>

            <h6 class="text-warning b-1"><i class="fad fa-sun"></i> SIANG</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FCT2_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FCT2_KD_WEATHER]) ? $data[COL_FCT2_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FCT2_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FCT2_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fct2[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FCT2_IMAGES])) {
                  $arrImg = json_decode($data[COL_FCT2_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>

            <h6 class="text-danger b-1"><i class="fad fa-sunset"></i> SORE</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FCT3_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FCT3_KD_WEATHER]) ? $data[COL_FCT3_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FCT3_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FCT3_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fct3[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FCT3_IMAGES])) {
                  $arrImg = json_decode($data[COL_FCT3_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>

            <h6 class="text-gray b-1"><i class="fad fa-moon"></i> MALAM</h6>
            <div class="form-group row">
              <label class="control-label col-sm-3">Cuaca</label>
              <div class="col-sm-9">
                <select name="<?=COL_FCT4_KD_WEATHER?>" class="form-control">
                  <?=GetCombobox("SELECT * FROM bmkg_mweather group by Nm_Weather order by Kd_Weather", COL_KD_WEATHER, COL_NM_WEATHER, (!empty($data[COL_FCT4_KD_WEATHER]) ? $data[COL_FCT4_KD_WEATHER] : null))?>
                </select>
              </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Keterangan</label>
                <div class="col-sm-9">
                  <textarea class="form-control" name="<?=COL_FCT4_DESC?>" rows="3" placeholder="Keterangan"><?=$edit?$data[COL_FCT4_DESC]:''?></textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="control-label col-sm-3">Gambar / Media</label>
                <div class="col-sm-4">
                  <input type="file" name="file_fct4[]" accept="image/*,video/*" multiple="multiple" />
                </div>
                <?php
                if($edit && !empty($data[COL_FCT4_IMAGES])) {
                  $arrImg = json_decode($data[COL_FCT4_IMAGES]);
                  $arrLink = array();
                  foreach($arrImg as $img) {
                    $arrLink[] = anchor(MY_UPLOADURL.$img, $img,array('target'=>'_blank'));
                  }
                  ?>
                  <div class="col-sm-9 offset-sm-3">
                    <p><?=implode(' | ',$arrLink)?></p>
                  </div>
                  <?php
                }
                ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  $('[name=Tanggal]').change(function() {
    if($(this).val()) {
      var tomorrow = moment($(this).val()).add(1, 'days').format('YYYY-MM-DD');
      $('[name=Tomorrow]').val(tomorrow);
    }
  }).trigger('change');
});
</script>
