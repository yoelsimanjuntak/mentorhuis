<form id="dataform-pending" method="post" action="#">
<table class="table table-striped mb-0">
  <thead class="text-center">
    <tr>
      <th>
        <input type="checkbox" class="checkbox-pending" class="" />
      </th>
      <th>Tanggal</th>
      <th>Hari Ini</th>
      <th>Besok</th>
      <th>Oleh</th>
    </tr>
  </thead>
  <tbody class="text-center">
    <?php
    if(!empty($res)) {
      foreach($res as $r) {
        ?>
        <tr>
          <td>
            <input type="checkbox" class="checkbox-pending-item" name="cekbox[]" value="<?=$r[COL_UNIQ]?>" />
          </td>
          <td><?=anchor('site/data/forecast-edit/'.$r[COL_UNIQ], date('Y-m-d', strtotime($r[COL_TANGGAL])))?></td>
          <td>
            <?=
            '<i class="fad fa-'.(!empty($r['Ic_FC1'])?$r['Ic_FC1']:'question-circle').'" title="Pagi: '.(!empty($r['Nm_FC1'])?$r['Nm_FC1']:'').'"></i>&nbsp'.
            '<i class="fad fa-'.(!empty($r['Ic_FC2'])?$r['Ic_FC2']:'question-circle').'" title="Siang: '.(!empty($r['Nm_FC2'])?$r['Nm_FC2']:'').'"></i>&nbsp'.
            '<i class="fad fa-'.(!empty($r['Ic_FC3'])?$r['Ic_FC3']:'question-circle').'" title="Sore: '.(!empty($r['Nm_FC3'])?$r['Nm_FC3']:'').'"></i>&nbsp'.
            '<i class="fad fa-'.(!empty($r['Ic_FC4'])?$r['Ic_FC4']:'question-circle').'" title="Malam: '.(!empty($r['Nm_FC4'])?$r['Nm_FC4']:'').'"></i>'
            ?>
          </td>
          <td>
            <?=
            '<i class="fad fa-'.(!empty($r['Ic_FCT1'])?$r['Ic_FCT1']:'question-circle').'" title="Pagi: '.(!empty($r['Nm_FCT1'])?$r['Nm_FCT1']:'').'"></i>&nbsp'.
            '<i class="fad fa-'.(!empty($r['Ic_FCT2'])?$r['Ic_FCT2']:'question-circle').'" title="Siang: '.(!empty($r['Nm_FCT2'])?$r['Nm_FCT2']:'').'"></i>&nbsp'.
            '<i class="fad fa-'.(!empty($r['Ic_FCT3'])?$r['Ic_FCT3']:'question-circle').'" title="Sore: '.(!empty($r['Nm_FCT3'])?$r['Nm_FCT3']:'').'"></i>&nbsp'.
            '<i class="fad fa-'.(!empty($r['Ic_FCT4'])?$r['Ic_FCT4']:'question-circle').'" title="Malam: '.(!empty($r['Nm_FCT4'])?$r['Nm_FCT4']:'').'"></i>'
            ?>
          </td>
          <td><?=$r[COL_CREATE_BY]?></td>
        </tr>
        <?php
      }
    } else {
      ?>
      <tr>
        <td colspan="5">
          <p>
            Tidak ada data tersedia.
          </p>
        </td>
      </tr>
      <?php
    }
     ?>
  </tbody>
</table>
</form>
<script>
$(document).ready(function() {
  $('.checkbox-pending').click(function(){
      if($(this).is(':checked')){
          $('.checkbox-pending-item').prop('checked',true);
      }else{
          $('.checkbox-pending-item').prop('checked',false);
      }
  });
});
</script>
