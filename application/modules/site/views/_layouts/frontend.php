
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>

    <!-- Font Awesome Icons -->
    <!--<link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/fontawesome-free/css/all.min.css">-->
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">

    <!-- Ionicons -->
    <link href="<?=base_url()?>assets/tbs/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

    <!-- Select 2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

    <!-- JQUERY -->
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

    <!-- DataTables -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/datatable/media/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/css/dataTables.bootstrap4.css">

    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/jquery.dataTables.min.js?ver=1"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>


    <!-- datatable reorder _ buttons ext + resp + print -->
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/media/js/ColReorderWithResize.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.print.min.js"></script>
    <link href="<?=base_url()?>assets/datatable/ext/buttons/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=base_url()?>assets/datatable/ext/responsive/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/jszip/jszip.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/pdfmake/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/responsive/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/datatable/ext/buttons/buttons.html5.min.js"></script>

    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

    <!-- daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">

    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/ekko-lightbox/ekko-lightbox.css">

    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">

    <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.number.js"></script>

    <!-- timepicker -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/jquery.timepicker.min.css">

    <!-- my css -->
    <link rel="stylesheet" href="<?=base_url()?>assets/css/pallete.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">
    <style>
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url()?>assets/preloader/images/<?=$this->setting_web_preloader?>) center no-repeat #fff;
    }

    #footer-section::after {
      background: rgba(0, 0, 0, 0) url(<?=MY_IMAGEURL?>footer-map-bg.png) no-repeat scroll center center / 75% auto;
      content: "";
      height: 100%;
      left: 0;
      opacity: 0.1;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
    }
    #footer-section table td {
      border-top: none !important;
    }
    .custom-file {
      overflow-x: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
    }
    .bootstrap-datetimepicker-widget table td {
      border: none !important
    }
    .omniauth-btn {
      width: 48%;
    }
    .omniauth-divider::before, .omniauth-divider::after {
      content: '';
      flex: 1;
      border-bottom: 1px solid #e1e1e1;
      margin: 24px 0;
    }
    .omniauth-divider::before {
      margin-right: 16px;
    }
    .omniauth-divider::after {
      margin-left: 16px;
    }
    .toast-message>p {
      margin: 0 !important;
    }

    .content-header h1>small {
      font-size: 15px;
      display: inline-block;
      padding-left: 4px;
      font-weight: 300;
    }
    .rating {
      display: inline-block;
      position: relative;
      font-size: 20pt;
    }

    .rating label {
      position: absolute;
      top: 0;
      left: 0;
      height: 100%;
      cursor: pointer;
    }

    .rating label:last-child {
      position: static;
    }

    .rating label:nth-child(1) {
      z-index: 5;
    }

    .rating label:nth-child(2) {
      z-index: 4;
    }

    .rating label:nth-child(3) {
      z-index: 3;
    }

    .rating label:nth-child(4) {
      z-index: 2;
    }

    .rating label:nth-child(5) {
      z-index: 1;
    }

    .rating label input {
      position: absolute;
      top: 0;
      left: 0;
      opacity: 0;
    }

    .rating label .icon {
      float: left;
      color: transparent;
    }

    .rating label:last-child .icon {
      color: #000;
    }

    .rating:not(:hover) label input:checked ~ .icon,
    .rating:hover label:hover input ~ .icon {
      color: #ffc107;
    }

    .rating label input:focus:not(:checked) ~ .icon:last-child {
      color: #ffc107;
      text-shadow: 0 0 5px #ffc107;
    }

    .ui-timepicker-container,.ui-timepicker,.ui-timepicker-viewport {
      min-width: 100px !important;
    }
    .menu-bar .nav-link.active {
      color: rgba(0,0,0,.9) !important;
    }
    .menu-bar .nav-link {
      color: rgba(0,0,0,.5) !important;
    }
    </style>
</head>
<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FIRSTNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$ruser[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
?>
<body class="hold-transition layout-top-nav layout-navbar-fixed" style="background-color: #f4f6f9 !important">
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-light navbar-white">
      <div class="container">
        <a href="<?=site_url()?>" class="navbar-brand">
            <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" alt="Logo" class="brand-image" style="opacity: .8">
            <span class="brand-text font-weight-light d-none"><?=$this->setting_web_name?> <!--<small class="font-weight-light d-none d-sm-inline-block"><?=$this->setting_web_desc?> <sup class="text-danger text-bold">ver <?=$this->setting_web_version?></sup></small>--></span>
        </a>
        <ul class="navbar-nav">
          <?php
          $isLogin = IsLogin();
          if($isLogin) {
            ?>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/dashboard')?>" class="nav-link">Beranda</a>
            </li>
            <?php
            if($ruser[COL_ROLEID]!=ROLEPSIKOLOG) {
              ?>
              <?php
            } else {
              ?>
              <li class="nav-item d-none d-sm-inline-block">
                  <a href="<?=site_url('site/sesi/index')?>" class="nav-link">Jadwal</a>
              </li>
              <?php
            }
          }
          if($ruser[COL_ROLEID]!=ROLEPSIKOLOG) {
            ?>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/services')?>" class="nav-link">Layanan</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/blog')?>" class="nav-link">Inspirasi</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?=site_url('site/home/about')?>" class="nav-link">Tentang Kami</a>
            </li>
            <?php
          }
          ?>
          <!--<form class="form-inline mr-2">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Cari Psikolog" aria-label="Cari">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
              </div>
            </div>
          </form>-->
        </ul>

        <ul class="navbar-nav ml-auto mr-2">
          <?php
          if(IsLogin()) {
            ?>
            <!--<li class="nav-item">
              <a class="nav-link" href="#">
                <i class="fas fa-bell"></i>
              </a>
            </li>-->
            <li class="nav-item dropdown">
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
                  <img src="<?=$displaypicture?>" class="user-image img-circle elevation-1" alt="<?=$displayname?>" style="width: 20px !important; height: 20px !important">
                  &nbsp; <?=$displayname?>
              </a>
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="<?=site_url('site/home/profile')?>" class="dropdown-item"><i class="fad fa-user"></i>&nbsp;&nbsp;Profil</a></li>
                <!--<li><a href="<?=site_url('site/home/dashboard')?>" class="dropdown-item">Dashboard</a></li>-->
                <!--<li class="dropdown-divider"></li>-->
                <li><a href="<?=site_url('site/user/changepassword')?>" class="dropdown-item"><i class="fad fa-key"></i>&nbsp;&nbsp;Ubah Password</a></li>
                <li><a href="#" class="dropdown-item"><i class="fad fa-info-circle"></i>&nbsp;&nbsp;Help</a></li>
                <li><a href="<?=site_url('site/user/logout')?>" class="dropdown-item"><i class="fad fa-sign-out"></i>&nbsp;&nbsp;Keluar</a></li>
              </ul>
            </li>
            <?php
          } else {
            ?>
            <li class="nav-item mr-2">
              <a class="btn btn-sm btn-pallete-3" href="<?=site_url('user/register')?>" title="Login"s>
                  <i class="fad fa-user-plus"></i>&nbsp;Daftar
              </a>
            </li>
            <li class="nav-item">
              <a class="btn btn-sm btn-outline-pallete-1 bg-white color-pallete-1" href="<?=site_url('user/login')?>" title="Login">
                  <i class="fad fa-sign-in"></i>&nbsp;Login
              </a>
            </li>

            <!--<li class="nav-item user-menu">
              <a class="btn btn-sm btn-outline-success" href="<?=site_url('user/register')?>" title="Daftar">
                  <i class="fa fa-user-plus"></i>&nbsp;Daftar
              </a>
            </li>-->
            <?php
          }
          ?>
          <!--<li class="nav-item d-none d-sm-inline-block">
              <a href="<?=site_url('site/home/page/data-covid-19-indonesia-oleh-bnpb')?>" class="nav-link">Data &nbsp;<sup><span class="right badge badge-danger">BNPB</span></sup></a>
          </li>-->
          <li class="nav-item d-sm-none dropdown">
            <a class="nav-link" href="#" data-toggle="dropdown"><i class="fad fa-bars"></i></a>
            <ul class="dropdown-menu border-0 shadow dropdown-menu-right menu-bar">
              <?php
              $isLogin = IsLogin();
              if($isLogin) {
                ?>
                <li class="nav-item">
                    <a href="<?=site_url('site/home/dashboard')?>" class="nav-link">Beranda</a>
                </li>
                <?php
                if($ruser[COL_ROLEID]!=ROLEPSIKOLOG) {
                  ?>
                  <?php
                } else {
                  ?>
                  <li class="nav-item">
                      <a href="<?=site_url('site/sesi/index')?>" class="nav-link">Jadwal</a>
                  </li>
                  <?php
                }
              }
              if($ruser[COL_ROLEID]!=ROLEPSIKOLOG) {
                ?>
                <li class="nav-item">
                    <a href="<?=site_url('site/home/services')?>" class="nav-link">Layanan</a>
                </li>
                <li class="nav-item">
                    <a href="<?=site_url('site/home/blog')?>" class="nav-link">Inspirasi</a>
                </li>
                <li class="nav-item">
                    <a href="<?=site_url('site/home/about')?>" class="nav-link">Tentang Kami</a>
                </li>
                <?php
              }
              ?>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
    <?=$content?>
    <div id="footer-section" class="bg-white" style="position: relative; z-index:9; border-top: 1px solid #dee2e6;">
      <div class="content" style="padding: 0 .5rem">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 p-2">
              <p class="mb-0">
                <img src="<?=MY_IMAGEURL.$this->setting_web_logo?>" height="40px" />
              </p>
              <p class="mt-0 text-justify">
                One Stop Human Development Service.
              </p>
              <p class="mt-0 text-justify">
                Mentorhuis adalah layanan kerjasama antara PT Sistem Inspirasi Pendidikan dengan Pojok Psikologi. Untuk kerjasama dan pengembangan produk platform dapat menghubungi:
              </p>
              <p>
                <strong>Pojok Psikologi: 081770273747</strong><br />
                Alamat :<br />
                Jl. Gegerkalong Hilir Nomor 47, Sukarasa, Kec. Sukasari, Kota Bandung, Jawa Barat 40153, Indonesia
              </p>
              <p>
                <a href="<?=$this->setting_org_linkfacebook?>" target="_blank" class="btn btn-outline-primary">
                  <i class="fab fa-facebook"></i>
                </a>
                <a href="<?=$this->setting_org_linkinstagram?>" target="_blank" class="btn btn-outline-danger">
                  <i class="fab fa-instagram"></i>
                </a>
                <a href="<?=$this->setting_org_linktwitter?>" target="_blank" class="btn btn-outline-info">
                  <i class="fab fa-twitter"></i>
                </a>
              </p>
            </div>
            <div class="col-lg-4 p-2">

            </div>
            <div class="col-lg-4 p-2">
              <p class="text-justify">
                Layanan ini bukan untuk keadaan darurat seperti keadaan bunuh diri, jika dalam keadaan darurat harap langsung menghubungi layanan cepat tanggap pihak berwajib di nomor <strong>119</strong>
              </p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
    <footer class="main-footer bg-pallete-4 bg-dark">
        <div class="float-right d-none d-sm-inline">
            Version <strong><?=$this->setting_web_version?></strong>
        </div>
        <strong>Copyright &copy; <?=date("Y")?> Mentorhuis Team</strong>. Strongly developed by <b>PT Sistem Inspirasi Pendidikan</b>.
    </footer>
  </div>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.motio.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/iCheck/icheck.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/select2/js/select2.full.min.js"></script>
  <script src="<?=base_url()?>assets/js/bootstrap-select.js"></script>
  <script type="text/javascript" src="<?=base_url() ?>assets/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/inputmask/jquery.inputmask.bundle.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/moment/moment.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/daterangepicker/daterangepicker_mod.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <script src="<?=base_url()?>assets/js/jquery.timepicker.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js/Chart.min.js"></script>
  <script type="text/javascript">
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
  });
  $(document).ready(function() {
      /*var element = document.querySelector('.content-wrapper');
      var panning = new Motio(element, {
        fps: 30,
        speedX: 30
      });
      panning.play();*/
      $('a[href="<?=current_url()?>"]').addClass('active');
      $('a[href="<?=current_url()?>"]').closest('li.has-treeview').addClass('menu-open').children('.nav-link').addClass('active');

      $("select").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('input[type=file]').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
      });
      $('.datepicker').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1900,//parseInt(moment().subtract(moment.duration(14, 'Y')).format('YYYY'), 10),
        maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
            format: 'Y-MM-DD'
        }
      });
      $(".uang").number(true, 0, '.', ',');
      $('[data-toggle="tooltip"]').tooltip();
      /*$('.timepicker').datetimepicker({
        format: 'HH:mm'
      });
      $('input.datetimepicker-input', $('.timepicker')).focus(function() {
        $(this).closest('.timepicker').datetimepicker('toggle');
      });*/
      $('[data-toggle="datetimepicker"]').datetimepicker({
        format: 'HH:mm',
        useCurrent: false
      });
      $('.timepicker').timepicker({
        timeFormat: 'HH:mm',
        interval: 30,
        minTime: '7',
        maxTime: '21',
        defaultTime: '7',
        startTime: '07:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true,
        zindex: 9999999
      });

      $('[data-mask]').inputmask();
      jQuery.extend(jQuery.validator.messages, {
        required: "Masi kosong ini coeg.",
        /*remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")*/
      });
      var elements = document.getElementsByTagName("input");
      for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
          e.target.setCustomValidity("");
          $(e.target).removeClass('is-invalid');
          $(e.target).closest('div').find('.help-block').remove();

          if (!e.target.validity.valid) {
            if(e.srcElement.type == "email") {
              e.target.setCustomValidity("Harap isi dengan format email yang benar.");
            } else {
              $(e.target).addClass('is-invalid');
              $(e.target).closest('div').append('<span class="help-block text-danger">Kolom ini wajib diisi.</span>');
              e.target.setCustomValidity("Kolom ini wajib diisi.");
            }
          }
        };
        elements[i].oninput = function(e) {
          e.target.setCustomValidity("");
          $(e.target).removeClass('is-invalid');
          $(e.target).closest('div').find('.help-block').remove();
        };
      }

      elements = document.getElementsByTagName("textarea");
      for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
          e.target.setCustomValidity("");
          $(e.target).removeClass('is-invalid');
          $(e.target).closest('div').find('.help-block').remove();

          if (!e.target.validity.valid) {
            if(e.srcElement.type == "email") {
              e.target.setCustomValidity("Harap isi dengan format email yang benar.");
            } else {
              $(e.target).addClass('is-invalid');
              $(e.target).closest('div').append('<span class="help-block text-danger">Kolom ini wajib diisi.</span>');
              e.target.setCustomValidity("Kolom ini wajib diisi.");
            }
          }
        };
        elements[i].oninput = function(e) {
          e.target.setCustomValidity("");
          $(e.target).removeClass('is-invalid');
          $(e.target).closest('div').find('.help-block').remove();
        };
      }

      elements = document.getElementsByTagName("select");
      for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
          e.target.setCustomValidity("");
          $(e.target).removeClass('is-invalid');
          $(e.target).closest('div').find('.help-block').remove();

          if (!e.target.validity.valid) {
            if(e.srcElement.type == "email") {
              e.target.setCustomValidity("Harap isi dengan format email yang benar.");
            } else {
              $(e.target).addClass('is-invalid');
              $(e.target).closest('div').append('<span class="help-block text-danger">Kolom ini wajib diisi.</span>');
              e.target.setCustomValidity("Kolom ini wajib diisi.");
            }
          }
        };
        elements[i].oninput = function(e) {
          e.target.setCustomValidity("");
          $(e.target).removeClass('is-invalid');
          $(e.target).closest('div').find('.help-block').remove();
        };
      }
  });
  </script>
</body>
</html>
