<style>
.header-hero-banner {
    background-size: cover;
    width: 100%;
    color: #fff;
    padding: 280px 0 70px;
    overflow: hidden;
    position: relative;
    /*-webkit-clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 94%);
    clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 94%);*/
}
@media (min-width: 992px) {
  .header-hero-banner {
      /*-webkit-clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 92%);
      clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 92%);*/
  }
}

.header-hero-banner--lower-tco {
    background-image: url('<?=MY_IMAGEURL.'_bg-banner-1.png'?>');
    background-position: center 50%;
    z-index: 1;
}
.header-hero-banner--lower-tco::after {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    /*background: rgba(0,0,0,0.8);*/
    background: linear-gradient(to top, #4D089A, rgba(208, 164, 57, 0.38));
    z-index: -1;
}
.header-hero-banner--lower-tco h1 {
    max-width: 100%;
    width: 650px;
    line-height: 1;
    font-size: 45px;
}
@media (min-width: 1200px) {
  .header-hero-banner--lower-tco h1 {
      font-size: 52px;
  }
}

.sub-wrapper {
    padding: 40px;
}
.sub-wrapper.main-content {
    padding-top: 20px;
}
.just-commit-intro-text {
    padding-top: 15px;
    padding-bottom: 10px;
}
.just-commit-intro-text p {
    color: #2e2e2e;
    font-size: 20px;
    padding-right: 0;
}
@media (min-width: 992px) {
  .just-commit-intro-text p {
      font-size: 23px;
      padding-right: 60px;
  }
}
.just-commit-form-wrap {
    position: relative;
    top: 0;
    z-index: 50;
    margin-top: 30px;
    border: 2px solid #D0A331;
    /*background: linear-gradient(to bottom, #3f3177 0%, #1c1635 1000px);*/
    padding: 35px 30px;
    border-radius: 4px;
}
@media (min-width: 992px) {
  .just-commit-form-wrap {
      top: -110px;
      margin-top: 0;
      position: absolute;
  }
}
@media (min-width: 992px) {
  .just-commit-form-wrap {
      top: -310px;
  }
}
.form-container {
    -ms-flex-negative: 0;
    flex-shrink: 0;
    overflow: hidden;
}
.form-container h3, .form-container h4 {
    color: #ffffff;
}
.form-container h3 {
    font-size: 24px;
    font-weight: 600;
}

.just-commit-highlight-row {
    background: #ddd;
    /*-webkit-clip-path: polygon(0 0, 100% 6%, 100% 100%, 0% 100%);
    clip-path: polygon(0 0, 100% 6%, 100% 100%, 0% 100%);*/
    min-height: 200px;
    padding: 40px 40px 50px;
}
@media (min-width: 992px) {
  .just-commit-highlight-row {
      /*-webkit-clip-path: polygon(0 0, 100% 8%, 100% 100%, 0% 100%);
      clip-path: polygon(0 0, 100% 8%, 100% 100%, 0% 100%);*/
  }
}
@media (min-width: 1200px) {
  .just-commit_lower-tco .just-commit-highlight-row {
      padding: 40px 40px 100px;
  }
}
</style>
<div class="header-hero-banner header-hero-banner--lower-tco">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="font-weight-bold text-white"><?=$title?></h1>
      </div>
    </div>
  </div>
</div>
<!--<div class="sub-wrapper main-content">
  <div class="container">
    <div class="row">
    </div>
  </div>
</div>-->
<div class="just-commit-highlight-row">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="just-commit-intro-text">
          <?php
          if($verify) {
            ?>
            <p class="color-pallete-1">
              Hai, <strong class="color-pallete-3"><?=$user[COL_NM_FIRSTNAME]?></strong><br  />Akun kamu telah berhasil diverifikasi. Silakan <a href="<?=site_url('site/user/login')?>">LOGIN</a> untuk menikmati fitur-fitur yang tersedia.
            </p>
            <?php
          } else if($user[COL_ROLEID] == ROLEPSIKOLOG) {
            ?>
            <p class="color-pallete-1">
              Terima kasih atas kesediaan kamu untuk berpartisipasi. Kami akan segera melakukan verifikasi terhadap data diri kamu dan mengirimkan <strong>link aktivasi</strong> ke email yang sudah didaftarkan (<strong><?=$user[COL_USERNAME]?></strong>).
            </p>

            <p class="font-italic">
              PS: Proses verifikasi akan berlangsung 1x24 jam. Jika masih belum menerima email berisi hasil verifikasi, silahkan kirim pesan melalui Instagram @mentorhuisID e-mail ke info <strong>admin@mentorhuis.com</strong>.
            </p>
            <?php
          } else {
            ?>
            <p class="color-pallete-1">
              Kami sudah mengirimkan tautan verifikasi ke alamat email <strong class="color-pallete-3"><?=$email?></strong>. Segera periksa dan klik pada tautan yang diberikan untuk mengaktifkan akun anda.
            </p>
            <?php
          }
          ?>
        </div>

      </div>
    </div>
  </div>
</div>
