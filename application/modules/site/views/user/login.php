
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?=!empty($title) ? $title.' | '.$this->setting_web_name : $this->setting_web_name?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- my css -->
  <link rel="stylesheet" href="<?=base_url()?>assets/css/pallete.css">
  <link rel="stylesheet" href="<?=base_url()?>assets/css/my.css">

  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/modernizr/modernizr.js"></script>

  <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.css">
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/toastr/toastr.min.js"></script>

  <link rel="icon" type="image/png" href="<?=MY_IMAGEURL.$this->setting_web_logo?>" />

  <style>
  .omniauth-btn {
    width: 48%;
  }
  .omniauth-divider::before, .omniauth-divider::after {
    content: '';
    flex: 1;
    border-bottom: 1px solid #e1e1e1;
    margin: 24px 0;
  }
  .omniauth-divider::before {
    margin-right: 16px;
  }
  .omniauth-divider::after {
    margin-left: 16px;
  }
  </style>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?=site_url()?>">
        <img src="<?=MY_IMAGEURL.'logo.png'?>" height="60px" />
      </a>
    </div>
    <div class="card">
      <div class="card-body login-card-body">
        <?php
        if(!empty($error)) {
          ?>
          <p class="text-danger font-weight-bold"><?=$error?></p>
          <?php
        }
        ?>
        <?=form_open(site_url('site/ajax/login'),array('role'=>'form','id'=>'form-login','class'=>'form-horizontal'))?>
        <div class="row">
          <div class="col-sm-12">
            <div class="form-group row">
              <div class="input-group">
                <input type="text" name="<?=COL_USERNAME?>" class="form-control" placeholder="E-mail terdaftar" required />
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-user"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="input-group">
                <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password" required />
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-key"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="icheck-primary">
                <input type="checkbox" id="RememberMe" name="RememberMe">
                <label for="RememberMe">Ingat Saya</label>
              </div>
            </div>
            <div class="form-group row">
              <button type="submit" class="btn btn-block btn-outline-pallete-1 mb-1"><i class="fad fa-sign-in"></i>&nbsp;&nbsp;LOGIN</button>
              <a href="<?=site_url('site/user/forgotpassword')?>" class="btn btn-block btn-outline-pallete-3"><i class="fad fa-lock-alt"></i>&nbsp;&nbsp;LUPA PASSWORD?</a>
            </div>
          </div>
        </div>
        <div class="omniauth-divider d-flex align-items-center text-center">
          <span class="color-pallete-1">ATAU</span>
        </div>
        <label class="label-bold d-block">
          Login menggunakan akun:
        </label>
        <div class="d-flex justify-content-between flex-wrap">
          <a href="<?=$googleAuthUrl?>" class="btn btn-default d-flex align-items-center omniauth-btn text-left oauth-login mb-2 p-2 ">
            <img src="<?=MY_IMAGEURL.'_logo-google.png'?>" style="width: 1.25rem; height: 1.25rem; vertical-align: middle" />
            <span class="ml-2">Google</span>
          </a>
          <a href="#" class="btn btn-default d-flex align-items-center omniauth-btn text-left oauth-login mb-2 p-2 disabled">
            <img src="<?=MY_IMAGEURL.'_logo-facebook.png'?>" style="width: 1.25rem; height: 1.25rem; vertical-align: middle"  />
            <span class="ml-2">Facebook</span>
          </a>
        </div>
        <?=form_close()?>
      </div>
      <div class="card-footer">
        <p class="font-italic">
          Belum punya akun? Ayo <a href="<?=site_url('site/user/register')?>">daftar</a>!
        </p>
      </div>
    </div>
  </div>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/dist/js/adminlte.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
  <script>
  $(document).ready(function() {
    $('#form-login').validate({
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
              if(res.data && res.data.redirect) {
                setTimeout(function(){
                  location.href = res.data.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
          }
        });
        return false;
      }
    });
  });
  </script>
</body>
</html>
