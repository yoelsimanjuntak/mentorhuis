<div class="content-wrapper">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?=site_url()?>">
        <img src="<?=MY_IMAGEURL.'logo.png'?>" height="60px" />
      </a>
    </div>
    <div class="card">
      <div class="card-body login-card-body">
        <?=form_open(current_url(),array('role'=>'form','id'=>'form-forgotpassword','class'=>'form-horizontal'))?>
        <div class="row">
          <div class="col-sm-12">
            <p class="font-weight-light">Masukkan email anda yang terdaftar untuk diverifikasi:</p>
            <div class="form-group row">
              <div class="input-group">
                <input type="email" name="<?=COL_EMAIL?>" class="form-control" placeholder="Email" required />
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <button type="submit" class="btn btn-block btn-outline-pallete-1 mb-1"><i class="fad fa-arrow-right"></i>&nbsp;&nbsp;VERIFIKASI</button>
              <a href="<?=site_url()?>" class="btn btn-block btn-outline-secondary mb-1"><i class="fad fa-home"></i>&nbsp;&nbsp;KE HALAMAN UTAMA</a>
            </div>
          </div>
        </div>
        <?=form_close()?>
      </div>
      <div class="card-footer">
        <p class="font-italic">
          Belum punya akun? Ayo <a href="<?=site_url('site/user/register')?>">daftar</a>!
        </p>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('#form-forgotpassword').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.data && res.data.redirect) {
              /*setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);*/
            }
            $('input', form).val('');
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
