<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light"><?= $title ?></h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <?=form_open(site_url('site/ajax/register-finish/'.GetEncryption($email)),array('role'=>'form','id'=>'form-register','class'=>'form-horizontal'))?>
      <div class="row">
        <!--<div class="col-sm-3">
          <div class="card">
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                  <a href="#" class="nav-link disabled text-success">
                    <i class="fas fa-check-circle"></i>&nbsp;REGISTRASI AKUN
                  </a>
                </li>
                <li class="nav-item active">
                  <a href="#" class="nav-link disabled font-weight-bold">
                    <i class="far fa-circle"></i>&nbsp;LENGKAPI FORMULIR
                  </a>
                </li>
                <li class="nav-item">
                  <a href="#" class="nav-link disabled">
                    <i class="far fa-circle"></i>&nbsp;VERIFIKASI EMAIL
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>-->
        <div class="col-sm-6">
          <div class="card card-outline card-purple">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Data Diri Lengkap</h5>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Email & Nickname</label>
                <div class="row">
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="<?=COL_EMAIL?>" value="<?=$email?>" placeholder="Email" readonly />
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="<?=COL_NM_NICKNAME?>" placeholder="Nickname" required />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Nama</label>
                <div class="row">
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="<?=COL_NM_FIRSTNAME?>" value="<?=!empty($ruser)?$ruser[COL_NM_FIRSTNAME]:''?>" placeholder="Nama Depan" required />
                  </div>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="<?=COL_NM_LASTNAME?>" value="<?=!empty($ruser)?$ruser[COL_NM_LASTNAME]:''?>" placeholder="Nama Belakang" required />
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Biodata Singkat (maks. 200 karakter)</label>
                <div class="row">
                  <div class="col-sm-12">
                    <textarea class="form-control" name="<?=COL_NM_BIO?>" maxlength="200" placeholder="Biodata Singkat" required></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label>Alamat</label>
                <div class="row">
                  <div class="col-sm-12">
                    <textarea class="form-control" name="<?=COL_NM_ADDRESS?>" placeholder="Alamat" required></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label>Provinsi</label>
                  <select name="<?=COL_NM_PROVINCE?>" class="form-control no-select2" style="width: 100%" required>
                  </select>
                </div>
                <div class="col-sm-6">
                  <label>Kabupaten / Kota</label>
                  <select name="<?=COL_NM_CITY?>" class="form-control no-select2" style="width: 100%" required>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label>Tanggal Lahir</label>
                  <input type="text" class="form-control" name="<?=COL_DATE_BIRTH?>" placeholder="yyyy-mm-dd" required />
                </div>
                <div class="col-sm-6">
                  <label>No. Telp / HP</label>
                  <input type="text" class="form-control" name="<?=COL_NO_PHONE?>" placeholder="No. Telp / HP" required />
                </div>
              </div>
              <div class="form-group row">
                <div class="col-sm-6">
                  <label>Status Pernikahan</label>
                  <select name="<?=COL_NM_MARITALSTATUS?>" class="form-control" style="width: 100%" required>
                    <option value="BELUM MENIKAH">BELUM MENIKAH</option>
                    <option value="MENIKAH">MENIKAH</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card card-outline card-purple">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Pekerjaan & Pengalaman</h5>
            </div>
            <div class="card-body">
              <div class="form-group row">
                <div class="col-sm-8">
                  <label>Pekerjaan</label>
                  <input type="text" class="form-control" name="<?=COL_NM_OCCUPATION?>" placeholder="Pekerjaan" required />
                </div>
                <div class="col-sm-4">
                  <label>Pengalaman</label>
                  <input type="text" class="form-control uang text-right" name="<?=COL_NUM_EXPERIENCE?>" placeholder="N Tahun" required />
                </div>
              </div>
              <div class="form-group">
                  <label>Spesialisasi</label>
                  <div class="row">
                    <div class="col-sm-12">
                      <select name="KD_Type[]" class="form-control no-select2" style="width: 100%" multiple>
                        <?=GetCombobox("SELECT * from mtype order by NM_Type", COL_KD_TYPE, COL_NM_TYPE)?>
                      </select>
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <label>Riwayat Pendidikan</label>
                <input type="hidden" name="<?=COL_NM_EDUCATIONALBACKGROUND?>" />
                <div class="row">
                  <div class="col-sm-12">
                    <div class="mt-2 mb-3">
                      <table id="tbl-education" class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Jenjang</th>
                            <th>Tahun Lulus</th>
                            <th>Nama Institusi</th>
                            <th class="text-center">#</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th>
                              <select name="Grade" class="form-control" style="min-width: 8vw">
                                <!--<option value="SD">SD</option>
                                <option value="SMP">SMP</option>
                                <option value="SMA/SMK">SMA/SMK</option>
                                <option value="DIPLOMA">DIPLOMA</option>-->
                                <option value="SARJANA">S1</option>
                                <option value="MAGISTER">S2</option>
                                <option value="DOKTOR">S3</option>
                              </select>
                            </th>
                            <th class="text-center" style="white-space: nowrap">
                              <input type="number" class="form-control" name="TahunLulus" placeholder="Tahun Lulus" />
                            </th>
                            <th class="text-center" style="white-space: nowrap">
                              <input type="text" class="form-control" name="Institusi" placeholder="Nama Institusi" />
                            </th>
                            <th class="text-center">
                              <button type="button" id="btn-add-education" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                            </th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label>Jadwal</label>
                <input type="hidden" name="ScheduleFixed" />
                <div class="row">
                  <div class="col-sm-12">
                    <div class="mt-2 mb-3">
                      <table id="tbl-schedule-fixed" class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Hari</th>
                            <th>Jam</th>
                            <th class="text-center">#</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>
                          <tr>
                            <th>
                              <select name="Day" class="form-control" style="min-width: 70px">
                                <?=GetCombobox("SELECT * from mdays order by KD_Day", COL_KD_DAY, COL_NM_DAY, null, true, false, '-- Hari --')?>
                              </select>
                            </th>
                            <th class="text-center" style="white-space: nowrap">
                              <!--<input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="timepickerFrom" name="TimeFrom" data-toggle="datetimepicker" data-target="#timepickerFrom" placeholder="HH:MM" style="width: 6vw" />-->
                              <input type="text" class="form-control d-inline-block timepicker" id="timepickerFrom" name="TimeFrom" placeholder="HH:MM" style="width: 6vw; min-width: 70px" readonly />
                              <span class="p-1">-</span>
                              <!--<input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="timepickerTo" name="TimeTo" data-toggle="datetimepicker" data-target="#timepickerTo" placeholder="HH:MM" style="width: 6vw" />-->
                              <input type="text" class="form-control d-inline-block" id="timepickerTo" name="TimeTo" placeholder="HH:MM" style="width: 6vw; min-width: 70px" readonly />
                            </th>
                            <th class="text-center">
                              <button type="button" id="btn-add-schedule-fixed" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                            </th>
                          </tr>
                        </tfoot>
                      </table>
                      <small class="font-italic text-danger">Note: Durasi per sesi standar antara 50 - 60 menit dan harap memperhatikan waktu persiapan antar sesi (idealnya 5-10 menit)</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group row text-right">
            <div class="col-sm-12">
              <button type="submit" class="btn btn-lg btn-info"><i class="far fa-arrow-right"></i>&nbsp;SUBMIT</button>
            </div>
          </div>
        </div>
      </div>
      <?=form_close()?>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('select[name^=KD_Type]').select2({
    width: 'resolve',
    theme: 'bootstrap4',
    placeholder: 'Spesialisasi'
  });
  $('[name=<?=COL_DATE_BIRTH?>]').daterangepicker({
    //autoUpdateInput: false,
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().subtract(80, 'years').format('YYYY'),10),
    maxYear: parseInt(moment().subtract(14, 'years').format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=TimeFrom]').timepicker({
    timeFormat: 'HH:mm',
    interval: 30,
    minTime: '7',
    maxTime: '21',
    defaultTime: '7',
    startTime: '07:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    zindex: 9999999,
    change: function(time) {
      var thisval = $(this).val();
      var timeto = moment(thisval, "HH:mm").add(60, 'minutes').format('HH:mm');
      $('[name=TimeTo]').val(timeto);
    }
  });

  $.get('https://wilayah-api.herokuapp.com/provinsi', function(prov) {
    var dataProvinsi = $.map(prov, function(n,i){
      return {id: n.name, text: n.name}
    });

    $('select[name=<?=COL_NM_PROVINCE?>]').select2({
      width: 'resolve',
      theme: 'bootstrap4',
      placeholder: 'Pilih Provinsi',
      data: dataProvinsi
    }).trigger('change');
  });

  $('select[name=<?=COL_NM_PROVINCE?>]').change(function() {
    var dis = $(this);
    var url = 'https://wilayah-api.herokuapp.com/provinsi?name='+dis.val();

    $.get(url, function(data) {
      var provId = 0;
      if(data && data.length>0) {
        provId = data[0].id;
      }

      $.get('https://wilayah-api.herokuapp.com/kabupaten?province_id='+provId, function(kot) {
        var dataKota = $.map(kot, function(n,i){
          return {id: n.name, text: n.name}
        });

        $('select[name=<?=COL_NM_CITY?>]').empty();
        $('select[name=<?=COL_NM_CITY?>]').select2({
          width: 'resolve',
          theme: 'bootstrap4',
          placeholder: 'Pilih Kabupaten / Kota',
          data: dataKota
        }).trigger('change');
      });
    });
  });

  $('[name=ScheduleFixed]').change(function() {
    writeSchedule('tbl-schedule-fixed', 'ScheduleFixed');
  }).trigger('change');
  $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').change(function() {
    writeEducation('tbl-education', '<?=COL_NM_EDUCATIONALBACKGROUND?>');
  }).trigger('change');

  $('#btn-add-schedule-fixed', $('#tbl-schedule-fixed')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleFixed]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day && a.TimeFrom == timeFr;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': $('[name=Day] option:selected', row).html(), 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleFixed]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('[name=TimeFrom]', row).val('07:00').trigger('change');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });
  $('#btn-add-education', $('#tbl-education')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var grade = $('[name=Grade]', row).val();
    var tahun = $('[name=TahunLulus]', row).val();
    var institusi = $('[name=Institusi]', row).val();
    if(grade && tahun && institusi) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Grade == grade;
      });
      if(exist.length == 0) {
        arr.push({'Grade': grade, 'TahunLulus':tahun, 'Institusi':institusi});
        $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi pendidikan dengan benar.');
    }
  });

  $('#form-register').validate({
    /*rules: {
      field: {
        required: true
      }
    },*/
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});

function writeSchedule(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return (a['Day']+'--'+a['TimeFrom']).localeCompare(b['Day']+'--'+b['TimeFrom']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].DayText+'</td>';
        html += '<td>'+arr[i].TimeFrom+' - '+arr[i].TimeTo+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Day+'--'+arr[i].TimeFrom+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val().split('--');
        var day = idx[0];
        var timefrom = idx[1];
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return (e.Day != day || e.TimeFrom != timefrom); });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

function writeEducation(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].Grade+'</td>';
        html += '<td>'+arr[i].TahunLulus+'</td>';
        html += '<td>'+arr[i].Institusi+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Grade+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Grade != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
