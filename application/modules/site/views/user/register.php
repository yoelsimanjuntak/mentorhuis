<style>
.header-hero-banner {
    background-size: cover;
    width: 100%;
    color: #fff;
    padding: 280px 0 70px;
    overflow: hidden;
    position: relative;
    /*-webkit-clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 94%);
    clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 94%);*/
}
@media (min-width: 992px) {
  .header-hero-banner {
      /*-webkit-clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 92%);
      clip-path: polygon(0 0, 100% 0%, 100% 100%, 0 92%);*/
  }
}

.header-hero-banner--lower-tco {
    background-image: url('<?=MY_IMAGEURL.'_bg-banner-1.png'?>');
    background-position: center 50%;
    z-index: 1;
}
.header-hero-banner--lower-tco::after {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    /*background: rgba(0,0,0,0.8);*/
    background: linear-gradient(to top, #4D089A, rgba(208, 164, 57, 0.38));
    z-index: -1;
}
.header-hero-banner--lower-tco h1 {
    max-width: 100%;
    width: 650px;
    line-height: 1;
    font-size: 45px;
}
@media (min-width: 1200px) {
  .header-hero-banner--lower-tco h1 {
      font-size: 52px;
  }
}

.sub-wrapper {
    padding: 40px;
}
.sub-wrapper.main-content {
    padding-top: 20px;
}
.just-commit-intro-text {
    padding-top: 15px;
    padding-bottom: 10px;
}
.just-commit-intro-text p {
    color: #2e2e2e;
    font-size: 20px;
    padding-right: 0;
}
@media (min-width: 992px) {
  .just-commit-intro-text p {
      font-size: 23px;
      padding-right: 60px;
  }
}
.just-commit-form-wrap {
    position: relative;
    top: 0;
    z-index: 50;
    margin-top: 30px;
    border: 2px solid #D0A331;
    /*background: linear-gradient(to bottom, #3f3177 0%, #1c1635 1000px);*/
    padding: 35px 30px;
    border-radius: 4px;
}
@media (min-width: 992px) {
  .just-commit-form-wrap {
      top: -110px;
      margin-top: 0;
      position: absolute;
  }
}
@media (min-width: 992px) {
  .just-commit-form-wrap {
      top: -310px;
  }
}
.form-container {
    -ms-flex-negative: 0;
    flex-shrink: 0;
    overflow: hidden;
}
.form-container h3, .form-container h4 {
    color: #ffffff;
}
.form-container h3 {
    font-size: 24px;
    font-weight: 600;
}

.just-commit-highlight-row {
    background: #ddd;
    /*-webkit-clip-path: polygon(0 0, 100% 6%, 100% 100%, 0% 100%);
    clip-path: polygon(0 0, 100% 6%, 100% 100%, 0% 100%);*/
    min-height: 200px;
    padding: 40px 40px 50px;
}
@media (min-width: 992px) {
  .just-commit-highlight-row {
      /*-webkit-clip-path: polygon(0 0, 100% 8%, 100% 100%, 0% 100%);
      clip-path: polygon(0 0, 100% 8%, 100% 100%, 0% 100%);*/
  }
}
@media (min-width: 1200px) {
  .just-commit_lower-tco .just-commit-highlight-row {
      padding: 40px 40px 100px;
  }
}
@media (min-width: 992px) {
  .register-text {
    min-height: 40vh;
  }
}
</style>
<div class="header-hero-banner header-hero-banner--lower-tco">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="font-weight-bold text-white"><?=$role=='member'?'Daftar Sekarang. Gratis!':'You Are The Frontline! Mari bergabung dengan MentorHuis.'?></h1>
      </div>
    </div>
  </div>
</div>
<div class="sub-wrapper main-content">
  <div class="container">
    <div class="row">
      <div class="col-sm-7 register-text">
        <div class="just-commit-intro-text">
          <?php
          if($role == 'member') {
            ?>
            <p>Waktunya mendapatkan bimbingan yang tepat sesuai kebutuhan kamu. Daftar sekarang dan dapatkan bimbingan terbaik dari para mentor.</p>
            <?php
          } else {
            ?>
            <p>Dunia membutuhkan <span class="color-pallete-3">Mentor</span> profesional sepertimu. Sekarang waktunya!</p>
            <?php
          }
          ?>

        </div>
      </div>
      <div class="col-sm-5">
        <div class="just-commit-form-wrap bg-white">
          <div class="form-to-resource-content">
            <div class="form-to-resource-content-section">
              <div class="form-container">
                <h3 class="mt-0 mb-3 color-pallete-1">Silakan isi form dibawah ini.</h3>
                <?php
                if(!empty($error)) {
                  ?>
                  <p class="text-danger font-weight-bold"><?=$error?></p>
                  <?php
                }
                ?>
                <?=form_open(site_url('site/ajax/register'),array('role'=>'form','id'=>'form-register','class'=>'form-horizontal'))?>
                <div class="row">
                  <div class="col-sm-12">
                    <input type="hidden" name="UserType" value="<?=$role=='member'?'Member':'Psikolog'?>" />
                    <?php
                    if($role=='member') {
                      ?>
                      <div class="form-group row">
                        <label class="control-label col-sm-4">Kategori</label>
                        <div class="col-sm-8">
                          <div class="input-group">
                            <select class="form-control" name="<?=COL_ROLEID?>" required>
                              <!--<?=GetCombobox("SELECT * FROM _roles where RoleID not in (1,5) ORDER BY RoleID", COL_ROLEID, COL_ROLENAME)?>-->
                              <option value="2">Pribadi</option>
                              <option value="3" disabled>Komunitas</option>
                              <option value="4" disabled>Perusahaan</option>
                            </select>
                            <div class="input-group-append">
                              <div class="input-group-text">
                                <span class="fas fa-list-alt"></span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php
                    }
                    ?>

                    <!--<div class="form-group row">
                      <label class="control-label col-sm-4">Username</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <input type="text" name="<?=COL_USERNAME?>" class="form-control" placeholder="Username" required />
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-user"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>-->
                    <div class="form-group row">
                      <label class="control-label col-sm-4">Email</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <input type="email" name="<?=COL_EMAIL?>" class="form-control" placeholder="Email" required />
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-envelope"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-sm-4">Password</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password" required />
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-key"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-sm-4">Ulangi Password</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                          <input type="password" name="ConfirmPassword" class="form-control" placeholder="Ulangi Password" required />
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-key"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-sm-12">
                        <small class="text-muted">
                          <i class="far fa-check-square text-primary"></i>&nbsp;&nbsp;Dengan mendaftar pada MentorHuis, kamu menyetujui <a href="<?=site_url('site/home/terms-and-conds')?>">Persyaratan Layanan dan Kebijakan Privasi</a>.
                        </small>
                      </div>
                      <div class="col-sm-12 mt-2">
                        <button type="submit" class="btn btn-block btn-outline-pallete-3">DAFTAR</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="omniauth-divider d-flex align-items-center text-center">
                  <span class="color-pallete-1">ATAU</span>
                </div>
                <label class="label-bold d-block">
                  Daftar menggunakan akun:
                </label>
                <div class="d-flex justify-content-between flex-wrap">
                  <a href="<?=$googleAuthUrl?>" class="btn btn-default d-flex align-items-center omniauth-btn text-left oauth-login mb-2 p-2 ">
                    <img src="<?=MY_IMAGEURL.'_logo-google.png'?>" style="width: 1.25rem; height: 1.25rem; vertical-align: middle" />
                    <span class="ml-2">Google</span>
                  </a>
                  <a href="#" class="btn btn-default d-flex align-items-center omniauth-btn text-left oauth-login mb-2 p-2 disabled">
                    <img src="<?=MY_IMAGEURL.'_logo-facebook.png'?>" style="width: 1.25rem; height: 1.25rem; vertical-align: middle"  />
                    <span class="ml-2">Facebook</span>
                  </a>
                </div>
                <?=form_close()?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--<div class="just-commit-highlight-row">
  <div class="container">
    <div class="row">
      <div class="col-md-7">
        <div class="just-commit-intro-text">
          <p class="color-pallete-1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </p>
        </div>

      </div>
    </div>
  </div>
</div>-->
<script type="text/javascript">
$(document).ready(function() {
  $('form').each(function() {
    $(this).validate({
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
              if(res.data && res.data.redirect) {
                setTimeout(function(){
                  location.href = res.data.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
          }
        });
        return false;
      }
    });
  });

});
</script>
