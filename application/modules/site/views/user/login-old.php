<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title font-weight-light"><i class="fad fa-sign-in"></i>&nbsp;LOGIN</h5>
            </div>
            <div class="card-body register-card-body">
              <?=form_open(site_url('site/ajax/login'),array('role'=>'form','id'=>'form-login','class'=>'form-horizontal'))?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Username / Email</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="text" name="<?=COL_USERNAME?>" class="form-control" placeholder="Username" required />
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-user"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Password</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password" required />
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-key"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8 offset-sm-4">
                      <div class="icheck-info">
                        <input type="checkbox" id="RememberMe" name="RememberMe">
                        <label for="RememberMe">Ingat Saya</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <button type="submit" class="btn btn-block btn-outline-info"><i class="fad fa-sign-in"></i>&nbsp;&nbsp;LOGIN</button>
                    </div>
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-block btn-outline-secondary"><i class="fad fa-lock-alt"></i>&nbsp;&nbsp;LUPA PASSWORD?</button>
                    </div>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
        </div>
        <!--<div class="col-sm-2 text-center">
          <p class="text-muted" style="padding-top: 20vh">
            ATAU
          </p>
        </div>-->
        <div class="col-sm-6">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title font-weight-light"><i class="fad fa-user-plus"></i>&nbsp;DAFTAR</h5>
            </div>
            <div class="card-body register-card-body">
              <?=form_open(site_url('site/ajax/register'),array('role'=>'form','id'=>'form-register','class'=>'form-horizontal'))?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Daftar Sebagai</label>
                    <div class="col-sm-8">
                      <div class="icheck-info d-inline mr-2">
                        <input type="radio" id="radioUserType_Member" name="UserType" value="Member" checked  />
                        <label class="font-weight-light" for="radioUserType_Member">
                          Pengguna
                        </label>
                      </div>
                      <div class="icheck-info d-inline">
                        <input type="radio" id="radioUserType_Psikolog" name="UserType" value="Psikolog"  />
                        <label class="font-weight-light" for="radioUserType_Psikolog">
                          Psikolog
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Kategori</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <select class="form-control" name="<?=COL_ROLEID?>" required>
                          <?=GetCombobox("SELECT * FROM _roles where RoleID not in (1,5) ORDER BY RoleID", COL_ROLEID, COL_ROLENAME)?>
                        </select>
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-list-alt"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Username</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="text" name="<?=COL_USERNAME?>" class="form-control" placeholder="Username" required />
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-user"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Email</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="email" name="<?=COL_EMAIL?>" class="form-control" placeholder="Email" required />
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Password</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="password" name="<?=COL_PASSWORD?>" class="form-control" placeholder="Password" required />
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-key"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Ulangi Password</label>
                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="password" name="ConfirmPassword" class="form-control" placeholder="Ulangi Password" required />
                        <div class="input-group-append">
                          <div class="input-group-text">
                            <span class="fas fa-key"></span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12">
                      <small class="text-muted">
                        <i class="far fa-check-square text-primary"></i>&nbsp;&nbsp;Dengan mendaftar pada MentorHuis, kamu menyetujui <a href="#">Persyaratan Layanan dan Kebijakan Privasi</a>.
                      </small>
                    </div>
                    <div class="col-sm-12 mt-2">
                      <button type="submit" class="btn btn-block btn-outline-info">DAFTAR</button>
                    </div>
                  </div>
                </div>
              </div>
              <div class="omniauth-divider d-flex align-items-center text-center">
                ATAU
              </div>
              <label class="label-bold d-block">
                Daftar menggunakan akun:
              </label>
              <div class="d-flex justify-content-between flex-wrap">
                <a href="#" class="btn btn-default d-flex align-items-center omniauth-btn text-left oauth-login mb-2 p-2 ">
                  <img src="<?=MY_IMAGEURL.'_logo-google.png'?>" style="width: 1.25rem; height: 1.25rem; vertical-align: middle" />
                  <span class="ml-2">Google</span>
                </a>
                <a href="#" class="btn btn-default d-flex align-items-center omniauth-btn text-left oauth-login mb-2 p-2 ">
                  <img src="<?=MY_IMAGEURL.'_logo-facebook.png'?>" style="width: 1.25rem; height: 1.25rem; vertical-align: middle"  />
                  <span class="ml-2">Facebook</span>
                </a>
              </div>
              <?=form_close()?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('[name=UserType]').change(function() {
    var val = $('[name=UserType]:checked').val();

    if(val == 'Member') {
      $('[name=<?=COL_ROLEID?>]').attr('disabled', false).closest('.form-group').show();
    } else {
      $('[name=<?=COL_ROLEID?>]').attr('disabled', true).closest('.form-group').hide();
    }
  });
  $('[name=UserType]:checked').trigger('change');

  $('form').each(function() {
    $(this).validate({
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
              if(res.data && res.data.redirect) {
                setTimeout(function(){
                  location.href = res.data.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
          }
        });
        return false;
      }
    });
  });

});
</script>
