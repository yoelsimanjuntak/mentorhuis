<div class="text-center">
  <img class="profile-user-img img-fluid img-circle" src="<?=!empty($user[COL_NM_PROFILEIMAGE])?MY_UPLOADURL.$user[COL_NM_PROFILEIMAGE]:MY_IMAGEURL.'user.jpg'?>" alt="User profile picture">
</div>
<h3 class="profile-username text-center"><?=$user[COL_NM_FIRSTNAME].' '.$user[COL_NM_LASTNAME]?></h3>
<ul class="list-group list-group-unbordered row">
  <li class="list-group-item">
    <b>Tanggal Lahir</b> <a class="float-right"><?=!empty($user[COL_DATE_BIRTH]) ? date('d/m/Y', strtotime($user[COL_DATE_BIRTH])) : '-'?></a>
  </li>
  <li class="list-group-item">
    <b>Pekerjaan</b> <a class="float-right"><?=!empty($user[COL_NM_OCCUPATION]) ? $user[COL_NM_OCCUPATION] : '-'?></a>
  </li>
  <li class="list-group-item">
    <b>Alamat</b> <a class="float-right"><?=!empty($user[COL_NM_ADDRESS]) ? $user[COL_NM_ADDRESS] : '-'?></a>
  </li>
  <li class="list-group-item">
    <b>Kota & Provinsi</b> <a class="float-right"><?=!empty($user[COL_NM_CITY]) ? $user[COL_NM_CITY].(!empty($user[COL_NM_PROVINCE])?', '.$user[COL_NM_PROVINCE]:'') : '-'?></a>
  </li>
  <?php
  $userstat = $this->db
  ->where(COL_USERNAME, $user[COL_USERNAME])
  ->order_by(COL_CREATEDON, 'desc')
  ->limit(5)
  ->get(TBL_USERSTATUS)
  ->result_array();
  if(!empty($userstat)) {
    ?>
    <li class="list-group-item border-bottom-0">
      <b>Status Terakhir</b>
      <?php
      foreach($userstat as $stat) {
        ?>
        <blockquote class="quote-secondary pb-0">
          <p class="text-sm text-justify font-italic mb-1"><?=$stat[COL_STATUS]?></p>
          <small class="font-weight-bold"><cite><?=date('d/m/Y H:i:s', strtotime($stat[COL_CREATEDON]))?></cite></small>
        </blockquote>
        <?php
      }
      ?>
    </li>
    <?php
  }
  ?>
</ul>
