<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light"><?= $title ?></h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title">Silakan isi kuisioner berikut sebelum kami mencarikan mentor yang sesuai untuk kamu.</h5>
            </div>
            <div class="card-body">
              <?=form_open(site_url('site/ajax/session-add'),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group row">
                    <label>Apa keluhan kamu?</label>
                    <div class="col-sm-12">
                      <textarea name="<?=COL_NM_REMARKSCLIENTFEELING?>" class="form-control" rows="3" placeholder="Deskripsikan keluhan kamu disini"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label>Bagaimana kondisi kamu saat ini?</label>
                    <div class="col-sm-12">
                      <textarea name="<?=COL_NM_REMARKSCLIENTCONDITION?>" class="form-control" rows="3" placeholder="Deskripsikan kondisi kamu saat ini"></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group row">
                    <label>Bantuan apa yang kamu harapkan dengan adanya sesi ini?</label>
                    <div class="col-sm-12">
                      <textarea name="<?=COL_NM_REMARKSCLIENTEXPECTATION?>" class="form-control" rows="3" placeholder="Deskripsikan hal yang kamu harapkan dengan adanya sesi ini"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Jenis Konseling</label>
                    <div class="row">
                      <div class="col-sm-2">
                        <div class="form-check">
                          <input id="RadioSessionPrivate" class="form-check-input" type="radio" name="<?=COL_IS_CAREGROUP?>" value="0" checked>
                          <label class="form-check-label" for="RadioSessionPrivate">Pribadi</label>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="form-check">
                          <input id="RadioSessionGroup" class="form-check-input" type="radio" name="<?=COL_IS_CAREGROUP?>" value="1">
                          <label class="form-check-label" for="RadioSessionGroup">Care Group</label>
                        </div>
                      </div>
                      <div class="col-sm-12">
                        <p class="text-muted mt-2">
                          <strong>CARE GROUP</strong> adalah sesi layanan konseling khusus yang dilakukan antara mentor dengan beberapa klien yang memiliki masalah yang sama.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row" style="border-top: 1px solid rgba(0,0,0,.125); margin-left: -20px; margin-right: -20px">
                <div class="col-sm-12 mt-3 text-center">
                  <div class="form-group mb-0">
                    <input type="hidden" name="<?=COL_KD_TYPE?>" value="<?=$data[COL_KD_TYPE]?>" />
                    <input type="hidden" name="<?=COL_NM_SESSIONHOST?>" />
                    <input type="hidden" name="<?=COL_DATE_SESSIONDATE?>" />
                    <input type="hidden" name="<?=COL_DATE_SESSIONTIME?>" />
                    <input type="hidden" name="<?=COL_KD_SESSIONVIA?>" />
                    <input type="hidden" name="<?=COL_KD_SESSION?>" />
                    <button type="button" id="btn-search-mentor" class="btn btn-outline-info"><i class="fas fa-arrow-right"></i>&nbsp;TAMPILKAN MENTOR / SESI</button>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
        </div>
      </div>
      <div id="list-mentor" class="row d-flex align-items-stretch">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-schedule" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Buat Jadwal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body pt-0 pb-0">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-outline-success btn-ok"><i class="far fa-check"></i>&nbsp;ATUR JADWAL</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var form = $('#form-main');
  var scheduleModal = $('#modal-schedule');
  var listEl = $('#list-mentor');
  var arrdays = new Array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');

  $('#btn-search-mentor').click(function() {
    var dis = $(this);
    var kdType = $('[name=<?=COL_KD_TYPE?>]', form).val();
    var isCareGroup = $('[name=<?=COL_IS_CAREGROUP?>]:checked', form).val();

    dis.attr('disabled', true);
    listEl.load('<?=site_url('site/ajax/list-mentor')?>', {'KD_Type': kdType, 'IS_CareGroup': isCareGroup}, function() {
      dis.attr('disabled', false);
      $('html, body').animate({
        scrollTop: listEl.offset().top-100
      }, 200);

      $('.btn-create-schedule', listEl).click(function() {
        var url = $(this).attr('href');
        var username = $(this).data('username');

        $('.modal-body', scheduleModal).load(url, function() {
          $('.datepicker', scheduleModal).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1900,
            maxYear: parseInt(moment().format('YYYY'),10),
            locale: {
                format: 'Y-MM-DD'
            }
          });

          $('[name=checkboxSchedule]', scheduleModal).change(function() {
            var sel = $('[name=checkboxSchedule]:checked', scheduleModal);
            var date = sel.data('date');
            var time = sel.data('timefrom');
            var day = sel.data('day');
            $('[name=<?=COL_DATE_SESSIONTIME?>]', scheduleModal).val(time).trigger('change');
            $('[name=<?=COL_DATE_SESSIONDATE?>]', scheduleModal).val(date).trigger('change');
            $('[name=<?=COL_DATE_SESSIONDATE?>]', scheduleModal).attr('readonly', (date?true:false));

            var optcal = {
              singleDatePicker: true,
              showDropdowns: true,
              minYear: parseInt(moment().format('YYYY'),10),
              maxYear: parseInt(moment().format('YYYY'),10),
              minDate: moment().add(moment.duration(3, 'd')),
              maxDate: moment().add(moment.duration(14, 'd')),
              locale: {
                  format: 'Y-MM-DD'
              }
            };
            if(day) {
              var _days = jQuery.grep(arrdays, function(value) {
                return value != day;
              });
              $.extend(optcal, {disabledDays: _days});
            } else {

            }
            $('.schedulepicker', scheduleModal).daterangepicker(optcal);
          });
          setTimeout(function() {
            $('[name=checkboxSchedule]:checked', scheduleModal).trigger('change');
          }, 200);

          $('[name=checkboxSchedule]:not(disabled)', scheduleModal).first().attr('checked', true);

          scheduleModal.modal('show');
        });

        $('[name=<?=COL_NM_SESSIONHOST?>]').val(username);
        return false;
      });

      $('.btn-join', listEl).click(function() {
        var kdSession = $(this).data('session');
        if(confirm('Yakin ingin bergabung?')) {
          $('[name=<?=COL_KD_SESSION?>]').val(kdSession);
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(data){
              if(data.error == 0) {
                toastr.success('Jadwal berhasil ditambahkan.');
                if(data.data.redirect) {
                  location.href = data.data.redirect;
                }
              } else {
                toastr.error(data.error);
              }
            },
            error: function(a,b,c){
              toastr.error('Response Error');
            }
          });
        }
        return false;
      });
    });
  });

  scheduleModal.on('hidden.bs.modal', function (e) {
    $('.modal-body', scheduleModal).empty();
  });

  $('.btn-ok', scheduleModal).click(function() {
    var tanggal = $('[name=DATE_SessionDate]', scheduleModal).val();
    var jam = $('[name=DATE_SessionTime]', scheduleModal).val();
    var format = $('[name=KD_SessionVia]:checked', scheduleModal).val();

    if(!tanggal || !jam || !format) {
      alert('Harap mengisi jadwal dengan benar!');
      return;
    }

    $('[name=<?=COL_DATE_SESSIONDATE?>]', form).val(tanggal);
    $('[name=<?=COL_DATE_SESSIONTIME?>]', form).val(jam);
    $('[name=<?=COL_KD_SESSIONVIA?>]', form).val(format);

    $(form).ajaxSubmit({
      dataType: 'json',
      type : 'post',
      success: function(data){
        if(data.error == 0) {
          toastr.success('Jadwal berhasil ditambahkan.');
          if(data.data.redirect) {
            location.href = data.data.redirect;
          }
        } else {
          toastr.error(data.error);
        }
      },
      error: function(a,b,c){
        toastr.error('Response Error');
      },
      complete: function() {
        //scheduleModal.modal('hide');
      }
    });

  });
});
</script>
