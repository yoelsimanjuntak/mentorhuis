<?php
$ruser = GetLoggedUser();
$qrate = $this->db
->select('AVG(NUM_Rate) as NUM_Rate')
->where(COL_KD_FEEDBACKTYPE, 'RATE')
->where(COL_KD_SESSION, $sesi[COL_KD_SESSION]);
if($ruser[COL_ROLEID] != ROLEPSIKOLOG) {
  $this->db->where(COL_CREATEDBY, $ruser[COL_USERNAME]);
}
$rate = $this->db->get(TBL_TSESSION_FEEDBACK)->row_array();

$rentry = $this->db
->where(COL_KD_SESSION, $sesi[COL_KD_SESSION])
->where(COL_USERNAME, $ruser[COL_USERNAME])
->get(TBL_TSESSION_ENTRY)
->row_array();

$osPayment = $this->db
->where(array(COL_KD_SESSION=>$sesi[COL_KD_SESSION], COL_KD_STATUSPAYMENT.'!='=>SESSIONPAY_VERIFIED))
->get(TBL_TSESSION_ENTRY)
->result_array();
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light">
              <?= $title ?>&nbsp;
              <span>
                <?php
                if(!empty($rate) && $rate[COL_NUM_RATE] != 0) {
                  $nrate = toNum($rate[COL_NUM_RATE]);
                  $rate = '';
                  $floating = ($nrate - floor($nrate)) != 0;
                  for($i=0; $i<floor($nrate); $i++) {
                    $rate .= '<i class="text-warning fas fa-star"></i>';
                  }
                  if($floating) {
                    $rate .= '<i class="text-warning fas fa-star-half-alt"></i>';
                  }
                  echo $rate;
                } else {
                  if($ruser[COL_ROLEID] != ROLEPSIKOLOG && $sesi[COL_KD_STATUS] == SESSIONSTAT_COMPLETED) {
                    ?>
                    <a href="<?=site_url('site/ajax/feedback-add/'.$sesi[COL_KD_SESSION].'/1')?>" class="btn btn-xs btn-outline-pallete-3 modal-popup-rating"><i class="far fa-star"></i>&nbsp;BERIKAN RATING</a>
                    <?php
                  }
                }
                ?>
              </span>
            </h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="card card-outline card-purple">
            <div class="card-header">
              <h5 class="card-title">Detail Sesi</h5>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <td style="width: 10vw">Mentor</td><td style="width: 2vw">:</td><td><?=$sesi['Mentor_FirstName'].' '.$sesi['Mentor_LastName']?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Layanan</td><td style="width: 2vw">:</td><td><?=$sesi[COL_NM_TYPE]?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Tanggal / Jam</td><td style="width: 2vw">:</td><td><?=date('Y-m-d', strtotime($sesi[COL_DATE_SESSIONDATE])).' '.$sesi[COL_DATE_SESSIONTIME]?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Format</td><td style="width: 2vw">:</td><td><?=$sesi[COL_KD_SESSIONVIA].($sesi[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2"><i class="fas fa-users"></i></small>':'')?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Status</td>
                    <td style="width: 2vw">:</td>
                    <td>
                      <?=$sesi[COL_KD_STATUS]?>
                      <?php
                      if($sesi[COL_KD_STATUS] == SESSIONSTAT_NEW && $ruser[COL_ROLEID]==ROLEPSIKOLOG && $ruser[COL_USERNAME]==$sesi[COL_NM_SESSIONHOST] && empty($osPayment)) {
                        ?>
                        <span class="pull-right">
                          <a href="<?=site_url('site/ajax/session-changestat/'.SESSIONSTAT_CANCELED.'/'.$sesi[COL_KD_SESSION])?>" class="btn btn-xs btn-danger btn-changestat"><i class="fas fa-times"></i>&nbsp;BATALKAN</a>
                          <a href="#" data-confirm-date="1" class="btn btn-xs btn-success btn-changestat"><i class="fas fa-check"></i>&nbsp;KONFIRMASI</a>
                        </span>
                        <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                  if($ruser[COL_ROLEID] != ROLEPSIKOLOG) {
                    if(!empty($rentry)) {
                      ?>
                      <tr>
                        <td style="width: 10vw">Status Bayar</td><td style="width: 2vw">:</td><td><?=$rentry[COL_KD_STATUSPAYMENT] == SESSIONPAY_WAITING && !empty($rentry[COL_NM_PAYMENTIMAGE]) ? 'PROSES VERIFIKASI' : $rentry[COL_KD_STATUSPAYMENT]?></td>
                      </tr>
                      <?php
                    }
                  }
                  ?>
                  <tr>
                    <td style="width: 10vw">Biaya</td><td style="width: 2vw">:</td>
                    <td>
                      Rp. <strong><?=number_format($sesi[COL_NUM_RATE])?></strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>

          <?php
          if($ruser[COL_ROLEID] == ROLEPSIKOLOG) {
            $participant = $this->db
            ->where(array(COL_KD_SESSION=>$sesi[COL_KD_SESSION]))
            ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
            ->get(TBL_TSESSION_ENTRY)
            ->result_array();
            ?>
            <div class="card card-outline card-purple">
              <div class="card-header">
                <h5 class="card-title">Peserta</h5>
              </div>
              <?php
              if(!$sesi[COL_IS_CAREGROUP]) {
                ?>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <td style="width: 12vw">Nama</td>
                        <td style="width: 2vw">:</td>
                        <td>
                          <a href="<?=site_url('site/ajax/get-member-detail/'.GetEncryption($participant[0][COL_USERNAME]))?>" class="link-profile"><?=$participant[0][COL_NM_FIRSTNAME].' '.$participant[0][COL_NM_LASTNAME]?></a>
                        </td>
                      </tr>
                      <tr>
                        <td style="width: 12vw">Status Bayar</td>
                        <td style="width: 2vw">:</td>
                        <td>
                          <?=$participant[0][COL_KD_STATUSPAYMENT] == SESSIONPAY_WAITING && !empty($participant[0][COL_NM_PAYMENTIMAGE]) ? 'PROSES VERIFIKASI' : $participant[0][COL_KD_STATUSPAYMENT]?></td>
                      </tr>
                      <tr>
                        <td style="width: 12vw">Notes - Perasaan</td>
                        <td style="width: 2vw">:</td>
                        <td><?=$participant[0][COL_NM_REMARKSCLIENTFEELING]?></td>
                      </tr>
                      <tr>
                        <td style="width: 12vw">Notes - Kondisi</td>
                        <td style="width: 2vw">:</td>
                        <td><?=$participant[0][COL_NM_REMARKSCLIENTCONDITION]?></td>
                      </tr>
                      <tr>
                        <td style="width: 12vw">Notes - Ekspektasi</td>
                        <td style="width: 2vw">:</td>
                        <td><?=$participant[0][COL_NM_REMARKSCLIENTEXPECTATION]?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <?php
                if($ruser[COL_ROLEID] == ROLEADMIN && $participant[0][COL_KD_STATUSPAYMENT] == SESSIONPAY_WAITING) {
                  ?>
                  <div class="card-footer text-center">
                    <a href="<?=site_url('site/ajax/session-change-status/'.SESSIONPAY_DECLINED.'/'.$participant[0][COL_UNIQ].'/1')?>" class="btn btn-sm btn-danger btn-change-status"><i class="fas fa-times"></i>&nbsp;TOLAK</a>
                    <a href="<?=site_url('site/ajax/session-change-status/'.SESSIONPAY_VERIFIED.'/'.$participant[0][COL_UNIQ])?>" class="btn btn-sm btn-success btn-change-status"><i class="fas fa-check"></i>&nbsp;KONFIRMASI</a>
                  </div>
                  <?php
                }
                if($ruser[COL_ROLEID] == ROLEPSIKOLOG && ($sesi[COL_KD_STATUS] == SESSIONSTAT_CONFIRMED || $sesi[COL_KD_STATUS] == SESSIONSTAT_COMPLETED)) {
                  ?>
                  <div class="card-footer text-center">
                    <?php
                    if($sesi[COL_KD_STATUS] == SESSIONSTAT_COMPLETED) {
                      ?>
                      <a href="<?=site_url('site/ajax/session-notes/'.$participant[0][COL_UNIQ])?>" class="btn btn-sm btn-info btn-popup-note"><i class="fas fa-pencil"></i>&nbsp;CATATAN</a>
                      <?php
                    } if($sesi[COL_KD_STATUS] == SESSIONSTAT_CONFIRMED) {
                      ?>
                      <a href="<?=site_url('site/ajax/session-changestat/'.SESSIONSTAT_COMPLETED.'/'.$sesi[COL_KD_SESSION])?>" class="btn btn-sm btn-success btn-change-status"><i class="fas fa-check"></i>&nbsp;SESI SELESAI</a>
                      <p class="text-danger text-sm text-left mt-3">
                        NB: Setelah klik <strong>SELESAI</strong>, selanjutnya berikan <strong>CATATAN</strong> yang perlu diketahui klien untuk sesi kali ini.
                      </p>
                      <?php
                    }
                    ?>
                  </div>
                  <?php
                }
                ?>
                <?php
              } else if($sesi[COL_IS_CAREGROUP] && count($participant) > 0) {
                ?>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <th>Nama</th>
                        <!--<th>Status Bayar</th>
                        <th>Bukti</th>-->
                        <th></th>
                      </tr>
                      <?php
                      foreach ($participant as $p) {
                        if($p[COL_KD_STATUSPAYMENT]!=SESSIONPAY_VERIFIED) continue;
                        ?>
                        <tr>
                          <td><?=$p[COL_NM_FIRSTNAME].' '.$p[COL_NM_LASTNAME]?></td>
                          <!--<td><?=$p[COL_KD_STATUSPAYMENT]?></td>
                          <td><?=!empty($p[COL_NM_PAYMENTIMAGE])?anchor(MY_UPLOADURL.$p[COL_NM_PAYMENTIMAGE],$p[COL_NM_PAYMENTIMAGE],array('class'=>'link-transfer-image')):'-'?></td>-->
                          <td class="text-center">
                            <?php
                            if($p[COL_KD_STATUSPAYMENT] == SESSIONPAY_WAITING) {
                              ?>
                              <a href="<?=site_url('site/ajax/session-change-status/'.SESSIONPAY_DECLINED.'/'.$p[COL_UNIQ])?>" class="btn btn-xs btn-danger btn-change-status" title="TOLAK"><i class="fas fa-times"></i></a>
                              <a href="<?=site_url('site/ajax/session-change-status/'.SESSIONPAY_VERIFIED.'/'.$p[COL_UNIQ])?>" class="btn btn-xs btn-success btn-change-status" title="KONFIRMASI"><i class="fas fa-check"></i></a>
                              <?php
                            } else if($sesi[COL_KD_STATUS] == SESSIONSTAT_CONFIRMED) {
                              ?>
                              <a href="<?=site_url('site/ajax/session-notes/'.$p[COL_UNIQ])?>" class="btn btn-xs btn-info btn-popup-note"><i class="fas fa-pencil"></i>&nbsp;CATATAN</a>
                              <?php
                            } else {
                              echo '-';
                            }
                            ?>
                          </td>
                        </tr>
                        <?php
                      }
                      ?>
                    </tbody>
                  </table>
                </div>
                <?php
                if($sesi[COL_KD_STATUS] == SESSIONSTAT_CONFIRMED) {
                  ?>
                  <div class="card-footer text-center">
                    <a href="<?=site_url('site/ajax/session-changestat/'.SESSIONSTAT_COMPLETED.'/'.$sesi[COL_KD_SESSION])?>" class="btn btn-sm btn-success btn-change-status"><i class="fas fa-check"></i>&nbsp;SESI SELESAI</a>
                  </div>
                  <?php
                }
                ?>

                <?php
              } else {
                ?>
                <div class="card-body">
                  <p>Peserta Kosong.</p>
                </div>
                <?php
              }
              ?>

            </div>
            <?php
          } else {
            if($rentry[COL_KD_STATUSPAYMENT] == SESSIONPAY_WAITING && empty($rentry[COL_NM_PAYMENTIMAGE])) {
              ?>
              <div class="card card-outline card-purple">
                <div class="card-header">
                  <h5 class="card-title">Cara Pembayaran</h5>
                </div>
                <div class="card-body p-0">
                  <table class="table table-striped">
                    <tbody>
                      <tr>
                        <td style="width: 10vw">Rekening</td><td style="width: 2vw">:</td>
                        <td>
                          <?php
                          $rbill = $this->db
                          ->get(TBL__SETTINGBILL)
                          ->result_array();
                          if(count($rbill) > 0) {
                            ?>
                            <ol class="pl-2">
                              <?php
                              foreach ($rbill as $b) {
                                ?>
                                <li><?=$b[COL_KD_BANK].' <strong>'.$b[COL_NM_ACCOUNTNO].'</strong> an. <strong>'.$b[COL_NM_ACCOUNTNAME].'</strong>'?></li>
                                <?php
                              }
                              ?>
                            </ol>
                            <?php
                          } else {
                            echo '--';
                          }
                          ?>
                        </td>
                      </tr>
                      <tr>
                        <td style="width: 10vw">Jumlah</td><td style="width: 2vw">:</td>
                        <td>
                          Rp. <strong><?=number_format($sesi[COL_NUM_RATE])?></strong>
                          <p class="font-italic"><small>Harap mentransfer jumlah yang sesuai tertera diatas dengan mencantumkan berita acara "<strong><?='#MH-'.str_pad($sesi[COL_KD_SESSION], 5, '0', STR_PAD_LEFT)?></strong>"</small></p>
                        </td>
                      </tr>
                      <tr>
                        <td style="width: 10vw">Bukti Transfer</td><td style="width: 2vw">:</td>
                        <td>
                          <?=form_open_multipart(site_url('site/ajax/session-upload-payment/'.$sesi[COL_KD_SESSION]),array('role'=>'form','id'=>'form-upload','class'=>'form-horizontal'))?>
                          <div class="input-group">
                            <div class="custom-file">
                              <input id="upload" type="file" class="custom-file-input" name="<?=COL_NM_PAYMENTIMAGE?>" accept="image/*" required  />
                              <label class="custom-file-label font-italic" for="upload">Upload bukti transfer</label>
                            </div>
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-success btn-sm">
                                <i class="fas fa-upload"></i>&nbsp;Upload</span>
                              </button>
                            </div>
                          </div>
                          <p class="text-sm font-italic text-danger mt-1">
                            Catatan: ukuran maks. 2MB dengan dimensi maks 2048 x 2048
                          </p>
                          <?=form_close()?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php
            }
          }
          ?>

        </div>
        <div class="col-sm-6">
          <div id="card-messages" class="card direct-chat direct-chat-info card-outline card-purple">
            <div class="card-header">
              <h5 class="card-title m-0"><i class="far fa-comment-alt-lines"></i>&nbsp;INTERAKSI</h5>
              <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('site/sesi/load-comments/'.$sesi[COL_KD_SESSION])?>"><i class="fas fa-sync-alt"></i></button>
              </div>
            </div>
            <div class="card-body">
              <div class="card-body p-0">

              </div>
            </div>
            <?php
            if(!($sesi[COL_KD_STATUS]==SESSIONSTAT_COMPLETED || $sesi[COL_KD_STATUS]==SESSIONSTAT_CANCELED)) {
              ?>
              <div class="card-footer">
                <form id="comment-form" action="<?=site_url('site/ajax/session-add-comment/'.$sesi[COL_KD_SESSION])?>" method="post">
                  <div class="input-group">
                    <input type="text" name="<?=COL_NM_FEEDBACKTEXT?>" placeholder="Ketik pesan / komentar ..." class="form-control">
                    <span class="input-group-append">
                      <button id="btn-add-comment" type="submit" class="btn btn-outline-info"><i class="fad fa-paper-plane"></i>&nbsp;KIRIM</button>
                    </span>
                  </div>
                </form>
              </div>
              <?php
            }
             ?>
          </div>
        </div>
        <?php
        if($sesi[COL_KD_STATUS]==SESSIONSTAT_COMPLETED && !empty($rentry)) {
          ?>
          <div class="col-sm-12">
            <div class="card card-outline card-purple">
              <div class="card-header">
                <h5 class="card-title">CATATAN</h5>
              </div>
              <div class="card-body">
                <p>
                  <?=!empty($rentry[COL_NM_REMARKSMENTOR])?$rentry[COL_NM_REMARKSMENTOR]:'Tidak ada catatan.'?>
                </p>
              </div>
            </div>
          </div>
          <?php
        }
         ?>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-notes" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Catatan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-payment" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Pembayaran</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-profile" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Profil</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-rating" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Beri Rating</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-times fa-sm"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor-rating" method="post" action="#">
          <div class="form-group row">
            <label class="control-label col-sm-4">Rating</label>
            <div class="col-sm-8">
              <div class="rating">
                <label>
                  <input type="radio" name="<?=COL_NUM_RATE?>" value="1" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_NUM_RATE?>" value="2" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_NUM_RATE?>" value="3" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_NUM_RATE?>" value="4" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
                <label>
                  <input type="radio" name="<?=COL_NUM_RATE?>" value="5" />
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                  <span class="icon"><i class="fas fa-star"></i></span>
                </label>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-4">Feedback</label>
            <div class="col-sm-8">
              <textarea class="form-control" rows="3" name="<?=COL_NM_FEEDBACKTEXT?>" placeholder="Ketikkan komentar anda..."></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;Cancel</button>
        <button type="button" class="btn btn-primary btn-ok"><i class="fas fa-pencil"></i>&nbsp;Submit</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-sessiondate" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Tanggal & Waktu</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor-date" class="form-horizontal" method="post" action="<?=site_url('site/ajax/session-changestat/'.SESSIONSTAT_CONFIRMED.'/'.$sesi[COL_KD_SESSION])?>">
          <div class="form-group row">
            <label class="control-label col-sm-4">Tanggal</label>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="<?=COL_DATE_SESSIONDATE?>" value="<?=$sesi[COL_DATE_SESSIONDATE]?>" disabled />
            </div>
          </div>
          <div class="form-group row">
            <label class="control-label col-sm-4">Waktu</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" name="<?=COL_DATE_SESSIONTIME?>" value="<?=$sesi[COL_DATE_SESSIONTIME]?>" />
            </div>
          </div>
          <div class="form-group row pt-2" style="border-top: 1px solid #f4f4f4">
            <div class="col-sm-12 text-center">
              <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;Batal</button>&nbsp;
              <button type="button" class="btn btn-primary btn-ok"><i class="fas fa-check"></i>&nbsp;Konfirmasi</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function scrollMessages() {
  var el = $('.direct-chat-messages');
  var msgHeightScroll = el[0].scrollHeight;
  var msgHeight = el.height();
  var scrollVal = msgHeightScroll - msgHeight;
  el.stop().animate({scrollTop:scrollVal}, 500, 'swing', function() {

  });
}

$(document).ready(function() {
  var modalNotes = $('#modal-notes');
  var modalProfile = $('#modal-profile');
  var modalPayment = $('#modal-payment');

  $('[name=<?=COL_DATE_SESSIONTIME?>]').timepicker({
    timeFormat: 'HH:mm',
    interval: 30,
    minTime: '7',
    maxTime: '21',
    defaultTime: '<?=$sesi[COL_DATE_SESSIONTIME]?>',
    startTime: '07:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    zindex: 9999999
  });

  $('.modal:not(#modal-rating, #modal-sessiondate)').on('hidden.bs.modal', function (event) {
      $(this).find(".modal-body").empty();
  });
  $('.btn-tool[data-card-widget=card-refresh]', $("#card-messages")).click();
  $('#btn-add-comment').click(function() {
    var dis = $(this);
    var msg = $('[name=<?=COL_NM_FEEDBACKTEXT?>]', $("#comment-form")).val();
    if(!msg) {
      toastr.error('Komentar kosong.');
      return false;
    }
    dis.html('Loading...').attr('disabled', true);
    $('#comment-form').ajaxSubmit({
        dataType: 'json',
        success : function(data){
          if(data.error==0){
            toastr.success('Komentar ditambahkan.');
            $('.btn-tool[data-card-widget=card-refresh]', $("#card-messages")).click();
            setTimeout(function() {
              scrollMessages();
            }, 1000);
          } else{
            toastr.error(data.error);
          }
        },
        error: function() {
          toastr.error('Server Problem');
        },
        complete: function() {
          dis.html('<i class="fad fa-paper-plane"></i>&nbsp;KIRIM').attr("disabled", false);
          $('[name=<?=COL_NM_FEEDBACKTEXT?>]', $("#comment-form")).val('');
        }
    });
    return false;
  });

  $('.btn-change-status').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.post(url, function(data) {
        if(data.error == 0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      }, "json").fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });

  $('.btn-popup-note').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalNotes).load(url, function() {
      modalNotes.modal('show');
      $('button[type=submit]').click(function() {
        $('form', modalNotes).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(data){
            if(data.error == 0) {
              toastr.success('Catatan berhasil diubah.');
            } else {
              toastr.error(data.error);
            }
          },
          error: function(a,b,c){
            toastr.error('Response Error');
          },
          complete: function() {
            modalNotes.modal('hide');
          }
        });

        return false;
      });
    });
    return false;
  });

  $('.link-profile').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalProfile).load(url, function() {
      modalProfile.modal('show');
    });
    return false;
  });
  $('.link-transfer-image').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalPayment).append('<p class="text-center"><img src="'+url+'" /></p>');
    modalPayment.modal('show');
    return false;
  });
  $('.modal-popup-rating').click(function(){
    var a = $(this);
    var editor = $("#modal-rating");

    editor.modal("show");
    $(".btn-ok", editor).unbind('click').click(function() {
      var dis = $(this);
      dis.html("Loading...").attr("disabled", true);
      $('#form-editor-rating').ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if(data.error==0){
            window.location.reload();
          }else{
            toastr.error(data.error);
          }
        },
        error: function() {
          toastr.error('Server Problem');
        },
        complete: function() {
          dis.html('<i class="fas fa-pencil"></i>&nbsp;Submit').attr("disabled", false);
          editor.modal("hide");
        }
      });
    });
    return false;
  });

  $('.btn-ok', $('#modal-sessiondate')).unbind('click').click(function() {
    $('form', $('#modal-sessiondate')).ajaxSubmit({
      dataType: 'json',
      success : function(data){
        if(data.error==0){
          toastr.success('Status berhasil diubah.');
          setTimeout(function() {
            location.reload();
          }, 1000);
        } else{
          toastr.error(data.error);
        }
      },
      error: function() {
        toastr.error('Server Problem');
      },
      complete: function() {
        $('#modal-sessiondate').modal("hide");
      }
    });
  });

  $('.btn-changestat').click(function() {
    var url = $(this).attr('href');
    var confirmdate = $(this).data('confirm-date');
    if(confirmdate) {
      $('#modal-sessiondate').modal("show");
    } else if(confirm('Apakah anda yakin?')) {
      $.post(url, function(data) {
        if(data.error == 0) {
          location.reload();
        } else {
          toastr.error(data.error);
        }
      }, "json").fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });

  $('#form-upload').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
