<?php
$displaypicture = MY_IMAGEURL.'user.jpg';
if(!empty($data[COL_NM_PROFILEIMAGE])) {
    $displaypicture = $data[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$data[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
?>
<div class="row pt-3">
  <div class="col-sm-12">
    <div class="text-center mb-3">
      <img class="profile-user-img img-circle" src="<?=$displaypicture?>" alt="Profile">
    </div>
    <table class="table table-striped mb-0">
      <tbody>
        <tr>
          <td>Nama</td><td>:</td><td class="font-weight-bold"><?=$data[COL_NM_FIRSTNAME].' '.$data[COL_NM_LASTNAME]?></td>
        </tr>
        <tr>
          <td>Umur</td><td>:</td>
          <td class="font-weight-bold">
            <?php
            if(!empty($data[COL_DATE_BIRTH])) {
              $from = new DateTime($data[COL_DATE_BIRTH]);
              $to   = new DateTime('today');
              echo $from->diff($to)->y.' tahun';
            }
            ?>
          </td>
        </tr>
        <tr>
          <td>Pekerjaan</td><td>:</td><td class="font-weight-bold"><?=$data[COL_NM_OCCUPATION]?></td>
        </tr>
        <tr>
          <td>Pengalaman</td><td>:</td><td class="font-weight-bold"><?=$data[COL_NUM_EXPERIENCE]?> tahun</td>
        </tr>
        <tr>
          <td>Pendidikan</td><td>:</td>
          <td class="font-weight-bold">
            <?php
            $edu = json_decode($data[COL_NM_EDUCATIONALBACKGROUND]);
            if(count($edu) > 1) {
              echo '<ul style="padding-left: 1.5rem">';
              foreach ($edu as $e) {
                echo '<li>'.$e->Grade.' ('.$e->Institusi.')</li>';
              }
              echo '</ul>';
            } else if(count($edu) == 1) {
              echo $edu[0]->Grade.' ('.$edu[0]->Institusi.')';
            } else {
              echo '-';
            }
            ?>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
