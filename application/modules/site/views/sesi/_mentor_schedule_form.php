<?php
$rrate = $this->db
//->where(COL_NUM_EXPFROM.' <=', $mentor[COL_NUM_EXPERIENCE])
//->where(COL_NUM_EXPTO.' >=', $mentor[COL_NUM_EXPERIENCE])
->where(COL_NM_GRADE, $mentor[COL_NM_GRADE])
->order_by(COL_NUM_EXPFROM, 'desc')
->get(TBL__SETTINGRATE)
->row_array();
?>
<div class="row">
  <div class="col-sm-6 pr-3">
    <?php
    if(!empty($scheduleFixed) || !empty($scheduleCalendar)) {
      ?>
      <p class="mt-2"><strong class="color-pallete-1">1.</strong>&nbsp;Pilih jadwal yang tersedia.</p>
      <table id="tbl-schedule-fixed" class="table table-condensed table-bordered">
        <tbody>
          <tr>
            <th>#</th>
            <th>Hari / Tanggal</th>
            <th class="text-center">Jam</th>
          </tr>
          <?php
          /*foreach($scheduleFixed as $s) {
            ?>
            <tr>
              <th>#</th>
              <td style="padding: 0.25rem"><?=$s[COL_NM_DAY]?> <small class="pull-right">(Berlaku setiap minggu)</small></td>
              <td class="text-center"><?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?></td>
            </tr>
            <?php
          }
          foreach($scheduleCalendar as $s) {
            ?>
            <tr>
              <th>#</th>
              <td style="padding: 0.25rem"><?= date('d-m-Y', strtotime($s[COL_DATE_SCHEDULEDATE]))?></td>
              <td class="text-center"><?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?></td>
            </tr>
            <?php
          }*/
          $n = 0;
          foreach($schedule as $s) {
            $exist = false;
            if($s[COL_KD_SCHEDULETYPE]=='CALENDAR') {
              $rsesi = $this->db
              ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_KD_SESSION." = ".TBL_TSESSION_ENTRY.".".COL_KD_SESSION,"inner")
              ->where(COL_KD_STATUS."!=", SESSIONSTAT_CANCELED)
              ->where(COL_NM_SESSIONHOST, $mentor[COL_USERNAME])
              ->where(COL_NM_PAYMENTIMAGE."!=", null)
              ->where(COL_KD_STATUSPAYMENT."!=", SESSIONPAY_DECLINED)
              ->where(COL_DATE_SESSIONDATE, $s[COL_DATE_SCHEDULEDATE])
              ->where(COL_DATE_SESSIONTIME, $s[COL_DATE_SCHEDULETIME_FROM])
              ->get(TBL_TSESSION_ENTRY)
              ->row_array();
              if(!empty($rsesi)) {
                $exist = true;
              }
            }
            ?>
            <tr <?=$exist?'class="text-muted"':''?>>
              <td style="padding: 0.25rem" class="text-center">
                <input type="radio" id=<?=$s[COL_UNIQ]?> name="checkboxSchedule" data-date="<?=$s[COL_KD_SCHEDULETYPE]!='FIXED'?date('Y-m-d', strtotime($s[COL_DATE_SCHEDULEDATE])):''?>" data-timefrom="<?=$s[COL_DATE_SCHEDULETIME_FROM]?>"  data-day="<?=$s[COL_KD_SCHEDULETYPE]=='FIXED'?date('D', strtotime("Sunday +".$s[COL_KD_SCHEDULEDAY]." days")):""?>" <?=$exist?'disabled':''?> />
              </td>
              <td style="padding: 0.25rem">
                <label for="<?=$s[COL_UNIQ]?>" class="font-weight-normal m-0">
                  <?=$s[COL_KD_SCHEDULETYPE]=='FIXED'?$s[COL_NM_DAY]:date('Y-m-d', strtotime($s[COL_DATE_SCHEDULEDATE]))?>
                </label>
                &nbsp;
                <?=$s[COL_KD_SCHEDULETYPE]=='FIXED'?'<small class="pull-right">(Berlaku setiap minggu)</small>':''?>
              </td>
              <td style="padding: 0.25rem" class="text-center">
                <label for="<?=$s[COL_UNIQ]?>" class="font-weight-normal m-0">
                  <?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?>
                </label>
              </td>
            </tr>
            <?php
            $n++;
          }
          ?>
        </tbody>
      </table>
      <?php
    } else {
      echo '<p class="p-3">Maaf, Mentor ini belum memiliki jadwal.</p>';
    }
    ?>
  </div>
  <div class="col-sm-6" style="border-left: 1px solid #f4f4f4">
    <div class="col-sm-12">
      <p class="mt-2"><strong class="color-pallete-1">2.</strong>&nbsp;Tentukan jadwal pertemuan.</p>
      <div class="form-group row">
        <label class="control-label col-sm-4">Tanggal</label>
        <div class="col-sm-5">
          <input type="text" class="form-control schedulepicker" name="<?=COL_DATE_SESSIONDATE?>" placeholder="yyyy-mm-dd" />
        </div>
      </div>
      <div class="form-group row">
        <label class="control-label col-sm-4">Jam</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="<?=COL_DATE_SESSIONTIME?>" placeholder="00:00" readonly />
        </div>
      </div>
      <div class="form-group row">
        <label class="control-label col-sm-4">Format</label>
        <div class="col-sm-8">
          <table>
            <?php
            if(!empty($rrate)&&!empty($rrate[COL_NUM_RATEREGULAR])) {
              ?>
              <tr>
                <td style="padding-left: 1.25rem" class="pr-2"><input id="RadioFormatReguler" class="form-check-input" type="radio" name="<?=COL_KD_SESSIONVIA?>" value="<?=SESSIONFORMAT_REGULER?>" checked /><label class="form-check-label" for="RadioFormatReguler">Reguler</label></td>
                <td class="pr-1">Rp.</td>
                <td class="text-right font-weight-bold"><?=number_format($rrate[COL_NUM_RATEREGULAR])?></td>
              </tr>
              <?php
            }
            if(!empty($rrate)&&!empty($rrate[COL_NUM_RATEONLINE]))  {
              ?>
              <tr>
                <td style="padding-left: 1.25rem" class="pr-2"><input id="RadioFormatOnline" class="form-check-input" type="radio" name="<?=COL_KD_SESSIONVIA?>" value="<?=SESSIONFORMAT_ONLINE?>" /><label class="form-check-label" for="RadioFormatOnline">Online</label></td>
                <td class="pr-1">Rp.</td>
                <td class="text-right font-weight-bold"><?=number_format($rrate[COL_NUM_RATEONLINE])?></td>
              </tr>
              <?php
            }
            ?>

          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {

});
</script>
