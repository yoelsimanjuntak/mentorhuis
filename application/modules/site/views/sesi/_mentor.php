<?php
if(count($res) > 0) {
  foreach($res as  $r) {
    $imgurl = !empty($r[COL_NM_PROFILEIMAGE]) ? MY_UPLOADURL.$r[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
    $rrate = $this->db
    ->select('AVG(tsession_feedback.NUM_Rate) as NUM_Rate')
    ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_KD_SESSION." = ".TBL_TSESSION_FEEDBACK.".".COL_KD_SESSION,"left")
    ->where(COL_KD_FEEDBACKTYPE, 'RATE')
    ->where(TBL_TSESSION.".".COL_NM_SESSIONHOST, $r[COL_USERNAME])
    ->get(TBL_TSESSION_FEEDBACK)
    ->row_array();

    $nrate = toNum($rrate[COL_NUM_RATE]);
    $floating = ($nrate - floor($nrate)) != 0;
    $rate = '';
    if($nrate==0) {
      $rate = '<small class="font-italic">Belum ada rating.</small>';
    }

    for($i=0; $i<floor($nrate); $i++) {
      $rate .= '<i class="text-warning fas fa-star"></i>';
    }
    if($floating) {
      $rate .= '<i class="text-warning fas fa-star-half-alt"></i>';
    }
    ?>
    <div class="col-12 col-lg-3 col-md-3 col-sm-3 d-flex align-items-stretch">
      <div class="card bg-light">
        <div class="card-header text-muted border-bottom-0">
          <?=$r[COL_NM_FIRSTNAME].' '.$r[COL_NM_LASTNAME]?>
        </div>
        <div class="card-body pt-0">
          <div class="row">
            <div class="col-sm-7">
              <h2 class="lead text-sm"><?=$rate?></h2>
              <p class="text-sm font-weight-light">
                <?=$r[COL_NM_BIO]?>
              </p>
              <!--<p class="text-muted text-sm">
                <strong>Alamat :</strong><br />
                <?=empty($r[COL_NM_ADDRESS])&&empty($r[COL_NM_CITY])&&empty($r[COL_NM_PROVINCE])?'-':$r[COL_NM_ADDRESS].', '.$r[COL_NM_CITY].', '.$r[COL_NM_PROVINCE]?>
              </p>-->
              <!--<ul class="ml-4 mb-0 fa-ul text-muted">
                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Demo Street 123, Demo City 04312, NJ</li>
                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: + 800 - 12 12 23 52</li>
              </ul>-->
            </div>
            <div class="col-sm-5 text-center">
              <img src="<?=$imgurl?>" alt="<?=$r[COL_NM_FIRSTNAME]?>" class="profile-user-img img-fluid" style="width: 128px; border-radius: 10%">
              <!--<p class="mt-2 mb-0">
                <i class="fab fa-instagram text-danger"></i>&nbsp;
                <i class="fab fa-facebook-square text-primary"></i>&nbsp;
                <i class="fab fa-linkedin text-info"></i>
              </p>-->
            </div>
          </div>
        </div>
        <div class="card-footer">
          <div class="text-left">
            <a href="<?=site_url('site/ajax/get-mentor-profile/'.GetEncryption($r[COL_USERNAME]))?>" class="btn mb-1 btn-sm text-sm bg-purple btn-view-profile btn-block text-left" data-toggle="tooltip" data-placement="top" title="Profil">
              <i class="far fa-user"></i>&nbsp;&nbsp;Profil Mentor
            </a>
            <a href="<?=site_url('site/ajax/get-mentor-history/'.GetEncryption($r[COL_USERNAME]))?>" class="btn mb-1 btn-sm text-sm bg-teal btn-view-history btn-block text-left" data-toggle="tooltip" data-placement="top" title="Riwayat Konseling">
              <i class="far fa-history"></i>&nbsp;&nbsp;Riwayat Konseling
            </a>
            <a href="<?=site_url('site/ajax/form-mentor-schedule/'.GetEncryption($r[COL_USERNAME]))?>" class="btn mb-1 btn-sm text-sm btn-primary btn-create-schedule btn-block text-left" data-username="<?=$r[COL_USERNAME]?>" data-toggle="tooltip" data-placement="top" title="Buat Jadwal">
              <i class="far fa-calendar-plus"></i>&nbsp;&nbsp;Buat Jadwal
            </a>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
} else {
  ?>
  <div class="col-sm-12">
    <div class="card">
      <div class="card-body">
        <p class="text-danger font-italic text-center">
          Maaf, untuk saat ini kami belum bisa mencarikan mentor yang cocok untuk kamu.
        </p>
      </div>
    </div>
  </div>
  <?php
}
?>
<div class="modal fade" id="modal-info" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Riwayat Konseling Terakhir</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body p-0">
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var modalInfo = $('#modal-info');
  $('.modal').on('hidden.bs.modal', function (event) {
      $(this).find(".modal-body").empty();
  });

  $('.btn-view-history').click(function() {
    var url = $(this).attr('href');
    $('.modal-title', modalInfo).html('Riwayat Konseling');
    $('.modal-body', modalInfo).load(url, function() {
      modalInfo.modal('show');
    });
    return false;
  });

  $('.btn-view-profile').click(function() {
    var url = $(this).attr('href');
    $('.modal-title', modalInfo).html('Profil');
    $('.modal-body', modalInfo).load(url, function() {
      modalInfo.modal('show');
    });
    return false;
  });

  $('[data-toggle="tooltip"]').tooltip();
});
</script>
