<?php
$ruser = GetLoggedUser();
$uname = $ruser[COL_USERNAME];
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light"><?= $title ?></h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item"><a href="<?=site_url('site/sesi/index')?>"> Jadwal Saya</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title">Silakan isi form berikut sebelum memulai sesi.</h5>
            </div>
            <div class="card-body">
              <?=form_open(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group row">
                    <label class="control label col-sm-4">Jenis Konsultasi</label>
                    <div class="col-sm-8">
                      <select name="<?=COL_KD_TYPE?>" class="form-control" required>
                        <?=GetCombobox("select mtype.* from usertype ut inner join mtype on mtype.KD_Type = ut.KD_Type where ut.UserName = '$uname' and mtype.KD_Category = 'PERSONAL' order by mtype.NM_Type", COL_KD_TYPE, COL_NM_TYPE)?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control label col-sm-4">Tanggal / Jam</label>
                    <div class="col-sm-5">
                      <input type="text" class="form-control datepicker" name="<?=COL_DATE_SESSIONDATE?>" placeholder="yyyy-mm-dd" required />
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" name="<?=COL_DATE_SESSIONTIME?>" placeholder="00:00" required />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">Format</label>
                    <div class="col-sm-8">
                      <div class="form-check d-inline-block mr-3">
                        <input id="RadioFormatReguler" class="form-check-input" type="radio" name="<?=COL_KD_SESSIONVIA?>" value="<?=SESSIONFORMAT_REGULER?>" checked />
                        <label class="form-check-label" for="RadioFormatReguler">REGULER (Tatap Muka)</label>
                      </div>
                      <div class="form-check d-inline-block mr-3">
                        <input id="RadioFormatOnline" class="form-check-input" type="radio" name="<?=COL_KD_SESSIONVIA?>" value="<?=SESSIONFORMAT_ONLINE?>" <?=empty($ruser[COL_NUM_RATEREGULAR])?'checked':''?> />
                        <label class="form-check-label" for="RadioFormatOnline">ONLINE</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-4 offset-sm-2">
                  <div class="form-group row">
                    <label class="control label col-sm-4">Tarif</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control uang text-right" name="<?=COL_NUM_RATE?>" placeholder="Rp. 0" required />
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control label col-sm-4">Jlh. Peserta</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control uang text-right" name="<?=COL_NUM_QUOTA?>" placeholder="0" required />
                    </div>
                  </div>
                </div>
              </div>
              <div class="row text-center" style="border-top: 1px solid rgba(0,0,0,.125); margin-left: -20px; margin-right: -20px">
                <div class="col-sm-12 mt-3 mb-0">
                  <input type="hidden" name="<?=COL_NM_SESSIONHOST?>" value="<?=$ruser[COL_USERNAME]?>" />
                  <a href="<?=site_url('site/sesi/index')?>" class="btn btn-outline-secondary"><i class="fas fa-arrow-left"></i>&nbsp;BATAL</a>
                  <button type="submit" class="btn btn-outline-info"><i class="fas fa-check"></i>&nbsp;BUAT SESI</button>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
        </div>
      </div>
      <div id="list-mentor" class="row d-flex align-items-stretch">
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var form = $('#form-main');
});
</script>
