<?php
$ruser = GetLoggedUser();
$uname = $ruser[COL_USERNAME];
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light"><?= $title ?></h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 mb-2">
          <button id="btn-schedule-setting" class="btn btn-primary btn-block"><i class="far fa-calendar-alt"></i>&nbsp;JADWAL PRIBADI</button>
        </div>
        <div class="col-sm-6 mb-2">
          <button id="btn-caregroup-setting" class="btn btn-success btn-block"><i class="far fa-calendar-day"></i>&nbsp;JADWAL CAREGROUP</button>
        </div>
        <div class="col-sm-12">
          <div class="card card-outline card-secondary">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Sesi Belum Terkonfirmasi</h5>
            </div>
            <div class="card-body p-0 table-responsive">
              <table class="table table-striped" width="100%">
                <thead>
                  <tr>
                    <th>No. Pesanan</th>
                    <th>Tanggal / Jam</th>
                    <th>Status</th>
                    <th>Klien</th>
                    <th>Layanan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($session_new) > 0) {
                    foreach ($session_new as $ses) {
                      $clients = '';
                      if($ses[COL_IS_CAREGROUP]) {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->result_array();
                        if(!empty($rentry)) {
                          $clients = number_format(count($rentry)).' orang';
                        } else {
                          $clients = '-';
                        }

                      } else {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->row_array();
                        if($rentry) {
                          $clients = $rentry[COL_NM_FIRSTNAME].' '.$rentry[COL_NM_LASTNAME];
                        }
                      }
                      ?>
                      <tr>
                        <td><?=anchor(site_url('site/sesi/detail/'.$ses[COL_KD_SESSION]), 'MH-'.str_pad($ses[COL_KD_SESSION], 5, '0', STR_PAD_LEFT))?></td>
                        <td><?=date('Y-m-d', strtotime($ses[COL_DATE_SESSIONDATE])).' '.$ses[COL_DATE_SESSIONTIME]?></td>
                        <td><?=$ses[COL_KD_STATUS]?></td>
                        <td><?=$clients?></td>
                        <td><?=$ses[COL_NM_TYPE].($ses[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2"><i class="fas fa-users"></i></small>':'')?></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="5">
                        <p class="font-italic">
                          Tidak ada data.
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="card card-outline card-primary">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Sesi Terkonfirmasi</h5>
            </div>
            <div class="card-body p-0 table-responsive">
              <table class="table table-striped" width="100%">
                <thead>
                  <tr>
                    <th>No. Pesanan</th>
                    <th>Tanggal / Jam</th>
                    <th>Status</th>
                    <th>Klien</th>
                    <th>Layanan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($session_confirmed) > 0) {
                    foreach ($session_confirmed as $ses) {
                      $clients = '';
                      if($ses[COL_IS_CAREGROUP]) {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->result_array();
                        if(!empty($rentry)) {
                          $clients = number_format(count($rentry)).' orang';
                        } else {
                          $clients = '-';
                        }

                      } else {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->row_array();
                        if($rentry) {
                          $clients = $rentry[COL_NM_FIRSTNAME].' '.$rentry[COL_NM_LASTNAME];
                        }
                      }
                      ?>
                      <tr>
                        <td><?=anchor(site_url('site/sesi/detail/'.$ses[COL_KD_SESSION]), 'MH-'.str_pad($ses[COL_KD_SESSION], 5, '0', STR_PAD_LEFT))?></td>
                        <td><?=date('Y-m-d', strtotime($ses[COL_DATE_SESSIONDATE])).' '.$ses[COL_DATE_SESSIONTIME]?></td>
                        <td><?=$ses[COL_KD_STATUS]?></td>
                        <td><?=$clients?></td>
                        <td><?=$ses[COL_NM_TYPE].($ses[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2"><i class="fas fa-users"></i></small>':'')?></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="5">
                        <p class="font-italic">
                          Tidak ada data.
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="card card-outline card-success">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Sesi Selesai / Batal</h5>
            </div>
            <div class="card-body p-0 table-responsive">
              <table class="table table-striped" width="100%">
                <thead>
                  <tr>
                    <th>No. Pesanan</th>
                    <th>Tanggal / Jam</th>
                    <th>Status</th>
                    <th>Klien</th>
                    <th>Layanan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($session_completed) > 0) {
                    foreach ($session_completed as $ses) {
                      $clients = '';
                      if($ses[COL_IS_CAREGROUP]) {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->result_array();
                        if(!empty($rentry)) {
                          $clients = number_format(count($rentry)).' orang';
                        } else {
                          $clients = '-';
                        }

                      } else {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->row_array();
                        if($rentry) {
                          $clients = $rentry[COL_NM_FIRSTNAME].' '.$rentry[COL_NM_LASTNAME];
                        }
                      }
                      ?>
                      <tr>
                        <td><?=anchor(site_url('site/sesi/detail/'.$ses[COL_KD_SESSION]), 'MH-'.str_pad($ses[COL_KD_SESSION], 5, '0', STR_PAD_LEFT))?></td>
                        <td><?=date('Y-m-d', strtotime($ses[COL_DATE_SESSIONDATE])).' '.$ses[COL_DATE_SESSIONTIME]?></td>
                        <td><?=$ses[COL_KD_STATUS]?></td>
                        <td><?=$clients?></td>
                        <td><?=$ses[COL_NM_TYPE].($ses[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2"><i class="fas fa-users"></i></small>':'')?></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="5">
                        <p class="font-italic">
                          Tidak ada data.
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-schedule-setting" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Pengaturan Jadwal</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <?=form_open(site_url('site/ajax/user-setting'),array('role'=>'form','id'=>'form-schedule-setting','class'=>'form-horizontal'))?>
      <div class="modal-body">
        <small class="font-italic text-danger">Note: Durasi per sesi standar antara 50 - 60 menit dan harap memperhatikan waktu persiapan antar sesi (idealnya 5-10 menit)</small>
        <div class="form-group">
          <input type="hidden" name="ScheduleFixed" />
          <input type="hidden" name="ScheduleCalendar" />
          <div class="row">
            <div class="col-sm-12">
              <div class="mt-2 mb-3">
                <table id="tbl-schedule-fixed" class="table table-bordered">
                  <thead>
                    <tr class="row-checkbox">
                      <th colspan="3">
                        <div class="form-check">
                          <input id="scheduleFixed" name="ScheduleTypeFixed" class="form-check-input" type="checkbox" <?=!empty($rScheduleFixed)?'checked':''?>>&nbsp;
                          <label class="form-check-label" for="scheduleFixed">TETAP <small>(Jadwal rutin berdasarkan hari dan jam)</small></label>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <th>Hari</th>
                      <th>Jam</th>
                      <th class="text-center">#</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach($rScheduleFixed as $s) {
                      ?>
                      <tr>
                        <td><?=$s[COL_NM_DAY]?></td>
                        <td><?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?></td>
                        <td class="text-center">
                          <button type="button" class="btn btn-xs btn-outline-danger btn-del"><i class="fas fa-minus"></i></button>
                          <input type="hidden" name="idx" value="<?=$s[COL_KD_DAY]?>" />
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>
                        <select name="Day" class="form-control form-control-sm no-select2" style="min-width: 10vw">
                          <?=GetCombobox("SELECT * from mdays order by KD_Day", COL_KD_DAY, COL_NM_DAY, null, true, false, '-- Hari --')?>
                        </select>
                      </th>
                      <th class="text-center" style="white-space: nowrap">
                        <input type="text" class="form-control d-inline-block timepicker" id="timepickerFrom" name="TimeFrom" placeholder="HH:MM" style="width: 40%; text-align: center" readonly />
                        <span class="p-1">-</span>
                        <input type="text" class="form-control d-inline-block" id="timepickerTo" name="TimeTo" placeholder="HH:MM" style="width: 40%; text-align: center" readonly />
                      </th>
                      <th class="text-center">
                        <button type="button" id="btn-add-schedule-fixed" class="btn btn-xs btn-default"><i class="fa fa-plus"></i></button>
                      </th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <div class="mt-2 mb-3">
                <table id="tbl-schedule-calendar" class="table table-bordered">
                  <thead>
                    <tr class="row-checkbox">
                      <th colspan="3">
                        <div class="form-check">
                          <input id="scheduleCalendar" name="ScheduleTypeCalendar" class="form-check-input" type="checkbox" <?=!empty($rScheduleCalendar)?'checked':''?>>&nbsp;
                          <label class="form-check-label" for="scheduleCalendar">SESUAI KALENDER <small>(Jadwal diatur sesuai tanggal dan jam tertentu)</small></label>
                        </div>
                      </th>
                    </tr>
                    <tr>
                      <th>Tanggal</th>
                      <th>Jam</th>
                      <th class="text-center">#</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach($rScheduleCalendar as $s) {
                      ?>
                      <tr>
                        <td><?=$s[COL_DATE_SCHEDULEDATE]?></td>
                        <td><?=$s[COL_DATE_SCHEDULETIME_FROM].' - '.$s[COL_DATE_SCHEDULETIME_TO]?></td>
                        <td class="text-center">
                          <button type="button" class="btn btn-xs btn-outline-danger btn-del"><i class="fas fa-minus"></i></button>
                          <input type="hidden" name="idx" value="<?=$s[COL_DATE_SCHEDULEDATE]?>" />
                        </td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th>
                        <input type="text" class="form-control form-control-sm calendar" name="Day" placeholder="yyyy-mm-dd" />
                      </th>
                      <th class="text-center" style="white-space: nowrap">
                        <input type="text" class="form-control d-inline-block timepicker" id="timepickerCalendarFrom" name="TimeFrom" placeholder="HH:MM" style="width: 40%; text-align: center" readonly />
                        <span class="p-1">-</span>
                        <input type="text" class="form-control d-inline-block" id="timepickerCalendarTo" name="TimeTo" placeholder="HH:MM" style="width: 40%; text-align: center" readonly />
                      </th>
                      <th class="text-center">
                        <button type="button" id="btn-add-schedule-calendar" class="btn btn-xs btn-default"><i class="fa fa-plus"></i></button>
                      </th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <p class="font-weight-light font-italic text-muted">
                Perubahan jadwal berlaku untuk pertemuan selanjutnya. Permintaan pertemuan yang sudah masuk sebelumnya akan tetap berlaku.
              </p>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group row">
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;TUTUP</button>
            <button type="submit" class="btn btn-primary"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
          </div>
        </div>
      </div>
      <?=form_close()?>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-caregroup-setting" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Sesi Caregroup</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <?=form_open(site_url('site/sesi/create-caregroup'),array('role'=>'form','id'=>'form-caregroup','class'=>'form-horizontal'))?>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            <input type="hidden" name="<?=COL_NM_SESSIONHOST?>" value="<?=$ruser[COL_USERNAME]?>" />
            <div class="form-group">
              <label>Jenis Konsultasi</label>
              <select name="<?=COL_KD_TYPE?>" class="form-control no-select2" required>
                <?=GetCombobox("select mtype.* from usertype ut inner join mtype on mtype.KD_Type = ut.KD_Type where ut.UserName = '$uname' and mtype.KD_Category = 'PERSONAL' order by mtype.NM_Type", COL_KD_TYPE, COL_NM_TYPE)?>
              </select>
            </div>
            <div class="form-group">
              <label>Tanggal / Jam</label>
              <div class="row">
                <div class="col-sm-5">
                  <input type="text" class="form-control datepicker" name="<?=COL_DATE_SESSIONDATE?>" placeholder="yyyy-mm-dd" required />
                </div>
                <div class="col-sm-3">
                  <input type="text" class="form-control datetimepicker-input mask-time d-inline-block" id="SessCaregroupTime" name="<?=COL_DATE_SESSIONTIME?>" data-toggle="datetimepicker" data-target="#SessCaregroupTime" placeholder="00:00" required />
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>Format</label>
                  <div class="form-check d-block mr-3">
                    <input id="RadioFormatReguler" class="form-check-input" type="radio" name="<?=COL_KD_SESSIONVIA?>" value="<?=SESSIONFORMAT_REGULER?>" checked />
                    <label class="form-check-label" for="RadioFormatReguler">REGULER (Tatap Muka)</label>
                  </div>
                  <div class="form-check d-block mr-3">
                    <input id="RadioFormatOnline" class="form-check-input" type="radio" name="<?=COL_KD_SESSIONVIA?>" value="<?=SESSIONFORMAT_ONLINE?>" />
                    <label class="form-check-label" for="RadioFormatOnline">ONLINE</label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <label>Jlh. Peserta</label>
                  <input type="text" class="form-control uang text-right" name="<?=COL_NUM_QUOTA?>" placeholder="0" required />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group row">
          <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;TUTUP</button>
            <button type="submit" class="btn btn-success"><i class="fad fa-save"></i>&nbsp;BUAT</button>
          </div>
        </div>
      </div>
      <?=form_close()?>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('.calendar').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minDate: moment().format('YYYY-MM-DD'),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('[name=TimeFrom]').timepicker({
    timeFormat: 'HH:mm',
    interval: 30,
    minTime: '7',
    maxTime: '21',
    defaultTime: '7',
    startTime: '07:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true,
    zindex: 9999999,
    change: function(time) {
      var thisval = $(this).val();
      var timeto = moment(thisval, "HH:mm").add(60, 'minutes').format('HH:mm');
      $('[name=TimeTo]', $(this).closest('tr')).val(timeto);
    }
  });

  var modalSchedule = $('#modal-schedule-setting');
  var modalCaregroup = $('#modal-caregroup-setting');
  var formSchedule = $('#form-schedule-setting');
  var formCaregroup = $('#form-caregroup');

  $('#btn-schedule-setting').click(function() {
    modalSchedule.modal('show');
  });
  $('#btn-caregroup-setting').click(function() {
    modalCaregroup.modal('show');
  });
  $('[name=ScheduleFixed]').change(function() {
    writeSchedule('tbl-schedule-fixed', 'ScheduleFixed');
  }).val(encodeURIComponent('<?=$data['ScheduleFixed']?>')).trigger('change');
  $('[name=ScheduleCalendar]').change(function() {
    writeSchedule('tbl-schedule-calendar', 'ScheduleCalendar');
  }).val(encodeURIComponent('<?=$data['ScheduleCalendar']?>')).trigger('change');

  $('#btn-add-schedule-fixed', $('#tbl-schedule-fixed')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleFixed]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day && a.TimeFrom == timeFr;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': $('[name=Day] option:selected', row).html(), 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleFixed]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('[name=TimeFrom]', row).val('07:00').trigger('change');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('#btn-add-schedule-calendar', $('#tbl-schedule-calendar')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleCalendar]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day && a.TimeFrom == timeFr;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': day, 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleCalendar]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('[name=TimeFrom]', row).val('07:00').trigger('change');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('[name^=ScheduleType]').change(function() {
    //var el = $('[name=ScheduleType]:checked');
    var tbl = $(this).closest('table');
    if($(this).is(':checked')) {
      $('tr:not(.row-checkbox)', tbl).removeClass('d-none');
      $('tr.row-checkbox', tbl).removeClass('row-checkbox').addClass('bg-secondary disabled row-checkbox');
    } else {
      $('tr:not(.row-checkbox)', tbl).addClass('d-none');
      $('tr.row-checkbox', tbl).removeClass('bg-secondary disabled');
    }
  }).trigger('change');

  $('form').each(function() {
    $(this).validate({
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
              /*if(res.data && res.data.redirect) {
                setTimeout(function(){
                  location.href = res.data.redirect;
                }, 1000);
              }*/
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
            modalSchedule.modal("hide");
          }
        });
        return false;
      }
    });
  });
});

function writeSchedule(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return (a['Day']+'--'+a['TimeFrom']).localeCompare(b['Day']+'--'+b['TimeFrom']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].DayText+'</td>';
        html += '<td>'+arr[i].TimeFrom+' - '+arr[i].TimeTo+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-xs btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Day+'--'+arr[i].TimeFrom+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val().split('--');
        var day = idx[0];
        var timefrom = idx[1];
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return (e.Day != day || e.TimeFrom != timefrom); });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
