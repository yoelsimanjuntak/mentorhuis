<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light"><?= $title ?></h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title">Detail Jadwal: <strong><?='#MH-'.str_pad($sesi[COL_KD_SESSION], 5, '0', STR_PAD_LEFT)?></strong></h5>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <td style="width: 10vw">Mentor</td><td style="width: 2vw">:</td><td><?=$sesi['Mentor_FirstName'].' '.$sesi['Mentor_LastName']?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Layanan</td><td style="width: 2vw">:</td><td><?=$sesi[COL_NM_TYPE]?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Tanggal / Jam</td><td style="width: 2vw">:</td><td><?=date('Y-m-d', strtotime($sesi[COL_DATE_SESSIONDATE])).' '.$sesi[COL_DATE_SESSIONTIME]?></td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Format</td><td style="width: 2vw">:</td><td><?=$sesi[COL_KD_SESSIONVIA].($sesi[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2"><i class="fas fa-users"></i></small>':'')?></td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title">Cara Pembayaran</h5>
            </div>
            <div class="card-body p-0">
              <table class="table table-striped">
                <tbody>
                  <tr>
                    <td style="width: 10vw">Rekening</td><td style="width: 2vw">:</td>
                    <td>
                      <?php
                      $rbill = $this->db
                      ->get(TBL__SETTINGBILL)
                      ->result_array();
                      if(count($rbill) > 0) {
                        ?>
                        <ol class="pl-2">
                          <?php
                          foreach ($rbill as $b) {
                            ?>
                            <li><?=$b[COL_KD_BANK].' <strong>'.$b[COL_NM_ACCOUNTNO].'</strong> an. <strong>'.$b[COL_NM_ACCOUNTNAME].'</strong>'?></li>
                            <?php
                          }
                          ?>
                        </ol>
                        <?php
                      } else {
                        echo '--';
                      }
                      ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Jumlah</td><td style="width: 2vw">:</td>
                    <td>
                      Rp. <strong><?=number_format($sesi[COL_NUM_RATE])?></strong>
                      <p class="font-italic text-danger"><small>Harap mentransfer jumlah yang sesuai tertera diatas dengan mencantumkan berita acara "<strong><?='#MH-'.str_pad($sesi[COL_KD_SESSION], 5, '0', STR_PAD_LEFT)?></strong>"</small></p>
                    </td>
                  </tr>
                  <tr>
                    <td style="width: 10vw">Bukti Transfer</td><td style="width: 2vw">:</td>
                    <td>
                      <?=form_open_multipart(site_url('site/ajax/session-upload-payment/'.$sesi[COL_KD_SESSION]),array('role'=>'form','id'=>'form-upload','class'=>'form-horizontal'))?>
                      <div class="input-group">
                        <div class="custom-file">
                          <input id="upload" type="file" class="custom-file-input" name="<?=COL_NM_PAYMENTIMAGE?>" accept="image/*" required  />
                          <label class="custom-file-label font-italic" for="upload">Upload bukti transfer</label>
                        </div>
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-success btn-sm">
                            <i class="fas fa-upload"></i>&nbsp;Upload</span>
                          </button>
                        </div>
                      </div>
                      <p class="text-sm font-italic text-danger mt-1">
                        Catatan: ukuran maks. 2MB dengan dimensi maks 2048 x 2048
                      </p>
                      <?=form_close()?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $('#form-upload').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
