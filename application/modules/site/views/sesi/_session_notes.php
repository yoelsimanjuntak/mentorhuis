<?=form_open(current_url(),array('role'=>'form','id'=>'form-program', 'class'=>'form-horizontal'))?>
<div class="form-group">
  <div class="col-sm-12">
    <textarea rows="8" placeholder="Catatan.." class="form-control" name="<?=COL_NM_REMARKSMENTOR?>"><?=!empty($data)?$data[COL_NM_REMARKSMENTOR]:''?></textarea>
  </div>
</div>
<div class="row" style="padding-top: 10px; border-top: 1px solid #f4f4f4">
  <div class="col-sm-12 text-center">
    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;CLOSE</button>&nbsp;
    <button type="submit" class="btn btn-success"><i class="fas fa-check"></i>&nbsp;SUBMIT</button>
  </div>
</div>
<?=form_close()?>
