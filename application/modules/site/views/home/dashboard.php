<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FIRSTNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$ruser[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="info font-weight-light">
            <?= 'Selamat datang, <strong>'.strtoupper($displayname).'</strong>!' ?>
          </h3>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Beranda</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <?php
          if(empty($ruser[COL_NM_FIRSTNAME]) || empty($ruser[COL_NM_LASTNAME]) || empty($ruser[COL_NO_PHONE]) || empty($ruser[COL_DATE_BIRTH])) {
            ?>
            <div class="callout callout-warning">
              <h5>Lengkapi Profil</h5>

              <p>Beberapa informasi di profil kamu masih belum lengkap. Yuk <a class="color-pallete-1" href="<?=site_url('site/home/profile')?>">lengkapi sekarang!</a></p>
            </div>
            <?php
          }
          ?>

          <div class="card" style="margin-bottom: 0 !important">
            <div class="card-header">
              <h1 class="card-title"><i class="fad fa-calendar"></i>&nbsp;&nbsp;Konseling Saya</h1>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0 table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th>No. Pesanan</th>
                    <th>Waktu</th>
                    <th>Status</th>
                    <th>Status Pembayaran</th>
                    <th>Mentor</th>
                    <th>Layanan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($session) > 0) {
                    foreach ($session as $ses) {
                      ?>
                      <tr>
                        <td style="white-space: nowrap"><?=anchor(site_url('site/sesi/detail/'.$ses[COL_KD_SESSION]), 'MH-'.str_pad($ses[COL_KD_SESSION], 5, '0', STR_PAD_LEFT))?></td>
                        <td style="white-space: nowrap"><?=date('Y-m-d', strtotime($ses[COL_DATE_SESSIONDATE])).'<br />'.$ses[COL_DATE_SESSIONTIME]?></td>
                        <td><?=$ses[COL_KD_STATUS]?></td>
                        <td style="white-space: nowrap"><?=$ses[COL_KD_STATUSPAYMENT] == SESSIONPAY_WAITING && !empty($ses[COL_NM_PAYMENTIMAGE]) ? 'PROSES VERIFIKASI' : $ses[COL_KD_STATUSPAYMENT]?></td>
                        <td style="white-space: nowrap"><?=$ses['Mentor_FirstName'].' '.$ses['Mentor_LastName']?></td>
                        <td><?=$ses[COL_NM_TYPE]?></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="6">
                        <p class="font-italic text-center">
                          Kamu belum memiliki jadwal pertemuan, ayo <a href="#section-services">mulai konseling</a>!
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div id="section-services" class="row mb-3" style="padding-top: 40px">
        <div class="col-sm-12 text-center">
          <h4 class="font-weight-light">Layanan <?=$ruser[COL_ROLENAME]?></h4>
        </div>
      </div>

      <?=$this->load->view('home/_services')?>
      <?php
      if($ruser[COL_ROLEID]==ROLEMEMBER_REG) {
        ?>
        <div class="row mb-2">
          <div class="col-sm-12">
            <h4 class="font-weight-light">Bingung memilih layanan? Yuk, ikuti kuisioner berikut!</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card">
              <div class="card-body">
                <p class="font-italic">
                  Silakan berikan penilaian atas dimensi-dimensi kehidupan kamu dengan memilih jawaban atas pernyataan-pernyataan dibawah, pastikan jawabanmu sesuai dengan kondisi nyata diri kamu yaaa..
                </p>
              </div>
              <div class="card-footer text-center">
                <a href="<?=site_url('site/home/questionaire')?>" class="btn btn-outline-info"><i class="fad fa-flag-checkered"></i>&nbsp;&nbsp;Mulai Kuisioner!</a>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
