<style>
a.btn-pallete-1.collapsed {
  color: #4D089A !important;
	border-color: #4D089A !important;
  background-color: #fff !important;
}
a.btn-pallete-1.collapsed:hover {
  color: #fff !important;
  background-color: #4D089A !important;
  border-color: #4D089A !important;
}
a.btn-pallete-3.collapsed {
  color: #D0A331;
	border-color: #D0A331;
  background-color: #fff !important;
}
a.btn-pallete-3.collapsed:hover {
    color: #fff !important;
    background-color: #D0A331 !important;
    border-color: #D0A331 !important;
}
</style>
<?php
$aspect = $this->db
->distinct()
->select(COL_NM_ASPECT)
->get(TBL_MQUESTION)
->result_array();
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="info font-weight-light">
            <?=$title?>
          </h3>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title">Petunjuk :</h5><br  />
              <p class="font-weight-light font-italic">
                Silakan pilih terlebih dahulu dimensi apa yang ingin kamu ketahui dari dirimu. Lalu berikan penilaian atas dimensi-dimensi kehidupan kamu dengan memilih jawaban atas pernyataan-pernyataan dibawah. Pastikan jawabanmu sesuai dengan kondisi nyata diri kamu ya.
              </p>
            </div>
            <div class="card-body">
              <?php
              $i=0;
              foreach ($aspect as $a) {
                ?>
                <a data-toggle="collapse" href="#dimensi<?=$a[COL_NM_ASPECT]?>" class="btn btn-pallete-<?=$i%2==0?'1':'3'?> <?=$i!=0?'collapsed':''?>"><?=$a[COL_NM_ASPECT]?></a>
                <?php
                $i++;
              }
              ?>
              <?=form_open('#',array('role'=>'form','id'=>'form-question','class'=>'form-horizontal'))?>
              <div class="row">
                <div class="col-sm-12 mt-3">
                  <?php
                  $n=0;
                  foreach ($aspect as $a) {
                    ?>
                    <div id="dimensi<?=$a[COL_NM_ASPECT]?>" class="panel-collapse in collapse <?=$n==0?'show':''?>">
                      <?php
                      $rquestion = $this->db->where(COL_NM_ASPECT, $a[COL_NM_ASPECT])->get(TBL_MQUESTION)->result_array();
                      $no=1;
                      foreach ($rquestion as $q) {
                        ?>
                        <div class="form-group row" style="border-bottom: 1px solid #dedede">
                          <label class="control-label col-sm-8"><?=$q[COL_NM_QUESTION]?></label>
                          <div class="col-sm-4">
                            <div class="icheck-info icheck-pallete-<?=$n%2==0?'1':'3'?> d-inline mr-2">
                              <input type="radio" id="radioQ<?=$q[COL_KD_QUESTION]?>_1" name="radioQ<?=$q[COL_KD_QUESTION]?>" value="1" checked  />
                              <label class="font-weight-light" for="radioQ<?=$q[COL_KD_QUESTION]?>_1">
                                Kurang
                              </label>
                            </div>
                            <div class="icheck-info icheck-pallete-<?=$n%2==0?'1':'3'?> d-inline">
                              <input type="radio" id="radioQ<?=$q[COL_KD_QUESTION]?>_2" name="radioQ<?=$q[COL_KD_QUESTION]?>" value="2"  />
                              <label class="font-weight-light" for="radioQ<?=$q[COL_KD_QUESTION]?>_2">
                                Jarang
                              </label>
                            </div>
                            <div class="icheck-info icheck-pallete-<?=$n%2==0?'1':'3'?> d-inline">
                              <input type="radio" id="radioQ<?=$q[COL_KD_QUESTION]?>_3" name="radioQ<?=$q[COL_KD_QUESTION]?>" value="3"  />
                              <label class="font-weight-light" for="radioQ<?=$q[COL_KD_QUESTION]?>_3">
                                Sering
                              </label>
                            </div>
                            <div class="icheck-info icheck-pallete-<?=$n%2==0?'1':'3'?> d-inline">
                              <input type="radio" id="radioQ<?=$q[COL_KD_QUESTION]?>_4" name="radioQ<?=$q[COL_KD_QUESTION]?>" value="4"  />
                              <label class="font-weight-light" for="radioQ<?=$q[COL_KD_QUESTION]?>_4">
                                Selalu
                              </label>
                            </div>
                          </div>
                        </div>
                        <?php
                        $no++;
                      }
                      ?>
                    </div>
                    <?php
                    $n++;
                  }
                  ?>
                  <div class="form-group row mt-2">
                    <div class="col-sm-12 text-center">
                      <button type="submit" class="btn btn-outline-pallete-1"><i class="fad fa-pencil-alt"></i>&nbsp;SUBMIT</button>
                    </div>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-result" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Hasil</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <h3 id="result" class="text-center font-weight-bold color-pallete-1"></h3>
        <p id="desc" class="text-center color-pallete-1"></p>
        <p class="text-sm text-center color-pallete-3">
          Anda dapat mengkonsultasikan dengan kami untuk pengembangan. Daftar sekarang dan dapatkan layanan dari Mentor terbaik kami.
        </p>
        <?php
        if(!IsLogin()) {
          ?>
          <p class="text-center">
            <a href="<?=site_url('site/user/register')?>" class="btn btn-outline-pallete-3"><i class="fad fa-user-plus"></i>&nbsp;DAFTAR</a>
          </p>
          <?php
        } else {
          ?>
          <p class="text-center">
            <a href="<?=site_url('site/home/services')?>" class="btn btn-outline-pallete-3"><i class="fad fa-arrow-right"></i>&nbsp;MULAI KONSULTASI</a>
          </p>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('[data-toggle="collapse"]').click(function() {
    var dis = $(this);
    if(!$(dis.attr('href')).hasClass('show')) {
      $('.panel-collapse.show').removeClass('show');
    }
    $('[data-toggle="collapse"]').addClass('collapsed');
  });

  $('#form-question').validate({
    submitHandler: function() {
      var activeEl = $('.panel-collapse.show');
      if(activeEl.length == 0) {
        alert('Silakan pilih dimensi yang ingin kamu ketahui dari dirimu');
        return;
      }

      var grade = 0;
      var gradetext = '';
      var gradedesc = '';
      var checkbox = $('input[type=radio]:checked', activeEl);
      for(var i=0; i<checkbox.length; i++) {
        grade += parseInt($(checkbox[i]).val());
      }

      if(grade>=16) {
        gradetext = 'SEHAT';
        gradedesc = 'Pertahankan kondisi saat ini dan nikmati hidup';
      }
      else if(grade>=11 && grade<=15) {
        gradetext = 'CUKUP SEHAT';
        gradedesc = 'Kembangkan kesehatan pada dimensi yang kurang';
      }
      else if(grade>=6 && grade<=10) {
        gradetext = 'KURANG SEHAT';
        gradedesc = 'Konsultasikan dengan Mentor untuk pengembangan';
      }
      else {
        gradetext = 'TIDAK SEHAT';
        gradedesc = 'Konsultasikan dengan Mentor untuk penanganan lebih';
      }

      $('#result', $('#modal-result')).text(gradetext);
      $('#desc', $('#modal-result')).text(gradedesc);
      $('#modal-result').modal('show');
      return false;
    }
  });
});
</script>
