<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FIRSTNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$ruser[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h3 class="info font-weight-light">
            <?= 'Selamat datang, <strong>'.strtoupper($displayname).'</strong>!' ?>
          </h3>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Beranda</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>

  <div class="content">
    <div class="container">
      <div class="row">
        <!--<div class="col-sm-6">
          <div class="card">
            <div class="card-header border-0">
              <h5 class="card-title font-weight-light"><i class="fad fa-chart-bar"></i>&nbsp;&nbsp;Grafik Pertemuan</h5>
            </div>
            <div class="card-body">
              <div class="d-flex">
                  <p class="d-flex flex-column">
                    <span class="text-bold text-lg"><?=number_format(rand(1,1000))?></span>
                    <small>Pertemuan</small>
                  </p>
                </div>
                <div class="position-relative mb-2">
                  <canvas id="visitors-chart" height="200"></canvas>
                </div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="row">
        <div class="col-sm-8">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title font-weight-light"><i class="fad fa-calendar"></i>&nbsp;&nbsp;Jadwal Mendatang</h5>
            </div>
            <div class="card-body p-0 table-responsive">
              <table class="table table-striped" width="100%">
                <thead>
                  <tr>
                    <th>No. Pesanan</th>
                    <th>Tanggal / Jam</th>
                    <th>Status</th>
                    <th>Klien</th>
                    <th>Layanan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  if(count($session) > 0) {
                    foreach ($session as $ses) {
                      $clients = '';
                      if($ses[COL_IS_CAREGROUP]) {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->result_array();
                        if(!empty($rentry)) {
                          $clients = number_format(count($rentry)).' orang';
                        } else {
                          $clients = '-';
                        }

                      } else {
                        $rentry = $this->db
                        ->select(TBL__USERINFORMATION.'.*')
                        ->where(COL_KD_SESSION, $ses[COL_KD_SESSION])
                        ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_ENTRY.".".COL_USERNAME,"inner")
                        ->get(TBL_TSESSION_ENTRY)
                        ->row_array();
                        if($rentry) {
                          $clients = $rentry[COL_NM_FIRSTNAME].' '.$rentry[COL_NM_LASTNAME];
                        }
                      }
                      ?>
                      <tr>
                        <td><?=anchor(site_url('site/sesi/detail/'.$ses[COL_KD_SESSION]), 'MH-'.str_pad($ses[COL_KD_SESSION], 5, '0', STR_PAD_LEFT))?></td>
                        <td><?=date('Y-m-d', strtotime($ses[COL_DATE_SESSIONDATE])).' '.$ses[COL_DATE_SESSIONTIME]?></td>
                        <td><?=$ses[COL_KD_STATUS]?></td>
                        <td><?=$clients?></td>
                        <td><?=$ses[COL_NM_TYPE].($ses[COL_IS_CAREGROUP]?'<small class="badge badge-primary ml-2">CARE GROUP</small>':'')?></td>
                      </tr>
                      <?php
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="6">
                        <p class="font-italic">
                          Data masih kosong
                        </p>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
            <div class="card-footer text-right">
              <a href="<?=site_url('site/sesi/index')?>" class="btn btn-outline-info"><i class="fa fa-list"></i>&nbsp;LIHAT SEMUA</a>
            </div>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title font-weight-light"><i class="fad fa-bell"></i>&nbsp;&nbsp;Notifikasi</h5>
            </div>
            <div class="card-body">
              <p class="font-italic">
                Belum ada notifikasi.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var ticksStyle = {
    fontColor: '#495057',
    fontStyle: 'bold'
  }

  var mode      = 'index'
  var intersect = true

  var $visitorsChart = $('#visitors-chart')
  var visitorsChart  = new Chart($visitorsChart, {
    data   : {
      labels  : ['Jan', 'Feb', 'Mar', 'Apr'],
      datasets: [{
        type                : 'line',
        data                : [100, 120, 170, 167],
        backgroundColor     : 'transparent',
        borderColor         : '#007bff',
        pointBorderColor    : '#007bff',
        pointBackgroundColor: '#007bff',
        fill                : false
        // pointHoverBackgroundColor: '#007bff',
        // pointHoverBorderColor    : '#007bff'
      },
        {
          type                : 'line',
          data                : [60, 80, 70, 67],
          backgroundColor     : 'tansparent',
          borderColor         : '#ced4da',
          pointBorderColor    : '#ced4da',
          pointBackgroundColor: '#ced4da',
          fill                : false
          // pointHoverBackgroundColor: '#ced4da',
          // pointHoverBorderColor    : '#ced4da'
        }]
    },
    options: {
      maintainAspectRatio: false,
      tooltips           : {
        mode     : mode,
        intersect: intersect
      },
      hover              : {
        mode     : mode,
        intersect: intersect
      },
      legend             : {
        display: false
      },
      scales             : {
        yAxes: [{
          // display: false,
          gridLines: {
            display      : true,
            lineWidth    : '4px',
            color        : 'rgba(0, 0, 0, .2)',
            zeroLineColor: 'transparent'
          },
          ticks    : $.extend({
            beginAtZero : true,
            suggestedMax: 200
          }, ticksStyle)
        }],
        xAxes: [{
          display  : true,
          gridLines: {
            display: false
          },
          ticks    : ticksStyle
        }]
      }
    }
  })
});
</script>
