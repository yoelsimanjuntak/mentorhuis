<div class="hero-container">
  <span class="hero-wave"></span>
  <div class="hero-content">
    <div class="row">
      <div class="col-md-12">
        <h1 class="font-weight-bold mt-0 hero-item">Selamat datang di <?=$this->setting_web_name?></h1>
        <div class="hero-item text-left">
          <p>
            Tempat berbagi cerita dengan Mentor / Psikolog Professional untuk pengembangan diri atau organisasi anda.
          </p>
        </div>
        <div class="hero-buttons-container mt-2 hero-item">
          <a href="<?=site_url('site/user/register/member')?>" class="btn btn-lg btn-pallete-3">
            <i class="far fa-user-plus"></i>&nbsp;Daftar Gratis!
          </a>
          <a href="<?=site_url('site/user/register/mentor')?>" class="btn btn-lg btn-pallete-2 ml-2">
            <i class="far fa-user-md"></i>&nbsp;Bergabung sebagai Mentor
          </a>
        </div>
      </div>
      <div class="col-md-12 special-link" style="margin-top: 40px">
        <h3 class="font-weight-bold mt-0 hero-item text-left">Bingung untuk memulai?</h3>
        <div class="hero-item text-left">
          <p style="line-height: 1.5rem">
            Jangan kuatir, kami akan bantu untuk menemukan layanan psikologi yang tepat buat kamu.
          </p>
          <p>
            <a href="<?=site_url('site/home/questionaire')?>">
              <span>IKUTI KUISIONER</span>
            </a>
          </p>
        </div>

      </div>
    </div>
  </div>
</div>
<div class="simple-cta pullup color-pallete-1">MentorHuis & Pojok Psikologi</div>
<div class="content-wrapper">
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="card card-widget widget-user">
              <div class="widget-user-header text-white bg-cover-pallete" style="z-index: 1; background: url('<?=MY_IMAGEURL.'_mentorhuis-1.png'?>') no-repeat center center / cover;">
                <h1 class="font-weight-bold text-center" style="line-height: 2">MentorHuis</h2>
              </div>
              <div class="card-footer bg-white pt-2" style="z-index: 2">
                <p class="text-justify">
                  MentorHuis adalah platform  untuk mentoring secara individu, kelompok maupun organisasi perusahaan. Temukan ragam jenis layanan konseling online atau mentoring dengan psikolog professional atau profesional inspiratif lainnya.
                </p>
              </div>
            </div>
        </div>
        <div class="col-sm-6">
          <div class="card card-widget widget-user">
              <div class="widget-user-header text-white bg-cover-pallete" style="z-index: 1; background: url('<?=MY_IMAGEURL.'_mentorhuis-2.png'?>') no-repeat center center / cover;">
                <h1 class="font-weight-bold text-center" style="line-height: 2">Pojok Psikologi</h2>
              </div>
              <div class="card-footer bg-white pt-2" style="z-index: 2">
                <p class="text-justify">
                  Pojok Psikologi adalah firma konsultasi psikologi untuk pengembangan individu, kelompok maupun organisasi/perusahaan. Kami percaya setiap individu,kelompok maupun perusahaan tercipta berbeda-beda dan memiliki keinginan untuk bertumbuh dengan cara yang berbeda. Oleh karena itu, dengan bekerja sama dengan Mentorhuis kami ingin menjadi partner dan mempertemukan individu, kelompok maupun perusahaan dengan mentor yang tepat untuk menemukan kembali jatidiri, visi dan bertumbuh lebih baik di masa depan.
                </p>
              </div>
            </div>
        </div>
      </div>
      <div class="sdlc-container position-relative pb-2">
        <div class="sdlc-description" style="margin-bottom: 30px">
          <h2 class="color-pallete-1" style="font-weight: 600">Cara mudah mendapatkan layanan psikologi yang tepat buat kamu.</h2>
          <p></p>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-6 offset-sm-3">
            <div class="info-box">
              <span class="info-box-icon bg-pallete-1 text-white elevation-1"><i class="fad fa-user-plus"></i></span>
              <div class="info-box-content text-left">
                <span class="info-box-text">DAFTAR</span>
                <span class="info-box-number">
                  Daftar mudah dengan email, Google atau Facebook.
                </span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 offset-sm-3">
            <div class="info-box">
              <span class="info-box-icon bg-pallete-3 text-white elevation-1"><i class="fad fa-list-alt"></i></span>
              <div class="info-box-content text-left">
                <span class="info-box-text">PILIH LAYANAN</span>
                <span class="info-box-number">
                  Tersedia berbagai layanan pribadi maupun untuk berkelompok sesuai kebutuhan kamu.
                </span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 offset-sm-3">
            <div class="info-box">
              <span class="info-box-icon bg-pallete-1 text-white elevation-1"><i class="fad fa-user-md"></i></span>
              <div class="info-box-content text-left">
                <span class="info-box-text">PILIH PSIKOLOG</span>
                <span class="info-box-number">
                  Kami menghadirkan Psikolog yang berkompeten dan berpengalaman dibidangnya.
                </span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 offset-sm-3">
            <div class="info-box">
              <span class="info-box-icon bg-pallete-3 text-white elevation-1"><i class="fad fa-comments-alt"></i></span>
              <div class="info-box-content text-left">
                <span class="info-box-text">IKUSI SESI</span>
                <span class="info-box-number">
                  Ikuti setiap sesi yang kamu pilih dengan Psikolog handal dari <span class="color-pallete-3">MentorHuis</span>
                </span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 offset-sm-3">
            <div class="info-box">
              <span class="info-box-icon bg-pallete-1 text-white elevation-1"><i class="fad fa-head-side-brain"></i></span>
              <div class="info-box-content text-left">
                <span class="info-box-text">PRIBADI LEBIH BAIK</span>
                <span class="info-box-number">
                  Pendampingan yang tepat untuk menjadi pribadi yang lebih baik.
                </span>
              </div>
            </div>
          </div>

          <!--<div class="sdlc-table">
            <div class="sdlc-column">
              <div class="stage-container">
                <h3 class="color-pallete-1"><i class="fad fa-user-plus"></i></h3>
                <p class="color-pallete-1">
                  DAFTAR
                </p>
              </div>
              <div class="solutions-container">
                <p>
                  Daftar mudah dengan email, Google atau Facebook.
                </p>
              </div>
            </div>
            <div class="sdlc-column">
              <div class="stage-container">
                <h3 class="color-pallete-1"><i class="fad fa-list-alt"></i></h3>
                <p class="color-pallete-1">
                  PILIH LAYANAN
                </p>
              </div>
              <div class="solutions-container">
                <p>
                  Tersedia berbagai layanan pribadi maupun untuk berkelompok sesuai kebutuhan kamu.
                </p>
              </div>
            </div>
            <div class="sdlc-column">
              <div class="stage-container">
                <h3 class="color-pallete-1"><i class="fad fa-user-md"></i></h3>
                <p class="color-pallete-1">
                  PILIH PSIKOLOG
                </p>
              </div>
              <div class="solutions-container">
                <p>
                  Kami menghadirkan Psikolog yang berkompeten dan berpengalaman dibidangnya.
                </p>
              </div>
            </div>
            <div class="sdlc-column">
              <div class="stage-container">
                <h3 class="color-pallete-1"><i class="fad fa-comments-alt"></i></h3>
                <p class="color-pallete-1">
                  IKUSI SESI
                </p>
              </div>
              <div class="solutions-container">
                <p>
                  Ikuti setiap sesi yang kamu pilih dengan Psikolog handal dari <span class="color-pallete-1">MentorHuis</span>
                </p>
              </div>
            </div>
            <div class="sdlc-column">
              <div class="stage-container">
                <h3 class="color-pallete-1"><i class="fad fa-head-side-brain"></i></h3>
                <p class="color-pallete-1">
                  PRIBADI LEBIH BAIK
                </p>
              </div>
              <div class="solutions-container">
                <p>
                  Pendampingan yang tepat untuk menjadi pribadi yang lebih baik
                </p>
              </div>
            </div>
          </div>-->
        </div>
      </div>

      <?php
      $rpartner = $this->db->order_by(COL_NM_PARTNER)->get(TBL_MPARTNER)->result_array();
      if(!empty($rpartner)) {
        $page = count($rpartner) / 5;
        ?>
        <div class="sdlc-container position-relative pb-0">
          <div class="sdlc-description mb-0">
            <h2 class="color-pallete-1" style="font-weight: 600">Klien</h2>
            <p></p>
          </div>
        </div>
        <div class="customer-logos-container clear-background pt-0">
          <p>
            Kami telah memberikan layanan psikologi yang menyeluruh dan terintegrasi pada banyak komunitas maupun perusahaan, diantaranya :
          </p>
          <div id="carouselPartner" class="carousel slide mt-3 p-3" data-ride="carousel">
            <ol class="carousel-indicators">
              <?php
              for($i=0;$i<$page;$i++) {
                ?>
                <li data-target="#carouselPartner" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                <?php
              }
              ?>
            </ol>
            <div class="carousel-inner">
              <?php
              $n=0;
              for($i=0;$i<$page;$i++) {
                ?>
                <div class="carousel-item <?=$i==0?'active':''?>" style="text-align: center">
                  <?php
                  for($x=0; $x<5; $x++) {
                    if(empty($rpartner[$n])) break;
                    ?>
                    <img src="<?=MY_UPLOADURL.$rpartner[$n][COL_NM_LOGOIMAGE]?>" class="m-3" style="width: 20vh" alt="<?=$rpartner[$n][COL_NM_PARTNER]?>">
                    <?php
                    $n++;
                  }
                  ?>
                </div>
                <?php
              }
              ?>
            </div>
            <?php
            if($page > 1) {
              ?>
              <a class="carousel-control-prev" href="#carouselPartner" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselPartner" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <?php
            }
            ?>
          </div>
        </div>
        <?php
      }
      ?>

      <?php
      $rtesti = $this->db
      ->where(COL_IS_DISPLAYHOMEPAGE, 1)
      ->order_by(COL_CREATEDON, 'desc')
      ->get(TBL_TSESSION_FEEDBACK)
      ->result_array();
      if(!empty($rtesti)) {
        $page = count($rtesti) / 3;
        ?>
        <div class="sdlc-container position-relative pb-0">
          <div class="sdlc-description mb-2">
            <h2 class="color-pallete-1" style="font-weight: 600">Testimoni</h2>
          </div>
        </div>
        <div class="row text-left mb-3">
          <div class="col-sm-12">
            <div id="carouselTestimoni" class="carousel slide mt-3 p-3" data-ride="carousel">
              <ol class="carousel-indicators">
                <?php
                for($i=0;$i<$page;$i++) {
                  ?>
                  <li data-target="#carouselTestimoni" data-slide-to="<?=$i?>" <?=$i==0?'class="active"':''?>></li>
                  <?php
                }
                ?>
              </ol>
              <div class="carousel-inner">
                <?php
                $n=0;
                for($i=0;$i<$page;$i++) {
                  ?>
                  <div class="carousel-item <?=$i==0?'active':''?>">
                    <div class="row">
                      <?php
                      for($x=0; $x<3; $x++) {
                        if(empty($rtesti[$n])) break;
                        ?>
                        <div class="col-sm-4">
                          <div class="card card-widget" style="width: 100%">
                            <div class="card-header">
                              <div class="user-block">
                                <img class="img-circle" src="<?=MY_IMAGEURL.'user.jpg'?>" alt="Anonim">
                                <span class="username"><?=$rtesti[$n][COL_NM_FEEDBACKNAME]?></span>
                                <span class="description"><?=(!empty($rtesti[$n][COL_NM_FEEDBACKOCUPATION])?$rtesti[$n][COL_NM_FEEDBACKOCUPATION]:'-').(!empty($rtesti[$n][COL_NM_FEEDBACKAGE])?' '.$rtesti[$n][COL_NM_FEEDBACKAGE].' tahun':'')?></span>
                              </div>
                            </div>
                            <div class="card-body">
                              <p><?=$rtesti[$n][COL_NM_FEEDBACKTEXT]?></p>
                            </div>
                          </div>
                        </div>
                        <?php
                        $n++;
                      }
                      ?>
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
              <?php
              if($page > 1) {
                ?>
                <a class="carousel-control-prev" href="#carouselTestimoni" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselTestimoni" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
                <?php
              }
              ?>
            </div>
          </div>

        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<script type="text/javascript">
(function ($) {
  var navbar = $('.navbar');
  var lastScrollTop = 0;

  $(window).scroll(function () {
      var st = $(this).scrollTop();
      // Scroll down
      if (st > lastScrollTop) {
          //navbar.fadeOut();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Scroll up but still lower than 200 (change that to whatever suits your need)
      else if(st < lastScrollTop && st > 200) {
          //navbar.fadeIn();
          navbar.removeClass('navbar-dark bg-transparent').addClass('navbar-light bg-white');
      }
      // Reached top
      else {
          navbar.removeClass('navbar-light bg-white').addClass('navbar-dark bg-transparent');
      }
      lastScrollTop = st;
  }).trigger('scroll');
})(jQuery);
$(document).ready(function() {
  $("#form-register").validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {

        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
