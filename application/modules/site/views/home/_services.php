<?php
$ruser = GetLoggedUser();
$servicecat = array(SERVICECAT_PERSONAL,SERVICECAT_COMMUNITY,SERVICECAT_CORPORATE);
if($ruser[COL_ROLEID]==ROLEMEMBER_REG) $servicecat = SERVICECAT_PERSONAL;
else if($ruser[COL_ROLEID]==ROLEMEMBER_GROUP) $servicecat = SERVICECAT_COMMUNITY;
else if($ruser[COL_ROLEID]==ROLEMEMBER_COY) $servicecat = SERVICECAT_CORPORATE;
?>
<div class="row <?=$ruser[COL_ROLEID]==ROLEMEMBER_REG?'mb-3':''?> d-flex align-items-stretch">
  <?php
  $rservice = $this->db
  ->where_in(COL_KD_CATEGORY, $servicecat)
  ->get(TBL_MTYPE)
  ->result_array();
  foreach($rservice as $s) {
    ?>
    <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
      <div class="card" style="border-top: 3px solid <?=$s[COL_NM_COLORMAIN]?>">
        <div class="card-header">
          <h3 class="card-title"><?=$s[COL_NM_TYPE]?></h3>
        </div>
        <div class="card-body p-0">
          <div style="width: 100%; height: 25vh; background-image: url(<?=MY_IMAGEURL.$s[COL_NM_THUMBNAIL]?>); background-position: center; background-repeat: no-repeat; background-size: cover">
          </div>
          <p class="p-3 mb-0">
            <?=$s[COL_NM_DESC]?>
          </p>
        </div>
        <div class="card-footer text-right">
          <a href="<?=site_url('site/sesi/create/'.$s[COL_KD_TYPE])?>" class="btn btn-sm btn-white btn-hover" title="Cari mentor" data-color="<?=$s[COL_NM_COLORMAIN]?>" style="background-color: #fff; color: <?=$s[COL_NM_COLORMAIN]?>; border-color: <?=$s[COL_NM_COLORMAIN]?>">
            Mulai&nbsp;&nbsp;<i class="far fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>
    <?php
  }
  ?>
</div>
<script>
$(document).ready(function() {
  $('.btn-hover').hover(function() {
    var color = $(this).data('color');
    $(this).css("color", '#fff');
    $(this).css("background-color", color);
  }, function() {
    var color = $(this).data('color');
    $(this).css("color", color);
    $(this).css("background-color", 'transparent');
    $(this).css("border-color", color);
  });
});
</script>
