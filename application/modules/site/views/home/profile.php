<?php
$ruser = GetLoggedUser();
$displayname = $ruser ? $ruser[COL_NM_FIRSTNAME] : "Guest";
$displaypicture = MY_IMAGEURL.'user.jpg';
if($ruser) {
    $displaypicture = $ruser[COL_NM_PROFILEIMAGE] ? MY_UPLOADURL.$ruser[COL_NM_PROFILEIMAGE] : MY_IMAGEURL.'user.jpg';
}
$rstatus = $this->db
->where(COL_USERNAME, $ruser[COL_USERNAME])
->order_by(COL_CREATEDON, 'desc')
->get(TBL_USERSTATUS)
->row_array();
$rrate = $this->db
->where(COL_NM_GRADE, $data[COL_NM_GRADE])
->order_by(COL_NUM_EXPFROM, 'desc')
->get(TBL__SETTINGRATE)
->row_array();
?>
<div class="content-wrapper">
  <div class="content-header">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="user-panel d-flex">
            <h1 class="m-0 font-weight-light"><?= $title ?></h1>
          </div>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-home"></i> Home</a></li>
              <li class="breadcrumb-item active"><?=$title?></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <div class="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-<?=$ruser[COL_ROLEID]!=ROLEPSIKOLOG?'6':'12'?>">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Data Diri</h5>
            </div>
            <?=form_open_multipart(site_url('site/ajax/profile-edit'),array('role'=>'form','id'=>'form-profile','class'=>'form-horizontal'))?>
            <div class="card-body box-profile">
              <div class="row">
                <div class="col-sm-<?=$ruser[COL_ROLEID]!=ROLEPSIKOLOG?'12':'6 pr-3'?>">
                  <div class="text-center mb-2">
                    <img class="profile-user-img img-fluid" src="<?=$displaypicture?>" alt="Profile" style="max-height: 800px; border-radius: 10%">
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12 text-center">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fad fa-image"></i></span>
                        </div>
                        <div class="custom-file">
                          <input id="avatarFile" type="file" class="custom-file-input" name="avatar" accept="image/*">
                          <label class="custom-file-label text-left" for="avatarFile">Upload</label>
                        </div>
                      </div>
                      <p class="text-sm font-italic text-danger mt-1">
                        Catatan: ukuran maks. 500MB dengan dimensi maks 2048 x 2048
                      </p>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label>Email</label>
                        <input type="text" class="form-control" name="<?=COL_EMAIL?>" placeholder="Email" value="<?=$data[COL_EMAIL]?>" readonly />
                    </div>
                    <div class="col-sm-6">
                      <label>Nickname</label>
                        <input type="text" class="form-control" name="<?=COL_NM_NICKNAME?>" placeholder="Nickname" value="<?=$data[COL_NM_NICKNAME]?>" required />
                    </div>
                  </div>
                  <div class="form-group">
                      <label>Nama</label>
                      <div class="row">
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="<?=COL_NM_FIRSTNAME?>" placeholder="Nama Depan" value="<?=$data[COL_NM_FIRSTNAME]?>" required />
                        </div>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="<?=COL_NM_LASTNAME?>" placeholder="Nama Belakang" value="<?=$data[COL_NM_LASTNAME]?>" />
                        </div>
                      </div>
                  </div>
                  <!--<div class="form-group">
                      <label>Jenis Kelamin</label>
                      <div class="row">
                        <div class="col-sm-2">
                          <div class="form-check">
                            <input id="RadioGenderLK" class="form-check-input" type="radio" name="<?=COL_NM_GENDER?>" value="Pria" <?=empty($data[COL_NM_GENDER])?"checked":($data[COL_NM_GENDER]=="Pria"?"checked":"")?>>
                            <label class="form-check-label" for="RadioGenderLK">Pria</label>
                          </div>
                        </div>
                        <div class="col-sm-2">
                          <div class="form-check">
                            <input id="RadioGenderPR" class="form-check-input" type="radio" name="<?=COL_NM_GENDER?>" value="Wanita" <?=$data[COL_NM_GENDER]=="Wanita"?"checked":""?>>
                            <label class="form-check-label" for="RadioGenderPR">Wanita</label>
                          </div>
                        </div>
                      </div>
                  </div>-->
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label>Tanggal Lahir</label>
                      <input type="text" class="form-control birthdaypicker" data-min="1960" name="<?=COL_DATE_BIRTH?>" placeholder="yyyy-mm-dd" value="<?=$data[COL_DATE_BIRTH]?>" required />
                    </div>
                    <div class="col-sm-6">
                      <label>No. Telp / HP</label>
                      <input type="text" class="form-control" name="<?=COL_NO_PHONE?>" placeholder="No. Telp / HP" value="<?=$data[COL_NO_PHONE]?>" required />
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Alamat</label>
                    <div class="row">
                      <div class="col-sm-12">
                        <textarea class="form-control" name="<?=COL_NM_ADDRESS?>" placeholder="Alamat" required><?=$data[COL_NM_ADDRESS]?></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label>Provinsi</label>
                      <select name="<?=COL_NM_PROVINCE?>" class="form-control no-select2" style="width: 100%" required>
                      </select>
                    </div>
                    <div class="col-sm-6">
                      <label>Kabupaten / Kota</label>
                      <select name="<?=COL_NM_CITY?>" class="form-control no-select2" style="width: 100%" required>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8">
                      <label>Pekerjaan</label>
                      <input type="text" class="form-control" name="<?=COL_NM_OCCUPATION?>" placeholder="Pekerjaan" value="<?=$data[COL_NM_OCCUPATION]?>" required />
                    </div>
                    <?php
                    if($ruser[COL_ROLEID] == ROLEPSIKOLOG) {
                      ?>
                      <div class="col-sm-4">
                        <label>Pengalaman (Tahun)</label>
                        <input type="text" class="form-control uang text-right" name="<?=COL_NUM_EXPERIENCE?>" placeholder="N Tahun" value="<?=$data[COL_NUM_EXPERIENCE]?>" required />
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-8">
                      <label>Status Pernikahan</label>
                      <select name="<?=COL_NM_MARITALSTATUS?>" class="form-control" style="width: 100%" required>
                        <option value="BELUM MENIKAH" <?=$data[COL_NM_MARITALSTATUS]=='BELUM MENIKAH'?'selected':''?>>BELUM MENIKAH</option>
                        <option value="MENIKAH" <?=$data[COL_NM_MARITALSTATUS]=='MENIKAH'?'selected':''?>>MENIKAH</option>
                      </select>
                    </div>
                  </div>
                  <?php
                  if($ruser[COL_ROLEID] == ROLEPSIKOLOG) {
                    ?>
                    <div class="form-group">
                      <label>Riwayat Pendidikan</label>
                      <input type="hidden" name="<?=COL_NM_EDUCATIONALBACKGROUND?>" />
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="mt-2 mb-3">
                            <table id="tbl-education" class="table table-bordered">
                              <thead>
                                <tr>
                                  <th>Jenjang</th>
                                  <th>Tahun Lulus</th>
                                  <th>Nama Institusi</th>
                                  <th class="text-center">#</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php
                                if(!empty($arrEducation)) {
                                  foreach($arrEducation as $edu) {
                                    ?>
                                    <tr>
                                      <td><?=$edu->Grade?></td>
                                      <td><?=$edu->TahunLulus?></td>
                                      <td><?=$edu->Institusi?></td>
                                      <td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="<?=$edu->Grade?>" /></td>
                                    </tr>
                                    <?php
                                  }
                                }
                                ?>
                              </tbody>
                              <tfoot>
                                <tr>
                                  <th>
                                    <select name="Grade" class="form-control" style="min-width: 8vw">
                                      <!--<option value="SD">SD</option>
                                      <option value="SMP">SMP</option>
                                      <option value="SMA/SMK">SMA/SMK</option>
                                      <option value="DIPLOMA">DIPLOMA</option>-->
                                      <option value="SARJANA">S1</option>
                                      <option value="MAGISTER">S2</option>
                                      <option value="DOKTOR">S3</option>
                                    </select>
                                  </th>
                                  <th class="text-center" style="white-space: nowrap">
                                    <input type="number" class="form-control" name="TahunLulus" placeholder="Tahun Lulus" />
                                  </th>
                                  <th class="text-center" style="white-space: nowrap">
                                    <input type="text" class="form-control" name="Institusi" placeholder="Nama Institusi" />
                                  </th>
                                  <th class="text-center">
                                    <button type="button" id="btn-add-education" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                                  </th>
                                </tr>
                              </tfoot>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                </div>
                <?php
                if($ruser[COL_ROLEID] == ROLEPSIKOLOG) {
                  ?>
                  <div class="col-sm-6 pl-3">
                    <div class="form-group row">
                      <label class="control-label col-sm-2">Grade</label>
                      <div class="col-sm-8">
                        <span class="badge pull-left badge-info"><?=$data[COL_NM_GRADE]?></span>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="control-label col-sm-2">Tarif</label>
                      <div class="col-sm-8">
                        <table>
                          <tr>
                            <td>Reguler</td><td class="pl-2">:</td><td class="text-right font-weight-bold"><?=!empty($rrate)?number_format($rrate[COL_NUM_RATEREGULAR]):''?></td>
                          </tr>
                          <tr>
                            <td>Online</td><td class="pl-2">:</td><td class="text-right font-weight-bold"><?=!empty($rrate)?number_format($rrate[COL_NUM_RATEONLINE]):''?></td>
                          </tr>
                          <tr>
                            <td>Care Group</td><td class="pl-2">:</td><td class="text-right font-weight-bold"><?=!empty($rrate)?number_format($rrate[COL_NUM_RATEGROUP]):''?></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Biodata Singkat (maks. 200 karakter)</label>
                      <div class="row">
                        <div class="col-sm-12">
                          <textarea class="form-control" name="<?=COL_NM_BIO?>" maxlength="200" placeholder="Biodata Singkat"><?=$data[COL_NM_BIO]?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                        <label>Layanan Konsultasi</label>
                        <div class="row">
                          <div class="col-sm-12">
                            <?php
                            $arrType = array();
                            $rtype = $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->get(TBL_USERTYPE)->result_array();
                            foreach($rtype as $s) {
                              $arrType[] = $s[COL_KD_TYPE];
                            }
                            ?>
                            <select name="KD_Type[]" class="form-control no-select2" style="width: 100%" multiple>
                              <?=GetCombobox("SELECT * from mtype order by NM_Type", COL_KD_TYPE, COL_NM_TYPE, $arrType)?>
                            </select>
                          </div>
                        </div>
                    </div>
                    <div class="form-group">
                      <input type="hidden" name="UserBills" />
                      <div class="row">
                        <div class="col-sm-12">
                          <table id="tbl-bill" class="table table-bordered">
                            <thead>
                              <tr>
                                <th>Informasi Rekening</th>
                                <th class="text-center">#</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php
                              if(count($rUserBills)  > 0) {
                                foreach($rUserBills as $s) {
                                  ?>
                                  <tr>
                                    <td><?=$s[COL_KD_BANK].'<br />'.$s[COL_NM_ACCOUNTNO].' an. '.$s[COL_NM_ACCOUNTNAME]?></td>
                                    <td class="text-center">
                                      <button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button>
                                      <input type="hidden" name="idx" value="<?=$s[COL_KD_BANK]?>" />
                                    </td>
                                  </tr>
                                  <?php
                                }
                              } else {
                                ?>
                                <tr>
                                  <td colspan="2">
                                    <p class="font-italic mb-0">Data masih kosong.</p>
                                  </td>
                                </tr>
                                <?php
                              }
                              ?>
                            </tbody>
                            <tfoot>
                              <tr>
                                <th style="white-space: nowrap">
                                  <div class="row">
                                    <div class="col-sm-6 p-2">
                                      <select name="<?=COL_KD_BANK?>" class="form-control" style="min-width: 10vw">
                                        <?=GetCombobox("SELECT * from mbank order by KD_Bank", COL_KD_BANK, COL_KD_BANK, null, true, false, '-- Bank --')?>
                                      </select>
                                    </div>
                                    <div class="col-sm-6 p-2">
                                      <input type="text" class="form-control" name="<?=COL_NM_ACCOUNTNO?>" placeholder="No. Rekening" />
                                    </div>
                                    <div class="col-sm-12 p-2">
                                      <input type="text" class="form-control" name="<?=COL_NM_ACCOUNTNAME?>" placeholder="Nama Rekening" />
                                    </div>
                                  </div>
                                </th>
                                <th class="text-center">
                                  <div class="row">
                                    <div class="col-sm-12 p-2">
                                      <button type="button" id="btn-add-bill" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>

                                </th>
                              </tr>
                            </tfoot>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php
                }
                ?>
              </div>
            </div>
            <div class="card-footer">
              <div class="form-group row">
                <div class="col-sm-12 text-center">
                  <button type="submit" class="btn btn-outline-info btn-block"><i class="fad fa-save"></i>&nbsp;SIMPAN</button>
                </div>
              </div>
            </div>
            <?=form_close()?>
          </div>
        </div>
        <div class="col-sm-6">
          <?php
          if($ruser[COL_ROLEID] != ROLEPSIKOLOG) {
            ?>
            <div class="card card-outline card-info">
              <div class="card-header">
                <h5 class="card-title font-weight-light">Perasaan kamu saat ini..</h5>
              </div>
              <?=form_open(site_url('site/ajax/status-add'),array('role'=>'form','id'=>'form-status','class'=>'form-horizontal'))?>
              <div class="card-body">
                <div class="form-group">
                  <?php
                  if(!empty($rstatus)) {
                    ?>
                    <label>Status Terakhir:</label>
                    <div class="row">
                      <div class="col-sm-12">
                        <p><?=$rstatus[COL_STATUS]?></p>
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                  <div class="row">
                    <div class="col-sm-12">
                      <textarea name="<?=COL_STATUS?>" class="form-control" rows="5" placeholder="Apa yang kamu rasakah hari ini? Yuk catat disini..."></textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="form-group row">
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-outline-info btn-block"><i class="fad fa-comment-alt-lines"></i>&nbsp;UBAH STATUS</button>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
            <?php
          } else {
            ?>

            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  $.get('https://wilayah-api.herokuapp.com/provinsi', function(data) {
    var dataProvinsi = $.map(data, function(n,i){
      return {id: n.name, text: n.name, selected: n.name=='<?=$data[COL_NM_PROVINCE]?>'}
    });

    $('select[name=<?=COL_NM_PROVINCE?>]').select2({
      width: 'resolve',
      theme: 'bootstrap4',
      placeholder: 'Pilih Provinsi',
      data: dataProvinsi
    }).trigger('change');
  });

  $('select[name=<?=COL_NM_PROVINCE?>]').change(function() {
    var dis = $(this);
    var url = 'https://wilayah-api.herokuapp.com/provinsi?name='+dis.val();

    $.get(url, function(data) {
      var provId = 0;
      if(data) {
        provId = data[0].id;
      }

      $.get('https://wilayah-api.herokuapp.com/kabupaten?province_id='+provId, function(kot) {
        var dataKota = $.map(kot, function(n,i){
          return {id: n.name, text: n.name, selected: n.name=='<?=$data[COL_NM_CITY]?>'}
        });

        $('select[name=<?=COL_NM_CITY?>]').empty();
        $('select[name=<?=COL_NM_CITY?>]').select2({
          width: 'resolve',
          theme: 'bootstrap4',
          placeholder: 'Pilih Kabupaten / Kota',
          data: dataKota
        }).trigger('change');
      });
    });
  });

  $('select[name^=KD_Type]').select2({
    width: 'resolve',
    theme: 'bootstrap4',
    placeholder: 'Pilih Layanan Konsultasi'
  });
  $('.calendar').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minDate: moment().format('YYYY-MM-DD'),
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'Y-MM-DD'
    }
  });
  $('.birthdaypicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().subtract(moment.duration(80, 'Y')).format('YYYY'), 10),
    maxYear: parseInt(moment().subtract(moment.duration(14, 'Y')).format('YYYY'), 10),
    locale: {
        format: 'Y-MM-DD'
    }
  });

  $('form').each(function() {
    $(this).validate({
      ignore: "[type=file]",
      /*invalidHandler: function(event, validator) {
        // 'this' refers to the form
        var errors = validator.numberOfInvalids();
        if (errors) {
          var message = errors == 1
            ? 'You missed 1 field. It has been highlighted'
            : 'You missed ' + errors + ' fields. They have been highlighted';
          alert(message);
        }
      },*/
      submitHandler: function(form) {
        var btnSubmit = $('button[type=submit]', $(form));
        var txtSubmit = btnSubmit[0].innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        $(form).ajaxSubmit({
          dataType: 'json',
          type : 'post',
          success: function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
              if(res.data && res.data.redirect) {
                setTimeout(function(){
                  location.href = res.data.redirect;
                }, 1000);
              }
            }
          },
          error: function() {
            toastr.error('SERVER ERROR');
          },
          complete: function() {
            btnSubmit.html(txtSubmit);
          }
        });
        return false;
      }
    });
  });

  $('[name=ScheduleFixed]').change(function() {
    writeSchedule('tbl-schedule-fixed', 'ScheduleFixed');
  }).val(encodeURIComponent('<?=$data['ScheduleFixed']?>')).trigger('change');
  $('[name=ScheduleCalendar]').change(function() {
    writeSchedule('tbl-schedule-calendar', 'ScheduleCalendar');
  }).val(encodeURIComponent('<?=$data['ScheduleCalendar']?>')).trigger('change');
  $('[name=UserBills]').change(function() {
    writeBill('tbl-bill', 'UserBills');
  }).val(encodeURIComponent('<?=$data['UserBills']?>')).trigger('change');
  $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').change(function() {
    writeEducation('tbl-education', '<?=COL_NM_EDUCATIONALBACKGROUND?>');
  }).val(encodeURIComponent('<?=(!empty($data)?$data[COL_NM_EDUCATIONALBACKGROUND]:'')?>')).trigger('change');

  $('#btn-add-schedule-fixed', $('#tbl-schedule-fixed')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleFixed]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': $('[name=Day] option:selected', row).html(), 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleFixed]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('#btn-add-schedule-calendar', $('#tbl-schedule-calendar')).click(function() {
    var dis = $(this);
    var arr = $('[name=ScheduleCalendar]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': day, 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=ScheduleCalendar]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('[name^=ScheduleType]').change(function() {
    //var el = $('[name=ScheduleType]:checked');
    var tbl = $(this).closest('table');
    if($(this).is(':checked')) {
      $('tr:not(.row-checkbox)', tbl).removeClass('d-none');
      $('tr.row-checkbox', tbl).removeClass('row-checkbox').addClass('bg-secondary disabled row-checkbox');
    } else {
      $('tr:not(.row-checkbox)', tbl).addClass('d-none');
      $('tr.row-checkbox', tbl).removeClass('bg-secondary disabled');
    }
  }).trigger('change');

  $('#btn-add-bill', $('#tbl-bill')).click(function() {
    var dis = $(this);
    var arr = $('[name=UserBills]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var kdBank = $('[name=KD_Bank]', row).val();
    var nmAccountNo = $('[name=NM_AccountNo]', row).val();
    var nmAccountName = $('[name=NM_AccountName]', row).val();
    if(kdBank && nmAccountNo && nmAccountName) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Bank == kdBank;
      });
      if(exist.length == 0) {
        arr.push({'Bank': kdBank, 'AccountNo': nmAccountNo, 'AccountName':nmAccountName});
        console.log(arr);
        $('[name=UserBills]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi informasi bank dengan benar.');
    }
  });

  $('#btn-add-education', $('#tbl-education')).click(function() {
    var dis = $(this);
    var arr = $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var grade = $('[name=Grade]', row).val();
    var tahun = $('[name=TahunLulus]', row).val();
    var institusi = $('[name=Institusi]', row).val();
    if(grade && tahun && institusi) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Grade == grade;
      });
      if(exist.length == 0) {
        arr.push({'Grade': grade, 'TahunLulus':tahun, 'Institusi':institusi});
        $('[name=<?=COL_NM_EDUCATIONALBACKGROUND?>]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi pendidikan dengan benar.');
    }
  });
});

function writeSchedule(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return a['Day'].localeCompare(b['Day']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].DayText+'</td>';
        html += '<td>'+arr[i].TimeFrom+' - '+arr[i].TimeTo+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Day+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Day != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

function writeBill(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return a['Bank'].localeCompare(b['Bank']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].Bank+'<br />'+arr[i].AccountNo+' an. '+arr[i].AccountName+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Bank+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Bank != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

function writeEducation(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].Grade+'</td>';
        html += '<td>'+arr[i].TahunLulus+'</td>';
        html += '<td>'+arr[i].Institusi+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Grade+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Grade != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="3"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
