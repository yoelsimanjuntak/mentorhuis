<?php
class Home extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(IsLogin()) {
      $ruser = GetLoggedUser();
      if($ruser[COL_ROLEID] == ROLEADMIN) {
        redirect('admin/dashboard');
      }
    }

    $this->load->model('mpost');
    $this->load->library('google');
  }

  public function google_auth_ver() {
    echo $this->google->getLibraryVersion();
  }

  public function index() {
    /*s$this->load->model('mpost');

    $data['berita'] = $this->mpost->search(3,"",1);
    $data['notif'] = $this->mpost->search(5,"",2);
    $data['grafis'] = $this->mpost->search(5,"",3);

    $data['count_berita'] = $this->db
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, 1)
    ->where(TBL__POSTS.".".COL_ISSUSPEND, false)
    ->where(TBL__POSTS.".".COL_POSTEXPIREDDATE." >= ", date("Y-m-d"))
    ->count_all_results(TBL__POSTS);

    $data['count_notif'] = $this->db
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, 2)
    ->where(TBL__POSTS.".".COL_ISSUSPEND, false)
    ->where(TBL__POSTS.".".COL_POSTEXPIREDDATE." >= ", date("Y-m-d"))
    ->count_all_results(TBL__POSTS);

    $data['count_grafis'] = $this->db
    ->where(TBL__POSTS.".".COL_POSTCATEGORYID, 3)
    ->where(TBL__POSTS.".".COL_ISSUSPEND, false)
    ->where(TBL__POSTS.".".COL_POSTEXPIREDDATE." >= ", date("Y-m-d"))
    ->count_all_results(TBL__POSTS);*/
    $data = array();
		$this->template->set('title', 'Home');
		$this->template->load('frontend' , 'home/index', $data);
    //$this->load->view('home/index');
  }

  public function dashboard() {
      if(!IsLogin()) {
          redirect('site/user/login');
      }

      $ruser = GetLoggedUser();
      $data['title'] = 'Beranda';
      if($ruser[COL_ROLEID]!=ROLEPSIKOLOG) {
        $data['session'] = $this->db
        ->select(TBL_TSESSION.'.*, '.TBL_TSESSION_ENTRY.'.'.COL_KD_STATUSPAYMENT.', '.TBL_MTYPE.'.'.COL_NM_TYPE.', '.TBL_TSESSION_ENTRY.'.'.COL_NM_PAYMENTIMAGE.', mentor.'.COL_NM_FIRSTNAME.' as Mentor_FirstName, mentor.'.COL_NM_LASTNAME.' as Mentor_LastName')
        ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_KD_SESSION." = ".TBL_TSESSION_ENTRY.".".COL_KD_SESSION,"inner")
        ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
        ->join(TBL__USERINFORMATION.' mentor','mentor.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_NM_SESSIONHOST,"inner")
        ->where(TBL_TSESSION_ENTRY.'.'.COL_USERNAME, $ruser[COL_USERNAME])
        ->order_by(COL_KD_SESSION, 'desc')
        ->get(TBL_TSESSION_ENTRY)
        ->result_array();

        $this->template->load('frontend', 'site/home/dashboard', $data);
      } else {
        $data['session'] = $this->db
        ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
        ->where(TBL_TSESSION.'.'.COL_NM_SESSIONHOST, $ruser[COL_USERNAME])
        ->where(TBL_TSESSION.'.'.COL_DATE_SESSIONDATE.' >=', date('Y-m-d'))
        ->where_in(TBL_TSESSION.'.'.COL_KD_STATUS, array(SESSIONSTAT_NEW, SESSIONSTAT_CONFIRMED))
        ->order_by(COL_DATE_SESSIONDATE, 'desc')
        ->get(TBL_TSESSION)
        ->result_array();

        $this->template->load('frontend', 'site/home/dashboard_psikolog', $data);
      }

  }

  public function profile() {
      if(!IsLogin()) {
          redirect('site/user/login');
      }

      $user = GetLoggedUser();
      $this->load->model('muser');
      $data['title'] = 'Profil';
      $data['data'] = $rdata = $this->muser->getdetails($user[COL_USERNAME]);

      $arrScheduleFixed = array();
      $rScheduleFixed = $this->db
      ->join(TBL_MDAYS,TBL_MDAYS.'.'.COL_KD_DAY." = ".TBL_USERSCHEDULE.".".COL_KD_SCHEDULEDAY,"left")
      ->where(COL_USERNAME, $user[COL_USERNAME])
      ->where(COL_KD_SCHEDULETYPE, 'FIXED')
      ->get(TBL_USERSCHEDULE)
      ->result_array();
      foreach($rScheduleFixed as $s) {
        $arrScheduleFixed[] = array(
          'Day' => $s[COL_KD_SCHEDULEDAY],
          'DayText' => $s[COL_NM_DAY],
          'TimeFrom' => $s[COL_DATE_SCHEDULETIME_FROM],
          'TimeTo' => $s[COL_DATE_SCHEDULETIME_TO]
        );
      }

      $arrScheduleCalendar = array();
      $rScheduleCalendar = $this->db
      ->where(COL_USERNAME, $user[COL_USERNAME])
      ->where(COL_KD_SCHEDULETYPE, 'CALENDAR')
      ->where(COL_DATE_SCHEDULEDATE.' >= ', date('Y-m-d'))
      ->get(TBL_USERSCHEDULE)
      ->result_array();
      foreach($rScheduleCalendar as $s) {
        $arrScheduleCalendar[] = array(
          'Day' => $s[COL_DATE_SCHEDULEDATE],
          'DayText' => $s[COL_DATE_SCHEDULEDATE],
          'TimeFrom' => $s[COL_DATE_SCHEDULETIME_FROM],
          'TimeTo' => $s[COL_DATE_SCHEDULETIME_TO]
        );
      }

      $arrBills = array();
      $rUserBills = $this->db
      ->where(COL_USERNAME, $user[COL_USERNAME])
      ->get(TBL_USERBILL)
      ->result_array();
      foreach($rUserBills as $s) {
        $arrBills[] = array(
          'Bank' => $s[COL_KD_BANK],
          'AccountNo' => $s[COL_NM_ACCOUNTNO],
          'AccountName' => $s[COL_NM_ACCOUNTNAME]
        );
      }

      $data['rScheduleFixed'] = $rScheduleFixed;
      $data['rScheduleCalendar'] = $rScheduleCalendar;
      $data['rUserBills'] = $rUserBills;
      $data['data']['ScheduleFixed'] = json_encode($arrScheduleFixed);
      $data['data']['ScheduleCalendar'] = json_encode($arrScheduleCalendar);
      $data['data']['UserBills'] = json_encode($arrBills);
  		$this->template->load('frontend', 'site/home/profile', $data);
  }

  public function page($slug) {
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('backend' , 'home/_error', $data);
    } else {
      $this->template->load('frontend' , 'home/_error', $data);
    }
  }

  public function post($cat) {
    $data['title'] = 'Tautan';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where(TBL__POSTS.".".COL_POSTCATEGORYID, $cat);
    $this->db->order_by(TBL__POSTS.".".COL_CREATEDON, 'desc');
    $data['res'] = $rpost = $this->db->get(TBL__POSTS)->result_array();

    if(!empty($rpost)) {
      $data['title'] = $rpost[0][COL_POSTCATEGORYNAME];
    }

    $data['data'] = $rpost;
    $this->template->load('frontend' , 'home/post', $data);
  }

  public function register_done($email='') {
    $data['title'] = 'Registrasi Berhasil!';
    $data['email'] = $email = GetDecryption($email);
    $data['verify'] = false;

    $data['user'] = $ruserinfo = $this->db
    ->where(COL_USERNAME, $email)
    ->get(TBL__USERS)
    ->row_array();
    if(empty($ruserinfo)) {
      show_error('Email tidak valid.');
      return;
    }

    if($ruserinfo[COL_ROLEID] != ROLEPSIKOLOG) {
      $link = site_url('site/home/verify/'.GetEncryption($email));
      $mailSubj = 'MentorHuis - Verifikasi Akun';
      $mailContent = @"
      <p>
        Terima kasih sudah bergabung di <strong>MentorHuis</strong>, silakan klik link dibawah untuk mengaktifkan akun anda:
      </p>
      $link
      ";
      $send = SendMail($email, $mailSubj, $mailContent);
    }

    $this->template->load('frontend' , 'user/register_done', $data);
  }

  public function register_finish($email='') {
    $data['title'] = 'Formulir Registrasi';
    $data['email'] = $email = GetDecryption($email);
    $data['ruser'] = $ruser = $this->db->where(COL_USERNAME, $email)->get(TBL__USERINFORMATION)->row_array();

    $this->template->load('frontend' , 'user/register_finish', $data);
  }

  public function verify($email) {
    $email = GetDecryption($email);

    $ruser = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->where(TBL__USERINFORMATION.'.'.COL_EMAIL, $email)
    ->get(TBL__USERS)
    ->row_array();

    if($ruser) {
      $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->update(TBL__USERINFORMATION, array(COL_IS_EMAILVERIFIED=>true));
    }
    $data['title'] = 'Verifikasi Akun Berhasil';
    $data['verify'] = true;
    $data['user'] = $ruser;
    $this->template->load('frontend' , 'user/register_done', $data);
  }

  public function services() {
    $data['title'] = 'Layanan';
    $this->template->load('frontend' , 'home/services', $data);
  }
  public function about() {
    $data['title'] = 'Tentang Kami';
    $this->template->load('frontend' , 'home/about', $data);
  }
  public function terms_and_conds() {
    $data['title'] = 'Terms and Condition';
    $this->template->load('frontend' , 'home/terms-and-conds', $data);
  }
  public function blog() {
    $data['title'] = 'Inspirasi';
    $data['rblog'] = $this->mpost->search(50,"",1);
    $this->template->load('frontend' , 'home/blog', $data);
  }
  public function questionaire() {
    $data['title'] = 'Kuisioner';
    $this->template->load('frontend' , 'home/questionaire', $data);
  }
}
 ?>
