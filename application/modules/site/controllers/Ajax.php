<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
      parent::__construct();
      setlocale (LC_TIME, 'id_ID');
  }

  function now() {
    //echo date('D, d M Y H:i:s');

    $dt = json_encode(array(
      'Day'=>date('D'),
      'Date'=>date('d'),
      'Month'=>date('M'),
      'Year'=>date('Y'),
      'Hour'=>date('H'),
      'Minute'=>date('i'),
      'Second'=>date('s')
    ));
    echo $dt;
  }

  public function register() {
    $this->form_validation->set_rules(array(
      /*array(
        'field' => COL_USERNAME,
        'label' => COL_USERNAME,
        'rules' => 'required|min_length[5]|regex_match[/^[a-z0-9.\-]+$/i]|is_unique[_users.UserName]',
        'errors' => array('is_unique' => 'Username sudah digunakan.')
      ),*/
      array(
        'field' => COL_EMAIL,
        'label' => COL_EMAIL,
        'rules' => 'required|valid_email|is_unique[_userinformation.Email]',
        'errors' => array('is_unique' => 'Email sudah digunakan.')
      ),
      array(
        'field' => COL_PASSWORD,
        'label' => COL_PASSWORD,
        'rules' => 'required|min_length[5]',
        'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
      ),
      array(
        'field' => 'ConfirmPassword',
        'label' => 'Repeat ConfirmPassword',
        'rules' => 'required|matches[Password]',
        'errors' => array('matches' => 'Kolom Ulangi Password wajib sama dengan Password.')
      )
    ));
    if($this->form_validation->run()) {
      $this->load->model('muser');
      $userType = $this->input->post('UserType');
      $userRole = $this->input->post(COL_ROLEID);

      if($userType == 'Psikolog') {
        $userRole = ROLEPSIKOLOG;
      }
      $userdata = array(
        COL_USERNAME => $this->input->post(COL_EMAIL),
        COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID => $userRole,
        COL_ISSUSPEND => $userRole == ROLEPSIKOLOG ? true : false
      );

      $userinfo = array(
        COL_USERNAME => $this->input->post(COL_EMAIL),
        COL_EMAIL => $this->input->post(COL_EMAIL),
        COL_NM_FIRSTNAME => $this->input->post(COL_NM_FIRSTNAME) ? $this->input->post(COL_NM_FIRSTNAME) : 'Pengguna',
        COL_DATE_REGISTERED => date('Y-m-d')
      );

      $reg = $this->muser->register($userdata, $userinfo);
      if($reg) {
        if($userRole == ROLEPSIKOLOG) {
          ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/home/register-finish/'.GetEncryption($userinfo[COL_EMAIL]))));
          return;
        } else {
          ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/home/register-done/'.GetEncryption($userinfo[COL_EMAIL]))));
          return;
        }
      }
      else {
        ShowJsonError('Error');
        return;
      }
    } else {
      $err = validation_errors();
      ShowJsonError($err);
    }
  }

  public function register_finish($email) {
    $email = GetDecryption($email);
    $this->form_validation->set_rules(array(
      array(
        'field' => 'KD_Type[]',
        'label' => 'Spesialisasi',
        'rules' => 'required',
        'errors' => array('required' => 'Harap isi Spesialisasi.')
      ),
      array(
        'field' => COL_NM_EDUCATIONALBACKGROUND,
        'label' => 'Riwayat Pendidikan',
        'rules' => 'required',
        'errors' => array('required' => 'Harap isi Riwayat Pendidikan.')
      ),
      array(
        'field' => 'ScheduleFixed',
        'label' => 'Jadwal',
        'rules' => 'required',
        'errors' => array('required' => 'Harap isi Jadwal.')
      )
    ));
    if($this->form_validation->run()) {
      $type = $this->input->post(COL_KD_TYPE);
      $scheduleFixed = $this->input->post("ScheduleFixed");
      $userinfo = array(
        COL_NM_FIRSTNAME => $this->input->post(COL_NM_FIRSTNAME),
        COL_NM_LASTNAME => $this->input->post(COL_NM_LASTNAME),
        COL_NM_NICKNAME => $this->input->post(COL_NM_NICKNAME),
        COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
        COL_NM_BIO => $this->input->post(COL_NM_BIO),
        COL_NM_GRADE => 'MUDA',
        COL_NM_PROVINCE => $this->input->post(COL_NM_PROVINCE),
        COL_NM_CITY => $this->input->post(COL_NM_CITY),
        COL_DATE_BIRTH => $this->input->post(COL_DATE_BIRTH),
        COL_NO_PHONE => $this->input->post(COL_NO_PHONE),
        COL_NM_MARITALSTATUS => $this->input->post(COL_NM_MARITALSTATUS),
        COL_NM_OCCUPATION => $this->input->post(COL_NM_OCCUPATION),
        COL_NUM_EXPERIENCE => !empty($this->input->post(COL_NUM_EXPERIENCE)) ? toNum($this->input->post(COL_NUM_EXPERIENCE)) : 0,
        COL_NM_EDUCATIONALBACKGROUND => !empty($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) ? urldecode($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) : null ,
      );

      $arrType = array();
      $arrScheduleFixed = array();

      if(!empty($type)) {
        foreach($type as $s) {
          $arrType[] = array(COL_USERNAME=>$email, COL_KD_TYPE=>$s);
        }
      }
      if(!empty($scheduleFixed)) {
        $scheduleFixed = json_decode(urldecode($scheduleFixed));
        foreach($scheduleFixed as $s) {
          $arrScheduleFixed[] = array(
            COL_USERNAME=>$email,
            COL_KD_SCHEDULETYPE=>'FIXED',
            COL_KD_SCHEDULEDAY=>$s->Day,
            COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
            COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
          );
        }
      }

      $res = true;
      $this->db->trans_begin();
      try {
        $this->db->where(COL_USERNAME, $email)->delete(TBL_USERTYPE);
        $this->db->where(COL_USERNAME, $email)->delete(TBL_USERSCHEDULE);

        if(!empty($arrType)) {
          $res = $this->db->insert_batch(TBL_USERTYPE, $arrType);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }
        if(!empty($arrScheduleFixed)) {
          $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleFixed);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }

        $res = $this->db->where(COL_USERNAME, $email)->update(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/home/register-done/'.GetEncryption($email))));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    } else {
      $err = validation_errors();
      ShowJsonError($err);
    }
  }

  public function login() {
    $this->form_validation->set_rules(array(
      array(
        'field' => 'UserName',
        'label' => 'UserName',
        'rules' => 'required'
      ),
      array(
        'field' => 'Password',
        'label' => 'Password',
        'rules' => 'required'
      )
    ));
    if($this->form_validation->run()) {
      $this->load->model('muser');
      $username = $this->input->post(COL_USERNAME);
      $password = $this->input->post(COL_PASSWORD);

      if($this->muser->authenticate($username, $password)) {
        if($this->muser->IsSuspend($username)) {
          ShowJsonError('Akun anda di suspend.');
          return;
        }

        $userdetails = $this->muser->getdetails($username);
        if(empty($userdetails[COL_IS_EMAILVERIFIED]) || $userdetails[COL_IS_EMAILVERIFIED] != 1) {
          ShowJsonError('Maaf, akun kamu belum diverifikasi. Silakan periksa link verifikasi yang dikirimkan melalui email kamu.');
          return;
        }

        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));
        SetLoginSession($userdetails);
        ShowJsonSuccess('Login berhasil.', array('redirect'=>site_url('site/home/dashboard')));
      } else {
        $ruser = $this->db
        ->where(COL_USERNAME, $username)
        ->get(TBL__USERINFORMATION)
        ->row_array();
        if(!empty($ruser) && $ruser[COL_IS_LOGINVIAGOOGLE]) {
          ShowJsonError('<p>Akun ini telah didaftarkan melalui Google. Anda dapat menggunakan fitur <strong>Login Via Google</strong> untuk melanjutkan.</p>');
          return;
        }
        ShowJsonError('Username / password tidak tepat.');
      }
    }
  }

  public function profile_edit() {
    $this->load->model('muser');
    $ruser = GetLoggedUser();
    $data = array(
      COL_NM_FIRSTNAME => $this->input->post(COL_NM_FIRSTNAME),
      COL_NM_LASTNAME => $this->input->post(COL_NM_LASTNAME),
      COL_NM_NICKNAME => $this->input->post(COL_NM_NICKNAME),
      COL_NM_ADDRESS => $this->input->post(COL_NM_ADDRESS),
      COL_NM_BIO => $this->input->post(COL_NM_BIO),
      COL_NM_PROVINCE => $this->input->post(COL_NM_PROVINCE),
      COL_NM_CITY => $this->input->post(COL_NM_CITY),
      COL_DATE_BIRTH => $this->input->post(COL_DATE_BIRTH),
      COL_NO_PHONE => $this->input->post(COL_NO_PHONE),
      COL_NM_MARITALSTATUS => $this->input->post(COL_NM_MARITALSTATUS),
      COL_NM_OCCUPATION => $this->input->post(COL_NM_OCCUPATION),
      COL_NUM_EXPERIENCE => !empty($this->input->post(COL_NUM_EXPERIENCE)) ? toNum($this->input->post(COL_NUM_EXPERIENCE)) : 0,
      COL_NM_EDUCATIONALBACKGROUND => !empty($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) ? urldecode($this->input->post(COL_NM_EDUCATIONALBACKGROUND)) : null ,
    );

    $config['upload_path'] = MY_UPLOADPATH;
    $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
    $config['max_size']	= 102400;
    $config['max_width']  = 2560;
    $config['max_height']  = 2560;
    $config['overwrite'] = TRUE;
    if(!empty($_FILES["avatar"]["name"])) {
      $arrname = explode('@', $ruser[COL_USERNAME]);
      $uname = $arrname[0];
      if(!empty($uname)) {
        $config['file_name'] = strtolower(urlencode($uname))."-".date('Y-m-dHis').".".end((explode(".", $_FILES["avatar"]["name"])));
      }

      $this->load->library('upload',$config);
      if(!$this->upload->do_upload("avatar")){
          $err = $this->upload->display_errors();
          ShowJsonError(strip_tags($err));
          return;
      }

      $dataupload = $this->upload->data();
      if(!empty($dataupload) && $dataupload['file_name']) {
          $data[COL_NM_PROFILEIMAGE] = $dataupload['file_name'];
          if(!empty($ruser[COL_NM_PROFILEIMAGE]) && file_exists(MY_UPLOADPATH.$ruser[COL_NM_PROFILEIMAGE])) {
            unlink(MY_UPLOADPATH.$ruser[COL_NM_PROFILEIMAGE]);
          }
      }
    }

    $res = $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->update(TBL__USERINFORMATION, $data);
    if(!$res) {
      $err = $this->db->_error_message();
      ShowJsonError($err);
      return;
    } else {
      if($ruser[COL_ROLEID] == ROLEPSIKOLOG) {
        $type = $this->input->post(COL_KD_TYPE);
        $scheduleFixed = $this->input->post("ScheduleFixed");
        $scheduleCalendar = $this->input->post("ScheduleCalendar");
        $checkedFixed = $this->input->post('ScheduleTypeFixed');
        $checkedCalendar = $this->input->post('ScheduleTypeCalendar');
        $userbills = $this->input->post("UserBills");

        $rateRegular = $this->input->post(COL_NUM_RATEREGULAR);
        $rateOnline = $this->input->post(COL_NUM_RATEONLINE);

        $arrType = array();
        $arrScheduleFixed = array();
        $arrScheduleCalendar = array();
        $arrUserBills = array();

        if(!empty($type)) {
          foreach($type as $s) {
            $arrType[] = array(COL_USERNAME=>$ruser[COL_USERNAME], COL_KD_TYPE=>$s);
          }
        }
        if(!empty($scheduleFixed) && $checkedFixed == 'on') {
          $scheduleFixed = json_decode(urldecode($scheduleFixed));
          foreach($scheduleFixed as $s) {
            $arrScheduleFixed[] = array(
              COL_USERNAME=>$ruser[COL_USERNAME],
              COL_KD_SCHEDULETYPE=>'FIXED',
              COL_DATE_SCHEDULEDATE=>null,
              COL_KD_SCHEDULEDAY=>$s->Day,
              COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
              COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
            );
          }
        }
        if(!empty($scheduleCalendar) && $checkedCalendar == 'on') {
          $scheduleCalendar = json_decode(urldecode($scheduleCalendar));
          foreach($scheduleCalendar as $s) {
            $arrScheduleCalendar[] = array(
              COL_USERNAME=>$ruser[COL_USERNAME],
              COL_KD_SCHEDULETYPE=>'CALENDAR',
              COL_DATE_SCHEDULEDATE=>$s->Day,
              COL_KD_SCHEDULEDAY=>null,
              COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
              COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
            );
          }
        }
        if(!empty($userbills)) {
          $userbills = json_decode(urldecode($userbills));
          foreach($userbills as $s) {
            $arrUserBills[] = array(
              COL_USERNAME=>$ruser[COL_USERNAME],
              COL_KD_BANK=>$s->Bank,
              COL_NM_ACCOUNTNO=>$s->AccountNo,
              COL_NM_ACCOUNTNAME=>$s->AccountName
            );
          }
        }

        $arrScheduleMerged = array_merge($arrScheduleFixed, $arrScheduleCalendar);
        $res = true;
        $this->db->trans_begin();
        try {
          if(!empty($arrType)) {
            $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERTYPE);
            $res = $this->db->insert_batch(TBL_USERTYPE, $arrType);
            if(!$res) {
              throw new Exception('Error: '.$this->db->error());
            }
          }

          if(!empty($arrScheduleMerged)) {
            $res = $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERSCHEDULE);
            $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleMerged);
            if(!$res) {
              throw new Exception('Error: '.$this->db->error());
            }
          }
          if(!empty($arrUserBills)) {
            $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERBILL);
            $res = $this->db->insert_batch(TBL_USERBILL, $arrUserBills);
            if(!$res) {
              throw new Exception('Error: '.$this->db->error());
            }
          }
          $res = $this->db
          ->where(COL_USERNAME, $ruser[COL_USERNAME])
          ->update(TBL__USERINFORMATION, array(
            COL_NM_BIO => $this->input->post(COL_NM_BIO),
            COL_NUM_RATEREGULAR => !empty($rateRegular) ? toNum($rateRegular) : null,
            COL_NUM_RATEONLINE => !empty($rateOnline) ? toNum($rateOnline) : null
          ));
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
          $this->db->trans_commit();
          $userdetails = $this->muser->getdetails($ruser[COL_USERNAME]);
          SetLoginSession($userdetails);
          ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/home/profile')));
          return;
        } catch (Exception $e) {
          $this->db->trans_rollback();
          ShowJsonError('Server Error');
          return;
        }
      }
      ShowJsonSuccess('Profil berhasil diubah', array('redirect'=>site_url('site/home/profile')));
      return;
    }
  }

  public function status_add() {
    $ruser = GetLoggedUser();
    $data = array(
      COL_STATUS => $this->input->post(COL_STATUS),
      COL_USERNAME => $ruser[COL_USERNAME],
      COL_CREATEDON => date('Y-m-d H:i:s')
    );

    $res = $this->db->insert(TBL_USERSTATUS, $data);
    if(!$res) {
      $err = $this->db->_error_message();
      ShowJsonError($err);
      return;
    } else {
      ShowJsonSuccess('Status berhasil ditulis', array('redirect'=>site_url('site/home/profile')));
      return;
    }
  }

  public function user_setting() {
    $ruser = GetLoggedUser();
    $type = $this->input->post(COL_KD_TYPE);
    $scheduleFixed = $this->input->post("ScheduleFixed");
    $scheduleCalendar = $this->input->post("ScheduleCalendar");
    $checkedFixed = $this->input->post('ScheduleTypeFixed');
    $checkedCalendar = $this->input->post('ScheduleTypeCalendar');
    $userbills = $this->input->post("UserBills");

    $rateRegular = $this->input->post(COL_NUM_RATEREGULAR);
    $rateOnline = $this->input->post(COL_NUM_RATEONLINE);

    $arrType = array();
    $arrScheduleFixed = array();
    $arrScheduleCalendar = array();
    $arrUserBills = array();

    if(!empty($type)) {
      foreach($type as $s) {
        $arrType[] = array(COL_USERNAME=>$ruser[COL_USERNAME], COL_KD_TYPE=>$s);
      }
    }
    if(!empty($scheduleFixed) && $checkedFixed == 'on') {
      $scheduleFixed = json_decode(urldecode($scheduleFixed));
      foreach($scheduleFixed as $s) {
        $arrScheduleFixed[] = array(
          COL_USERNAME=>$ruser[COL_USERNAME],
          COL_KD_SCHEDULETYPE=>'FIXED',
          COL_DATE_SCHEDULEDATE=>null,
          COL_KD_SCHEDULEDAY=>$s->Day,
          COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
          COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
        );
      }
    }
    if(!empty($scheduleCalendar) && $checkedCalendar == 'on') {
      $scheduleCalendar = json_decode(urldecode($scheduleCalendar));
      foreach($scheduleCalendar as $s) {
        $arrScheduleCalendar[] = array(
          COL_USERNAME=>$ruser[COL_USERNAME],
          COL_KD_SCHEDULETYPE=>'CALENDAR',
          COL_DATE_SCHEDULEDATE=>$s->Day,
          COL_KD_SCHEDULEDAY=>null,
          COL_DATE_SCHEDULETIME_FROM=>$s->TimeFrom,
          COL_DATE_SCHEDULETIME_TO=>$s->TimeTo
        );
      }
    }
    if(!empty($userbills)) {
      $userbills = json_decode(urldecode($userbills));
      foreach($userbills as $s) {
        $arrUserBills[] = array(
          COL_USERNAME=>$ruser[COL_USERNAME],
          COL_KD_BANK=>$s->Bank,
          COL_NM_ACCOUNTNO=>$s->AccountNo,
          COL_NM_ACCOUNTNAME=>$s->AccountName
        );
      }
    }

    $arrScheduleMerged = array_merge($arrScheduleFixed, $arrScheduleCalendar);
    $res = true;
    $this->db->trans_begin();
    try {
      if(!empty($arrType)) {
        $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERTYPE);
        $res = $this->db->insert_batch(TBL_USERTYPE, $arrType);
        if(!$res) {
          throw new Exception('Error: '.$this->db->error());
        }
      }
      /*if(!empty($arrScheduleFixed)) {
        $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleFixed);
        if(!$res) {
          ShowJsonError(var_dump($arrScheduleFixed));
          return;
          throw new Exception('Error: '.$this->db->error());
        }
      }
      if(!empty($arrScheduleCalendar)) {
        $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERSCHEDULE);
        $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleCalendar);
        if(!$res) {
          throw new Exception('Error: '.$this->db->error());
        }
      }*/

      if(!empty($arrScheduleMerged)) {
        $res = $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERSCHEDULE);
        $res = $this->db->insert_batch(TBL_USERSCHEDULE, $arrScheduleMerged);
        if(!$res) {
          throw new Exception('Error: '.$this->db->error());
        }
      }
      if(!empty($arrUserBills)) {
        $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->delete(TBL_USERBILL);
        $res = $this->db->insert_batch(TBL_USERBILL, $arrUserBills);
        if(!$res) {
          throw new Exception('Error: '.$this->db->error());
        }
      }
      $res = $this->db
      ->where(COL_USERNAME, $ruser[COL_USERNAME])
      ->update(TBL__USERINFORMATION, array(
        COL_NM_BIO => $this->input->post(COL_NM_BIO),
        COL_NUM_RATEREGULAR => !empty($rateRegular) ? toNum($rateRegular) : null,
        COL_NUM_RATEONLINE => !empty($rateOnline) ? toNum($rateOnline) : null
      ));
      if(!$res) {
        throw new Exception('Error: '.$this->db->error());
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/home/profile')));
      return;
    } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError('Server Error');
        return;
    }
  }

  public function list_mentor() {
    $kdType = $this->input->post(COL_KD_TYPE);
    $isCareGroup = $this->input->post(COL_IS_CAREGROUP);

    if($isCareGroup) {
      $data['res'] = $this->db
      ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
      ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_NM_SESSIONHOST,"left")
      ->where(TBL_TSESSION.'.'.COL_KD_TYPE, $kdType)
      ->where(TBL_TSESSION.'.'.COL_IS_CAREGROUP, 1)
      ->where(TBL_TSESSION.'.'.COL_DATE_SESSIONDATE.' >=', date('Y-m-d'))
      ->order_by(COL_DATE_SESSIONDATE, 'desc')
      ->get(TBL_TSESSION)
      ->result_array();
      $this->load->view('sesi/_session_caregroup', $data);
    } else {
      $data['res'] = $this->db
      ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_USERTYPE.".".COL_USERNAME,"left")
      ->where(COL_KD_TYPE, $kdType)
      //->where('(_userinformation.NUM_RateRegular is not null or _userinformation.NUM_RateOnline is not null)')
      ->order_by(COL_NM_FIRSTNAME)
      ->order_by(COL_NM_LASTNAME)
      ->get(TBL_USERTYPE)
      ->result_array();
      $this->load->view('sesi/_mentor', $data);
    }
  }

  public function get_mentor_history($id) {
    $id = GetDecryption($id);
    $rmentor = $this->db
    ->where(COL_USERNAME, $id)
    ->get(TBL__USERINFORMATION)
    ->row_array();

    $rsession = $this->db
    ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
    ->where(COL_NM_SESSIONHOST, $id)
    ->where(COL_KD_STATUS, SESSIONSTAT_COMPLETED)
    ->order_by(COL_DATE_SESSIONDATE, 'desc')
    ->limit(5)
    ->get(TBL_TSESSION)
    ->result_array();

    if(!empty($rsession)) {
      $html = '';
      $html .= '<table class="table table-bordered">';
      $html .= '<tbody>';
      $html .= '<tr class="font-weight-bold">';
      $html .= '<td>Tanggal</td><td>Layanan</td><td>Rating</td>';
      $html .= '</tr>';
      foreach($rsession as $r) {
        $rrate = $this->db
        ->select('tsession_feedback.NUM_Rate')
        ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_KD_SESSION." = ".TBL_TSESSION_FEEDBACK.".".COL_KD_SESSION,"left")
        ->where(COL_KD_FEEDBACKTYPE, 'RATE')
        ->where(TBL_TSESSION.".".COL_KD_SESSION, $r[COL_KD_SESSION])
        ->where(TBL_TSESSION.".".COL_NM_SESSIONHOST, $r[COL_NM_SESSIONHOST])
        ->get(TBL_TSESSION_FEEDBACK)
        ->row_array();
        $q = $this->db->last_query();

        $nrate = toNum($rrate[COL_NUM_RATE]);
        $floating = ($nrate - floor($nrate)) != 0;
        $rate = '';
        if($nrate==0) {
          $rate = '<small class="font-italic">Belum ada rating.</small>';
        }

        for($i=0; $i<floor($nrate); $i++) {
          $rate .= '<i class="text-warning fas fa-star"></i>';
        }
        if($floating) {
          $rate .= '<i class="text-warning fas fa-star-half-alt"></i>';
        }

        $html .= '<td>'.date('d/m/Y', strtotime($r[COL_DATE_SESSIONDATE])).'</td><td>'.$r[COL_NM_TYPE].'</td><td>'.$rate.'</td>';
        $html .= '</tr>';
      }
      $html .= '</tbody>';
      $html .= '</table>';
      echo $html;
    } else {
      echo '<p class="font-italic text-center mt-2">Belum ada riwayat untuk ditampilkan.</p>';
    }
  }

  public function get_mentor_profile($id) {
    $id = GetDecryption($id);
    $rmentor = $this->db
    ->where(COL_USERNAME, $id)
    ->get(TBL__USERINFORMATION)
    ->row_array();

    $data['data'] = $rmentor;
    $this->load->view('sesi/_mentor_profile', $data);
  }

  public function form_mentor_schedule($id) {
    $id = GetDecryption($id);
    $data['mentor'] = $this->db
    ->where(COL_USERNAME, $id)
    ->get(TBL__USERINFORMATION)
    ->row_array();

    $data['scheduleFixed'] = $this->db
    ->join(TBL_MDAYS,TBL_MDAYS.'.'.COL_KD_DAY." = ".TBL_USERSCHEDULE.".".COL_KD_SCHEDULEDAY,"left")
    ->where(COL_USERNAME, $id)
    ->where(COL_KD_SCHEDULETYPE, 'FIXED')
    ->order_by(COL_KD_DAY)
    ->get(TBL_USERSCHEDULE)
    ->result_array();

    $data['scheduleCalendar'] = $this->db
    ->where(COL_USERNAME, $id)
    ->where(COL_KD_SCHEDULETYPE, 'CALENDAR')
    ->where(COL_DATE_SCHEDULEDATE.' >= ', date('Y-m-d'))
    ->get(TBL_USERSCHEDULE)
    ->result_array();

    $data['schedule'] = $this->db
    ->join(TBL_MDAYS,TBL_MDAYS.'.'.COL_KD_DAY." = ".TBL_USERSCHEDULE.".".COL_KD_SCHEDULEDAY,"left")
    ->where(COL_USERNAME, $id)
    ->order_by(COL_KD_SCHEDULETYPE, 'desc')
    ->order_by(COL_KD_DAY)
    ->get(TBL_USERSCHEDULE)
    ->result_array();

    $this->load->view('sesi/_mentor_schedule_form', $data);
  }

  public function session_add() {
    $ruser = GetLoggedUser();
    $isCareGroup = $this->input->post(COL_IS_CAREGROUP);
    if($isCareGroup) {
      $kdSession = $this->input->post(COL_KD_SESSION);
      $rec = array(
        COL_KD_SESSION => $kdSession,
        COL_KD_STATUSPAYMENT => SESSIONPAY_WAITING,
        COL_USERNAME => $ruser[COL_USERNAME],
        COL_NM_REMARKSCLIENTFEELING => $this->input->post(COL_NM_REMARKSCLIENTFEELING),
        COL_NM_REMARKSCLIENTCONDITION => $this->input->post(COL_NM_REMARKSCLIENTCONDITION),
        COL_NM_REMARKSCLIENTEXPECTATION => $this->input->post(COL_NM_REMARKSCLIENTEXPECTATION),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_TSESSION_ENTRY, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError("Database error: ".$err['message']);
        return;
      }

      ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/sesi/finish/'.$kdSession)));
      return;
    } else {
      $rmentor = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_NM_SESSIONHOST))
      ->get(TBL__USERINFORMATION)
      ->row_array();

      if(empty($rmentor)) {
        ShowJsonError('Mentor tidak valid.');
        return;
      }

      $date = $this->input->post(COL_DATE_SESSIONDATE);
      $time = $this->input->post(COL_DATE_SESSIONTIME);
      $rday = $this->db
      ->where(COL_USERNAME, $rmentor[COL_USERNAME])
      ->where(COL_KD_SCHEDULETYPE, 'FIXED')
      ->where(COL_KD_SCHEDULEDAY, date('w', strtotime($date)))
      ->where(COL_DATE_SCHEDULETIME_FROM, $time)
      ->get(TBL_USERSCHEDULE)
      ->row_array();
      $rdate = $this->db
      ->where(COL_USERNAME, $rmentor[COL_USERNAME])
      ->where(COL_KD_SCHEDULETYPE, 'CALENDAR')
      ->where(COL_DATE_SCHEDULEDATE, $date)
      ->where(COL_DATE_SCHEDULETIME_FROM, $time)
      ->get(TBL_USERSCHEDULE)
      ->row_array();
      if(empty($rday) && empty($rdate)) {
        ShowJsonError('Jadwal yang anda pilih tidak sesuai dengan ketersediaan jadwal mentor. Silakan pilih jadwal yang sesuai.');
        return false;
      }

      $rsesi_exist = $this->db
      ->join(TBL_TSESSION,TBL_TSESSION.'.'.COL_KD_SESSION." = ".TBL_TSESSION_ENTRY.".".COL_KD_SESSION,"inner")
      ->where(COL_KD_STATUS."!=", SESSIONSTAT_CANCELED)
      ->where(COL_NM_SESSIONHOST, $rmentor[COL_USERNAME])
      ->where(COL_NM_PAYMENTIMAGE."!=", null)
      ->where(COL_KD_STATUSPAYMENT."!=", SESSIONPAY_DECLINED)
      ->where(COL_DATE_SESSIONDATE, $date)
      ->where(COL_DATE_SESSIONTIME, $time)
      ->get(TBL_TSESSION_ENTRY)
      ->row_array();
      if(!empty($rsesi_exist)) {
        ShowJsonError('Jadwal yang anda pilih tidak sesuai dengan ketersediaan jadwal mentor. Silakan pilih jadwal lain.');
        return false;
      }

      $rrate = $this->db
      //->where(COL_NUM_EXPFROM.' <=', $rmentor[COL_NUM_EXPERIENCE])
      //->where(COL_NUM_EXPTO.' >=', $rmentor[COL_NUM_EXPERIENCE])
      ->where(COL_NM_GRADE, $rmentor[COL_NM_GRADE])
      ->order_by(COL_NUM_EXPFROM, 'desc')
      ->get(TBL__SETTINGRATE)
      ->row_array();
      if(empty($rrate)) {
        ShowJsonError('Biaya / Tarif belum diatur.');
        return false;
      }

      $rec = array(
        COL_KD_SESSIONVIA => $this->input->post(COL_KD_SESSIONVIA),
        COL_KD_TYPE => $this->input->post(COL_KD_TYPE),
        COL_KD_STATUS => SESSIONSTAT_NEW,
        COL_NM_SESSIONHOST => $rmentor[COL_USERNAME],
        COL_DATE_SESSIONDATE => $date,
        COL_DATE_SESSIONTIME => $this->input->post(COL_DATE_SESSIONTIME),
        COL_NUM_RATE => $this->input->post(COL_KD_SESSIONVIA) == SESSIONFORMAT_ONLINE ? $rrate[COL_NUM_RATEONLINE] : $rrate[COL_NUM_RATEREGULAR],
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      try {
        $res = $this->db->insert(TBL_TSESSION, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception("Database error: ".$err['message']);
        }

        $kdSession = $this->db->insert_id();
        $recEntry = array(
          COL_KD_SESSION => $kdSession,
          COL_KD_STATUSPAYMENT => SESSIONPAY_WAITING,
          COL_USERNAME => $ruser[COL_USERNAME],
          COL_NM_REMARKSCLIENTFEELING => $this->input->post(COL_NM_REMARKSCLIENTFEELING),
          COL_NM_REMARKSCLIENTCONDITION => $this->input->post(COL_NM_REMARKSCLIENTCONDITION),
          COL_NM_REMARKSCLIENTEXPECTATION => $this->input->post(COL_NM_REMARKSCLIENTEXPECTATION),
          COL_CREATEDBY => $ruser[COL_USERNAME],
          COL_CREATEDON => date('Y-m-d H:i:s')
        );

        $res = $this->db->insert(TBL_TSESSION_ENTRY, $recEntry);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception("Database error: ".$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/sesi/finish/'.$kdSession)));
        return;

      } catch(Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }
  }

  public function session_upload_payment($id) {
    $ruser = GetLoggedUser();
    $filename = '';
    $config['upload_path'] = MY_UPLOADPATH;
    $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
    $config['max_size']	= 20480;
    $config['max_width']  = 2560;
    $config['max_height']  = 2560;
    $config['overwrite'] = FALSE;
    if(!empty($_FILES[COL_NM_PAYMENTIMAGE]["name"])) {
      $config['file_name'] = 'MH-'.str_pad($id, 5, '0', STR_PAD_LEFT).".".end((explode(".", $_FILES[COL_NM_PAYMENTIMAGE]["name"])));

      $this->load->library('upload',$config);
      if(!$this->upload->do_upload(COL_NM_PAYMENTIMAGE)){
          $err = $this->upload->display_errors();
          ShowJsonError(strip_tags($err));
          return;
      }

      $dataupload = $this->upload->data();
      if(!empty($dataupload) && $dataupload['file_name']) {
        $filename = $dataupload['file_name'];
      }
    }

    if($filename) {
      $res = $this->db->where(array(COL_KD_SESSION=>$id, COL_USERNAME=>$ruser[COL_USERNAME]))->update(TBL_TSESSION_ENTRY, array(COL_NM_PAYMENTIMAGE=>$filename));
      if(!$res) {
        ShowJsonError('Server Error');
        return;
      }
    }

    ShowJsonSuccess('Berhasil', array('redirect'=>site_url('site/home/dashboard')));
    return;
  }

  function session_add_comment($id, $rate=0) {
    $user = GetLoggedUser();
    $msg = $this->input->post(COL_NM_FEEDBACKTEXT);
    $dat = array(
      COL_KD_SESSION=>$id,
      COL_KD_FEEDBACKTYPE=>'COMMENT',
      COL_NM_FEEDBACKTEXT=>$msg,
      COL_CREATEDBY=>$user[COL_USERNAME],
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );
    if($rate != 0) {
      $dat[COL_NUM_RATE] = $this->input->post(COL_NUM_RATE);
      $dat[COL_KD_FEEDBACKTYPE] = 'RATE';
    }
    $res = $this->db->insert(TBL_TSESSION_FEEDBACK, $dat);
    if($res) {
      ShowJsonSuccess('OK');
    } else {
      ShowJsonError('Server error.');
    }
  }

  function session_change_status($stat, $uniq, $canceled=0) {
    $user = GetLoggedUser();
    $rdata = $res = $this->db->where(COL_UNIQ, $uniq)->get(TBL_TSESSION_ENTRY)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Data tidak valid.');
      return;
    }

    $res = $this->db
    ->where(COL_UNIQ, $uniq)
    ->update(TBL_TSESSION_ENTRY, array(COL_KD_STATUSPAYMENT=>$stat));
    if($res) {
      if($stat == SESSIONPAY_VERIFIED) {
        /*$res = $this->db
        ->where(COL_KD_SESSION, $rdata[COL_KD_SESSION])
        ->update(TBL_TSESSION, array(COL_KD_STATUS=>SESSIONSTAT_CONFIRMED));*/
      } else if($canceled) {
        /*$res = $this->db
        ->where(COL_KD_SESSION, $rdata[COL_KD_SESSION])
        ->update(TBL_TSESSION, array(COL_KD_STATUS=>SESSIONSTAT_CANCELED));*/
      }

      ShowJsonSuccess('OK');
    } else {
      ShowJsonError('Server error.');
    }
  }

  function session_changestat($stat, $uniq) {
    $user = GetLoggedUser();
    $time = $this->input->post(COL_DATE_SESSIONTIME);
    $updateArr = array(
      COL_KD_STATUS=>$stat
    );

    if(!empty($time)) {
      $updateArr[COL_DATE_SESSIONTIME] = $time;
    }

    $res = $this->db
    ->where(COL_KD_SESSION, $uniq)
    ->update(TBL_TSESSION, $updateArr);
    if($res) {
      ShowJsonSuccess('OK');
    } else {
      ShowJsonError('Server error.');
    }
  }

  function session_notes($id) {
    $user = GetLoggedUser();
    $data['data'] = $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_TSESSION_ENTRY)->row_array();
    if(empty($rdata)) {
      echo 'Data tidak valid';
      return;
    }

    if(!empty($_POST)) {
      $res = $this->db
      ->where(COL_UNIQ, $id)
      ->update(TBL_TSESSION_ENTRY, array(COL_NM_REMARKSMENTOR=>$this->input->post(COL_NM_REMARKSMENTOR)));
      ShowJsonSuccess('OK');
    } else {
      $this->load->view('sesi/_session_notes', $data);
    }
  }

  public function get_member_detail($id) {
    $id = GetDecryption($id);
    $user = $this->db
    ->where(COL_USERNAME, $id)
    ->get(TBL__USERINFORMATION)
    ->row_array();
    if(empty($user)) {
      echo 'Maaf, pengguna tidak ditemukan';
      return false;
    }

    $data['user'] = $user;
    $this->load->view('site/user/_detail', $data);
  }

  function feedback_add($id, $rate=0) {
    $user = GetLoggedUser();
    $msg = $this->input->post(COL_NM_FEEDBACKTEXT);
    $dat = array(
      COL_KD_SESSION=>$id,
      COL_KD_FEEDBACKTYPE=>'COMMENT',
      COL_NM_FEEDBACKTEXT=>$msg,
      COL_NM_FEEDBACKNAME=> !empty($user[COL_NM_NICKNAME]) ? $user[COL_NM_NICKNAME] : $user[COL_NM_FIRSTNAME],
      COL_NM_FEEDBACKOCUPATION=>$user[COL_NM_OCCUPATION],
      COL_CREATEDBY=>$user[COL_USERNAME],
      COL_CREATEDON=>date('Y-m-d H:i:s')
    );
    if($rate != 0) {
      $dat[COL_NUM_RATE] = $this->input->post(COL_NUM_RATE);
      $dat[COL_KD_FEEDBACKTYPE] = 'RATE';
    }
    $res = $this->db->insert(TBL_TSESSION_FEEDBACK, $dat);
    if($res) {
      ShowJsonSuccess('OK');
    } else {
      $err = $this->db->error();
      ShowJsonError('Server error. '.$err['message']);
    }
  }
}
