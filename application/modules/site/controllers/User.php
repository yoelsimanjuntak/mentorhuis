<?php
class User extends MY_Controller {
  function __construct() {
      parent::__construct();
      $this->load->library('encrypt');
      $this->load->model('muser');
      if(IsLogin() && GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        //redirect('site/home');
      }
  }

  function index() {
      if(!IsLogin()) {
          redirect('site/user/login');
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
          show_error('Anda tidak memiliki akses terhadap modul ini.');
          return;
      }

      $data['title'] = "User";
      $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
      $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner");
      $this->db->order_by(TBL__USERS.".".COL_USERNAME, 'asc');
      $data['res'] = $this->db->get(TBL__USERS)->result_array();
      //$this->load->view('user/index', $data);
      $this->template->load('backend', 'user/index', $data);
  }

  function Login_old(){
      if(IsLogin()) {
          redirect('site/user/dashboard');
      }
      $rules = array(
          array(
              'field' => 'UserName',
              'label' => 'UserName',
              'rules' => 'required'
          ),
          array(
              'field' => 'Password',
              'label' => 'Password',
              'rules' => 'required'
          )
      );
      $this->form_validation->set_rules($rules);
      if($this->form_validation->run()){
          //$this->load->library('si/securimage');
          $username = $this->input->post(COL_USERNAME);
          $password = $this->input->post(COL_PASSWORD);

          /*if($this->securimage->check($this->input->post('Captcha')) != true){
              redirect(site_url('site/user/login')."?msg=captcha");
          }*/

          if($this->muser->authenticate($username, $password)) {
              if($this->muser->IsSuspend($username)) {
                  redirect(site_url('site/user/login')."?msg=suspend");
              }

              // Update Last Login IP
              $this->db->where(COL_USERNAME, $username);
              $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));

              $userdetails = $this->muser->getdetails($username);
              SetLoginSession($userdetails);
              redirect('site/user/dashboard');
          }
          else {
              redirect(site_url('site/user/login')."?msg=notmatch");
          }
      } else {
          $this->template->load('frontend', 'user/login');
      }

  }

  function login() {
    $this->load->library('google');
    $this->google->setRedirectUri(MY_BASEURL.'user/google-auth-login');

    $data['title'] = "Login";
    $data['googleAuthUrl'] = $this->google->createAuthUrl();
    $this->load->view('user/login', $data);
  }
  function register($role='member') {
    $this->load->library('google');
    $this->google->setRedirectUri(MY_BASEURL.'user/google-auth-register/'.$role);

    $data['title'] = "Daftar";
    $data['role'] = $role;
    $data['googleAuthUrl'] = $this->google->createAuthUrl();
    $this->template->load('frontend', 'user/register', $data);
  }
  function forgotpassword() {
    $data['title'] = "Lupa Password";
    if(!empty($_POST)) {
      $email = $this->input->post(COL_EMAIL);
      $ruser = $this->db
      ->where(COL_EMAIL, $email)
      ->get(TBL__USERINFORMATION)
      ->row_array();

      if(empty($ruser)) {
        ShowJsonError('Email tidak terdaftar.');
        return false;
      }

      $name = $ruser[COL_NM_FIRSTNAME];
      $link = site_url('site/user/resetpassword/'.GetEncryption($ruser[COL_EMAIL]));
      $mailSubj = 'MentorHuis - Reset Password';
      $mailContent = @"
      <p>
        Hai $name, anda telah mengirim permintaan untuk Reset Password. Silakan klik link dibawah untuk mengatur ulang Password anda dan harap menjaga kerahasiaan password anda.
      </p>
      $link
      ";
      $send = SendMail($ruser[COL_EMAIL], $mailSubj, $mailContent);
      ShowJsonSuccess('Link Reset Password telah dikirim ke email anda. Silakan periksa email anda untuk mereset password akun anda', array('redirect'=>$link));
      return false;
    }
    $this->template->load('frontend', 'user/forgotpassword', $data);
  }
  function resetpassword($id) {
    $data['email'] = $email = GetDecryption($id);
    $data['title'] = "Reset Password";
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password.')
        )
      ));

      if($this->form_validation->run()) {
        $ruser = $this->db
        ->where(COL_EMAIL, $email)
        ->get(TBL__USERINFORMATION)
        ->row_array();
        if(empty($ruser)) {
          ShowJsonError('Akun tidak ditemukan.');
          return false;
        }

        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui', array('redirect'=>site_url('site/user/login')));
        return false;
      }
    }
    $this->template->load('frontend', 'user/resetpassword', $data);
  }
  function changepassword() {
    if(!IsLogin()) {
      redirect('site/home');
    }

    $ruser = GetLoggedUser();
    $data['email'] = $ruser[COL_EMAIL];
    $data['title'] = "Ubah Password";
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password.')
        )
      ));

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui. Silakan login kembali.', array('redirect'=>site_url('site/user/logout')));
        return false;
      }
    }
    $this->template->load('frontend', 'user/resetpassword', $data);
  }

  function Logout(){
      UnsetLoginSession();
      redirect(site_url());
  }
  function Dashboard() {
      if(!IsLogin()) {
          redirect('site/user/login');
      }

      $data['title'] = 'Dashboard';
  		$this->template->load('backend', 'site/user/dashboard', $data);
  }
  /*function ChangePassword() {
      if(!IsLogin()) {
          redirect('site/user/login');
      }
      $user = GetLoggedUser();
      $data['title'] = 'Change Password';
      $rules = array(
          array(
              'field' => 'OldPassword',
              'label' => 'Old Password',
              'rules' => 'required'
          ),
          array(
              'field' => COL_PASSWORD,
              'label' => COL_PASSWORD,
              'rules' => 'required'
          ),
          array(
              'field' => 'RepeatPassword',
              'label' => 'Repeat Password',
              'rules' => 'required|matches[Password]'
          )
      );
      $this->form_validation->set_rules($rules);

      if($this->form_validation->run()){
          $rcheck = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->get(TBL__USERS)->row_array();
          if(!$rcheck) {
              redirect(site_url('site/user/changepassword'));
          }
          if($rcheck[COL_PASSWORD] != md5($this->input->post("OldPassword"))) {
              redirect(site_url('site/user/changepassword')."?nomatch=1");
          }
          $upd = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post(COL_PASSWORD))));
          if($upd) redirect(site_url('site/user/changepassword')."?success=1");
          else redirect(site_url('site/user/changepassword')."?error=1");
      }
      else {
        $this->template->load('backend', 'user/changepassword', $data);
      }
  }*/

  function add() {
    if(!IsLogin()) {
        redirect(site_url('site/user/login'));
    }
    $loginuser = GetLoggedUser();
    if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
        show_error('Anda tidak memiliki akses terhadap modul ini.');
        return;
    }

    $data['title'] = "Users";
    $data['edit'] = FALSE;
    if(!empty($_POST)){
        $data['data'] = $_POST;
        $rules = $this->muser->rules(true, $this->input->post(COL_ROLEID));
        $this->form_validation->set_rules($rules);
        $this->form_validation->set_message('custom_rule', 'Karakter yang digunakan untuk username tidak tepat.');
        if($this->form_validation->run()) {
          $userdata = array(
            COL_USERNAME => $this->input->post(COL_USERNAME),
            COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
            COL_ROLEID => $this->input->post(COL_ROLEID),
            COL_ISSUSPEND => false
          );
          $userinfo = array(
            COL_USERNAME => $this->input->post(COL_USERNAME),
            COL_EMAIL => $this->input->post(COL_EMAIL),
            COL_NAME => $this->input->post(COL_NAME),
            COL_IDENTITYNO => $this->input->post(COL_IDENTITYNO),
            COL_BIRTHDATE => $this->input->post(COL_BIRTHDATE)?date('Y-m-d', strtotime($this->input->post(COL_BIRTHDATE))):null,
            COL_RELIGIONID => $this->input->post(COL_RELIGIONID),
            COL_GENDER => $this->input->post(COL_GENDER),
            COL_ADDRESS => $this->input->post(COL_ADDRESS),
            COL_PHONENUMBER => $this->input->post(COL_PHONENUMBER),
            COL_EDUCATIONID => $this->input->post(COL_EDUCATIONID),
            COL_UNIVERSITYNAME => $this->input->post(COL_UNIVERSITYNAME),
            COL_FACULTYNAME => $this->input->post(COL_FACULTYNAME),
            COL_MAJORNAME => $this->input->post(COL_MAJORNAME),
            COL_ISGRADUATED => $this->input->post(COL_ISGRADUATED) ? $this->input->post(COL_ISGRADUATED) : false,
            COL_GRADUATEDDATE => ($this->input->post(COL_ISGRADUATED) && $this->input->post(COL_GRADUATEDDATE) ? date('Y-m-d', strtotime($this->input->post(COL_GRADUATEDDATE))) : null),
            COL_YEAROFEXPERIENCE => $this->input->post(COL_YEAROFEXPERIENCE),
            COL_RECENTPOSITION => $this->input->post(COL_RECENTPOSITION),
            COL_RECENTSALARY => $this->input->post(COL_RECENTSALARY),
            COL_EXPECTEDSALARY => $this->input->post(COL_EXPECTEDSALARY),
            COL_REGISTEREDDATE => date('Y-m-d')
          );
          if($this->input->post(COL_ROLEID) == ROLEKADIS || $this->input->post(COL_ROLEID) == ROLEKEUANGAN) {
            $userinfo[COL_COMPANYID] = $this->input->post(COL_KD_URUSAN).".".$this->input->post(COL_KD_BIDANG).".".$this->input->post(COL_KD_UNIT).".".$this->input->post(COL_KD_SUB);
          }
          else if($this->input->post(COL_ROLEID) == ROLEKABID) {
            $userinfo[COL_COMPANYID] = $this->input->post(COL_KD_URUSAN).".".$this->input->post(COL_KD_BIDANG).".".$this->input->post(COL_KD_UNIT).".".$this->input->post(COL_KD_SUB).".".$this->input->post(COL_KD_BID);
          }
          else if($this->input->post(COL_ROLEID) == ROLEKASUBBID) {
            $userinfo[COL_COMPANYID] = $this->input->post(COL_KD_URUSAN).".".$this->input->post(COL_KD_BIDANG).".".$this->input->post(COL_KD_UNIT).".".$this->input->post(COL_KD_SUB).".".$this->input->post(COL_KD_BID).".".$this->input->post(COL_KD_SUBBID);
          }

          $reg = $this->muser->register($userdata, $userinfo, $companydata);
          if($reg) redirect(site_url('user/index'));
          else redirect(current_url().'?error=1');
        }
        else {
          $this->template->load('backend', 'user/form', $data);
        }
    }
    else {
      $this->template->load('backend', 'user/form', $data);
    }
  }

  function edit($id) {
      if(!IsLogin()) {
          redirect(site_url('user/login'));
      }
      $loginuser = GetLoggedUser();
      if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
          show_error('Anda tidak memiliki akses terhadap modul ini.');
          return;
      }
      $data['title'] = "Users";
      $data['edit'] = TRUE;

      $data['data'] = $edited = $this->muser->getdetails($id);
      if(empty($edited)){
          show_404();
          return;
      }

      if(!empty($_POST)){
          $data['data'] = $_POST;
          $rules = $this->muser->rules(false, $edited[COL_ROLEID]);
          $this->form_validation->set_rules($rules);
          $this->form_validation->set_message('customAlpha', 'Karakter yang digunakan untuk username tidak tepat.');
          if($this->form_validation->run()){
              $config['upload_path'] = MY_UPLOADPATH;
              $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
              $config['max_size']	= 500;
              $config['max_width']  = 1024;
              $config['max_height']  = 768;
              $config['overwrite'] = FALSE;

              $this->load->library('upload',$config);
              if(!empty($_FILES["companyfile"]["name"])) {
                  if(!$this->upload->do_upload("companyfile")){
                      $data['upload_errors'] = $this->upload->display_errors();
                      $this->load->view('company/form', $data);
                      return;
                  }
              }

              $dataupload = $this->upload->data();
              /*$companydata = array(
                  COL_COMPANYNAME => $this->input->post(COL_COMPANYNAME),
                  COL_COMPANYADDRESS => $this->input->post(COL_COMPANYADDRESS),
                  COL_COMPANYTELP => $this->input->post(COL_COMPANYTELP),
                  COL_COMPANYFAX => $this->input->post(COL_COMPANYFAX),
                  COL_COMPANYWEBSITE => $this->input->post(COL_COMPANYWEBSITE),
                  COL_COMPANYEMAIL => $this->input->post(COL_COMPANYEMAIL),
                  COL_INDUSTRYTYPEID => $this->input->post(COL_INDUSTRYTYPEID)
              );
              if(!empty($dataupload) && $dataupload['file_name']) {
                  $companydata[COL_FILENAME] = $dataupload['file_name'];
              }*/

              $userdata = array(
                  COL_NAME => $this->input->post(COL_NAME),
                  COL_EMAIL => $this->input->post(COL_EMAIL),
                  //COL_COMPANYID => join(",", $this->input->post(COL_COMPANYID)),
                  COL_IDENTITYNO => $this->input->post(COL_IDENTITYNO),
                  COL_BIRTHDATE => $this->input->post(COL_BIRTHDATE)?date('Y-m-d', strtotime($this->input->post(COL_BIRTHDATE))):null,
                  COL_RELIGIONID => $this->input->post(COL_RELIGIONID),
                  COL_GENDER => $this->input->post(COL_GENDER),
                  COL_ADDRESS => $this->input->post(COL_ADDRESS),
                  COL_PHONENUMBER => $this->input->post(COL_PHONENUMBER),
                  COL_EDUCATIONID => $this->input->post(COL_EDUCATIONID),
                  COL_UNIVERSITYNAME => $this->input->post(COL_UNIVERSITYNAME),
                  COL_FACULTYNAME => $this->input->post(COL_FACULTYNAME),
                  COL_MAJORNAME => $this->input->post(COL_MAJORNAME),
                  COL_ISGRADUATED => $this->input->post(COL_ISGRADUATED) ? $this->input->post(COL_ISGRADUATED) : false,
                  COL_GRADUATEDDATE => ($this->input->post(COL_ISGRADUATED) && $this->input->post(COL_GRADUATEDDATE) ? date('Y-m-d', strtotime($this->input->post(COL_GRADUATEDDATE))) : null),
                  COL_YEAROFEXPERIENCE => $this->input->post(COL_YEAROFEXPERIENCE),
                  COL_RECENTPOSITION => $this->input->post(COL_RECENTPOSITION),
                  COL_RECENTSALARY => $this->input->post(COL_RECENTSALARY),
                  COL_EXPECTEDSALARY => $this->input->post(COL_EXPECTEDSALARY)
              );
              if($this->input->post(COL_ROLEID) == ROLEKADIS || $this->input->post(COL_ROLEID) == ROLEKEUANGAN) {
                  $userdata[COL_COMPANYID] = $this->input->post(COL_KD_URUSAN).".".$this->input->post(COL_KD_BIDANG).".".$this->input->post(COL_KD_UNIT).".".$this->input->post(COL_KD_SUB);
              }
              else if($this->input->post(COL_ROLEID) == ROLEKABID) {
                  $userdata[COL_COMPANYID] = $this->input->post(COL_KD_URUSAN).".".$this->input->post(COL_KD_BIDANG).".".$this->input->post(COL_KD_UNIT).".".$this->input->post(COL_KD_SUB).".".$this->input->post(COL_KD_BID);
              }
              else if($this->input->post(COL_ROLEID) == ROLEKASUBBID) {
                  $userdata[COL_COMPANYID] = $this->input->post(COL_KD_URUSAN).".".$this->input->post(COL_KD_BIDANG).".".$this->input->post(COL_KD_UNIT).".".$this->input->post(COL_KD_SUB).".".$this->input->post(COL_KD_BID).".".$this->input->post(COL_KD_SUBBID);
              }

              if($edited[COL_ROLEID] == ROLECOMPANY) {
                  $reg = $this->db->where(COL_USERNAME, $edited[COL_USERNAME])->update(TBL__USERINFORMATION, $userdata);
              }
              else {
                  $reg = $this->db->where(COL_USERNAME, $edited[COL_USERNAME])->update(TBL__USERINFORMATION, $userdata);
              }

              $this->db->where(COL_USERNAME, $edited[COL_USERNAME])->update(TBL__USERS, array(COL_ROLEID => $this->input->post(COL_ROLEID)));

              if($reg) redirect(site_url('user/index'));
              else redirect(current_url().'?error=1');
          }
          else {
              $this->template->load('backend', 'user/form', $data);
          }
      }
      else {
          $this->template->load('backend', 'user/form', $data);
      }
  }

  function delete() {
    if(!IsLogin()) {
        ShowJsonError('Silahkan login terlebih dahulu');
        return;
    }
    $loginuser = GetLoggedUser();
    if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
        return;
    }
    $this->load->model('muser');
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        if($this->muser->delete($datum)) {
            $deleted++;
        }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada data dihapus");
    }
  }

  function activate($opt = 0) {
    if(!IsLogin()) {
        ShowJsonError('Silahkan login terlebih dahulu');
        return;
    }
    $loginuser = GetLoggedUser();
    if(!$loginuser || $loginuser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('Anda tidak memiliki akses terhadap modul ini.');
        return;
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
        if($opt == 0 || $opt == 1) {
            if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_ISSUSPEND=>$opt))) {
                $deleted++;
            }
            if($opt == 1) {
                $this->db->where(COL_USERNAME, $datum)->update(TBL__USERINFORMATION, array(COL_REGISTEREDDATE=>date('Y-m-d H:i:s')));
            }
        }
        else {
            if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_PASSWORD=>MD5('123456')))) {
                $deleted++;
            }
        }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data diubah");
    }else{
        ShowJsonError("Tidak ada data yang diubah");
    }
  }

  public function google_auth_register($role='') {
    $this->load->library('google');
    $this->google->setRedirectUri(MY_BASEURL.'user/google-auth-register/'.$role);

    if(isset($_GET['code'])) {
      $token = $this->google->fetchAccessTokenWithAuthCode($_GET["code"]);
      if(!isset($token["error"])) {
        $this->google->setAccessToken($token['access_token']);
        $googleSrv = new Google_Service_Oauth2($this->google);

        $data = $googleSrv->userinfo->get();

        $ruser = $this->db
        ->where(COL_EMAIL, $data["email"])
        ->get(TBL__USERINFORMATION)
        ->row_array();

        if(!empty($ruser)) {
          $dat['title'] = "Daftar";
          $dat['role'] = $role;
          $dat['googleAuthUrl'] = $this->google->createAuthUrl();
          $dat['error'] = "Akun email ini sudah terdaftar di sistem.";
          $this->template->load('frontend', 'user/register', $dat);
          return;
        }

        $roleId = $role == 'mentor' ? ROLEPSIKOLOG : ROLEMEMBER_REG;
        $defPasswd = randomPassword();
        $userdata = array(
          COL_USERNAME => $data['email'],
          COL_PASSWORD => $data['id'],
          COL_ROLEID => $roleId,
          COL_ISSUSPEND => $roleId == ROLEPSIKOLOG ? true : false
        );

        $userinfo = array(
          COL_USERNAME => $data['email'],
          COL_EMAIL => $data['email'],
          COL_NM_FIRSTNAME => $data['given_name'],
          COL_NM_LASTNAME => $data['family_name'],
          COL_DATE_REGISTERED => date('Y-m-d'),
          COL_IS_LOGINVIAGOOGLE => 1,
          COL_NM_GOOGLEOAUTHID => $data['id']
        );
        if(!empty($data['picture'])) {
          $fname = 'pp-'.array_shift(explode("@", $data['email'])).'.png';
          $ch = curl_init($data['picture']);
          $fp = fopen(MY_UPLOADPATH.$fname, 'wb');
          curl_setopt($ch, CURLOPT_FILE, $fp);
          curl_setopt($ch, CURLOPT_HEADER, 0);
          curl_exec($ch);
          curl_close($ch);
          fclose($fp);

          $userinfo[COL_NM_PROFILEIMAGE] = $fname;
        }

        $reg = $this->muser->register($userdata, $userinfo);
        if($reg) {
          if($roleId == ROLEPSIKOLOG) {
            redirect(site_url('site/home/register-finish/'.GetEncryption($userinfo[COL_EMAIL])));
          } else {
            redirect(site_url('site/home/register-done/'.GetEncryption($userinfo[COL_EMAIL])));
          }
        }
        else {
          $dat['title'] = "Daftar";
          $dat['role'] = $role;
          $dat['googleAuthUrl'] = $this->google->createAuthUrl();
          $dat['error'] = "Terjadi kesalahan sistem. Silakan coba beberapa saat lagi.";
          $this->template->load('frontend', 'user/register', $dat);
          return;
        }

        echo json_encode($data);
        return;
      } else {
        echo $token["error"];
        return;
      }
    }
  }

  public function google_auth_login() {
    $this->load->library('google');
    $this->google->setRedirectUri(MY_BASEURL.'user/google-auth-login');

    if(isset($_GET['code'])) {
      $token = $this->google->fetchAccessTokenWithAuthCode($_GET["code"]);
      if(!isset($token["error"])) {
        $this->google->setAccessToken($token['access_token']);
        $googleSrv = new Google_Service_Oauth2($this->google);

        $data = $googleSrv->userinfo->get();

        $ruser = $this->db
        ->where(COL_EMAIL, $data["email"])
        ->where(COL_IS_LOGINVIAGOOGLE, 1)
        ->where(COL_NM_GOOGLEOAUTHID, $data['id'])
        ->get(TBL__USERINFORMATION)
        ->row_array();

        if(empty($ruser)) {
          $dat['title'] = "Login";
          $dat['googleAuthUrl'] = $this->google->createAuthUrl();
          $dat['error'] = "Email belum terdaftar di sistem.";
          $this->load->view('user/login', $dat);
          return;
        }

        $username = $ruser[COL_USERNAME];
        if($this->muser->IsSuspend($username)) {
          $dat['title'] = "Login";
          $dat['googleAuthUrl'] = $this->google->createAuthUrl();
          $dat['error'] = "Akun ini sedang di suspend. Silakan hubungi Administrator untuk info lebih lanjut.";
          $this->load->view('user/login', $dat);
          return;
        }

        $userdetails = $this->muser->getdetails($username);
        if(empty($userdetails[COL_IS_EMAILVERIFIED]) || $userdetails[COL_IS_EMAILVERIFIED] != 1) {
          $dat['title'] = "Login";
          $dat['googleAuthUrl'] = $this->google->createAuthUrl();
          $dat['error'] = "Maaf, akun kamu belum diverifikasi. Silakan periksa link verifikasi yang dikirimkan melalui email kamu.";
          $this->load->view('user/login', $dat);
          return;
        }

        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));
        SetLoginSession($userdetails);
        redirect(site_url('site/home/dashboard'));
      } else {
        echo $token["error"];
        return;
      }
    }
  }
}
 ?>
