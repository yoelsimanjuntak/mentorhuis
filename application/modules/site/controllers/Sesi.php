<?php
class Sesi extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    if (!IsLogin()) {
      redirect('site/user/login');
    }
  }

  public function create($id=0) {
    $data['title'] = 'Mulai Sesi Konseling';
    $data['data'][COL_KD_TYPE] = $id;
    $this->template->load('frontend' , 'sesi/form', $data);
  }

  public function create_caregroup() {
    $ruser = GetLoggedUser();
    $data['title'] = 'Mulai Sesi Care Group';

    if(!empty($_POST)) {
      $quota = !empty($this->input->post(COL_NUM_QUOTA)) ? toNum($this->input->post(COL_NUM_QUOTA)) : 0;
      if($quota <= 0) {
        ShowJsonError('Jlh. Peserta tidak valid.');
        return;
      }

      $rmentor = $this->db
      ->where(COL_USERNAME, $ruser[COL_USERNAME])
      ->get(TBL__USERINFORMATION)
      ->row_array();

      if(empty($rmentor)) {
        ShowJsonError('Mentor tidak valid.');
        return;
      }

      $rrate = $this->db
      //->where(COL_NUM_EXPFROM.' <=', $rmentor[COL_NUM_EXPERIENCE])
      //->where(COL_NUM_EXPTO.' >=', $rmentor[COL_NUM_EXPERIENCE])
      ->where(COL_NM_GRADE, $rmentor[COL_NM_GRADE])
      ->order_by(COL_NUM_EXPFROM, 'desc')
      ->get(TBL__SETTINGRATE)
      ->row_array();
      if(empty($rrate)) {
        ShowJsonError('Biaya / Tarif belum diatur.');
        return false;
      }

      $rec = array(
        COL_KD_SESSIONVIA => $this->input->post(COL_KD_SESSIONVIA),
        COL_KD_TYPE => $this->input->post(COL_KD_TYPE),
        COL_IS_CAREGROUP => 1,
        COL_KD_STATUS => SESSIONSTAT_NEW,
        COL_NM_SESSIONHOST => $ruser[COL_USERNAME],
        COL_DATE_SESSIONDATE => $this->input->post(COL_DATE_SESSIONDATE),
        COL_DATE_SESSIONTIME => $this->input->post(COL_DATE_SESSIONTIME),
        COL_NUM_RATE => ($rrate[COL_NUM_RATEGROUP]/$quota),
        COL_NUM_QUOTA => $quota,
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );
      $res = $this->db->insert(TBL_TSESSION, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError("Database error: ".$err['message']);
        return;
      }
      ShowJsonSuccess("Berhasil");
      return;
      //redirect('site/sesi/index');
    }
    $this->template->load('frontend' , 'sesi/form_caregroup', $data);
  }

  public function finish($id) {
    $ruser = GetLoggedUser();

    $data['title'] = 'Selesaikan Pembayaran';
    $data['sesi'] = $this->db
    ->select(TBL_TSESSION.'.*, mtype.*, mentor.'.COL_NM_FIRSTNAME.' as Mentor_FirstName, mentor.'.COL_NM_LASTNAME.' as Mentor_LastName')
    ->join(TBL__USERINFORMATION.' mentor','mentor.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_NM_SESSIONHOST,"inner")
    ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
    ->where(COL_KD_SESSION, $id)
    ->get(TBL_TSESSION)
    ->row_array();
    $this->template->load('frontend' , 'sesi/form_payment', $data);
  }

  public function index() {
    $ruser = GetLoggedUser();

    $data['title'] = 'Jadwal Saya';
    if($ruser[COL_ROLEID] != ROLEPSIKOLOG) {
      $this->template->load('frontend' , 'sesi/session', $data);
    } else {
      $data['session'] = $this->db
      ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
      ->where(TBL_TSESSION.'.'.COL_NM_SESSIONHOST, $ruser[COL_USERNAME])
      //->where(TBL_TSESSION.'.'.COL_DATE_SESSIONDATE.' >=', date('Y-m-d'))
      ->order_by(COL_DATE_SESSIONDATE, 'desc')
      ->get(TBL_TSESSION)
      ->result_array();

      $data['session_new'] = $this->db
      ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
      ->where(TBL_TSESSION.'.'.COL_NM_SESSIONHOST, $ruser[COL_USERNAME])
      ->where(TBL_TSESSION.'.'.COL_KD_STATUS, SESSIONSTAT_NEW)
      ->order_by(COL_DATE_SESSIONDATE, 'desc')
      ->limit(10)
      ->get(TBL_TSESSION)
      ->result_array();

      $data['session_confirmed'] = $this->db
      ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
      ->where(TBL_TSESSION.'.'.COL_NM_SESSIONHOST, $ruser[COL_USERNAME])
      ->where(TBL_TSESSION.'.'.COL_KD_STATUS, SESSIONSTAT_CONFIRMED)
      ->order_by(COL_DATE_SESSIONDATE, 'desc')
      ->limit(10)
      ->get(TBL_TSESSION)
      ->result_array();

      $data['session_completed'] = $this->db
      ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
      ->where(TBL_TSESSION.'.'.COL_NM_SESSIONHOST, $ruser[COL_USERNAME])
      ->where_in(TBL_TSESSION.'.'.COL_KD_STATUS, array(SESSIONSTAT_COMPLETED, SESSIONSTAT_CANCELED))
      ->order_by(COL_DATE_SESSIONDATE, 'desc')
      ->limit(10)
      ->get(TBL_TSESSION)
      ->result_array();

      $arrScheduleFixed = array();
      $rScheduleFixed = $this->db
      ->join(TBL_MDAYS,TBL_MDAYS.'.'.COL_KD_DAY." = ".TBL_USERSCHEDULE.".".COL_KD_SCHEDULEDAY,"left")
      ->where(COL_USERNAME, $ruser[COL_USERNAME])
      ->where(COL_KD_SCHEDULETYPE, 'FIXED')
      ->get(TBL_USERSCHEDULE)
      ->result_array();
      foreach($rScheduleFixed as $s) {
        $arrScheduleFixed[] = array(
          'Day' => $s[COL_KD_SCHEDULEDAY],
          'DayText' => $s[COL_NM_DAY],
          'TimeFrom' => $s[COL_DATE_SCHEDULETIME_FROM],
          'TimeTo' => $s[COL_DATE_SCHEDULETIME_TO]
        );
      }

      $arrScheduleCalendar = array();
      $rScheduleCalendar = $this->db
      ->where(COL_USERNAME, $ruser[COL_USERNAME])
      ->where(COL_KD_SCHEDULETYPE, 'CALENDAR')
      ->where(COL_DATE_SCHEDULEDATE.' >= ', date('Y-m-d'))
      ->get(TBL_USERSCHEDULE)
      ->result_array();
      foreach($rScheduleCalendar as $s) {
        $arrScheduleCalendar[] = array(
          'Day' => $s[COL_DATE_SCHEDULEDATE],
          'DayText' => $s[COL_DATE_SCHEDULEDATE],
          'TimeFrom' => $s[COL_DATE_SCHEDULETIME_FROM],
          'TimeTo' => $s[COL_DATE_SCHEDULETIME_TO]
        );
      }

      $data['rScheduleFixed'] = $rScheduleFixed;
      $data['rScheduleCalendar'] = $rScheduleCalendar;
      $data['data']['ScheduleFixed'] = json_encode($arrScheduleFixed);
      $data['data']['ScheduleCalendar'] = json_encode($arrScheduleCalendar);
      $this->template->load('frontend' , 'sesi/index', $data);
    }

  }

  public function clients() {
    $ruser = GetLoggedUser();

    $data['title'] = 'Klien Saya';
    $this->template->load('frontend' , 'sesi/index_client', $data);
  }

  public function detail($id) {
    $ruser = GetLoggedUser();

    $data['sesi'] = $rsesi = $this->db
    ->select(TBL_TSESSION.'.*, mtype.*, mentor.'.COL_NM_FIRSTNAME.' as Mentor_FirstName, mentor.'.COL_NM_LASTNAME.' as Mentor_LastName')
    ->join(TBL__USERINFORMATION.' mentor','mentor.'.COL_USERNAME." = ".TBL_TSESSION.".".COL_NM_SESSIONHOST,"inner")
    ->join(TBL_MTYPE,TBL_MTYPE.'.'.COL_KD_TYPE." = ".TBL_TSESSION.".".COL_KD_TYPE,"left")
    ->where(COL_KD_SESSION, $id)
    ->get(TBL_TSESSION)
    ->row_array();
    if(empty($rsesi)) {
      show_error('Jadwal tidak valid');
      return;
    }

    $data['title'] = 'MH-'.str_pad($rsesi[COL_KD_SESSION], 5, '0', STR_PAD_LEFT);
    $this->template->load('frontend' , 'sesi/session', $data);
  }

  function load_comments($id) {
    $user = GetLoggedUser();
    $msgs = $this->db
    ->select('tsession_feedback.*, _userinformation.NM_FirstName')
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TSESSION_FEEDBACK.".".COL_CREATEDBY,"left")
    ->where(COL_KD_SESSION, $id)
    ->where(COL_KD_FEEDBACKTYPE, 'COMMENT')
    ->order_by(COL_CREATEDON, 'asc')
    ->get(TBL_TSESSION_FEEDBACK)
    ->result_array();
    $html = '';
    $html .= '<div class="direct-chat-messages" style="height: 36vh !important">';
    if(empty($msgs)) {
      $html .= '<p class="text-muted">Belum ada catatan / komentar</p>';
    } else {
      $last_user = '';
      $last_time = '';
      foreach($msgs as $m) {
        if($m[COL_CREATEDBY]==$user[COL_USERNAME]) {
          $html .= '<div class="direct-chat-msg right">';
          if($m[COL_NM_FIRSTNAME] != $last_user || date('d M Y  H:i', strtotime($m[COL_CREATEDON])) != $last_time) {
            $html .= '<div class="direct-chat-infos clearfix">';
            $html .= '<span class="direct-chat-name float-right">Anda</span>';
            $html .= '<span class="direct-chat-timestamp float-left">'.date('d M Y  H:i', strtotime($m[COL_CREATEDON])).'</span>';
            $html .= '</div>';
          }
          //$html .= '<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">';
          $html .= '<div class="direct-chat-text mr-0">';
          $html .= $m[COL_NM_FEEDBACKTEXT];
          $html .= '</div>';
          $html .= '</div>';
        } else {
          $html .= '<div class="direct-chat-msg left">';
          if($m[COL_NM_FIRSTNAME] != $last_user || date('d M Y  H:i', strtotime($m[COL_CREATEDON])) != $last_time) {
            $html .= '<div class="direct-chat-infos clearfix">';
            $html .= '<span class="direct-chat-name float-left">'.$m[COL_NM_FIRSTNAME].'</span>';
            $html .= '<span class="direct-chat-timestamp float-right">'.date('d M Y  H:i', strtotime($m[COL_CREATEDON])).'</span>';
            $html .= '</div>';
          }
          //$html .= '<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">';
          $html .= '<div class="direct-chat-text ml-0">';
          $html .= $m[COL_NM_FEEDBACKTEXT];
          $html .= '</div>';
          $html .= '</div>';
        }
        $last_user = $m[COL_NM_FEEDBACKTEXT];
        $last_time = date('d M Y  H:i', strtotime($m[COL_CREATEDON]));
      }
    }
    $html .= '</div>';
    $html .= '<script>$(document).ready(function() { scrollMessages(); });</script>';
    echo $html;
  }
}
?>
